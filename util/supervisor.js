
const meld = require("meld");
const Skematic = require("skematic");

function superviseAction(fn, validator) {
    return meld.around(fn, function (call) {
        const req = call.args[0],
            res = call.args[1];
        //Need To write Logic for all the type of request data
        const data = call.args[0];
        if (validator) {
            const formattedData = Skematic.format(validator, data);
            const validateData = Skematic.validate(validator, formattedData);
            if (validateData.valid) {
                req.data = formattedData;
                return call.proceed();
            }
            req.data = data;
            let errorData = _clean(validateData.errors);
            res.errors = errorData;
        }
        return call.proceed();
    })
}

function _clean(errorsObj) {
    let errorData = "";
    let errorKey = Object.keys(errorsObj)[0];
    let errorValue = errorsObj[Object.keys(errorsObj)[0]].length ? errorsObj[Object.keys(errorsObj)[0]][0] : errorsObj[Object.keys(errorsObj)[0]];
    if (typeof errorValue == "object" && Object.keys(errorValue).length) {
        let objectErrorValue = "";
        for (var valueKey in errorValue) {
            valueKey = (isNaN(valueKey)) ? valueKey : parseInt(valueKey);
            let dirtyValueKey = (isNaN(valueKey)) ? valueKey : "";
            errorKey = errorKey + " " + dirtyValueKey;
            if (!isNaN(valueKey)) {
                errorKey = errorKey + " " + Object.keys(errorValue[valueKey])[0];
            }
            objectErrorValue = isNaN(valueKey) ? errorValue[valueKey][0] : errorValue[valueKey][Object.keys(errorValue[valueKey])[0]][0];
            //objectErrorValue = errorValue[valueKey][0];
            break;
        }
        errorData = errorKey + " " + objectErrorValue;
    } else {
        errorData = errorKey + " " + errorValue;
    }
    return errorData;
}
module.exports = {
    superviseAction
}

