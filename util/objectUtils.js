
function getNestedObjectsValue(obj, fetchValue, callback) {
    for (var objK in obj) {
        if (typeof obj[objK] === 'object' && obj[objK] !== null) {
            parseObjectProperties(obj[objK], parse)
        } else if (obj.hasOwnProperty(objK)) {
            if (objK == fetchValue) {
                callback(obj[objK])
            }
        }
    }
}

function formWhereQuery(whereAndObject = {}, whereOrObject = {}, pages = {}) {
    let { offset = 0, limit = 10 } = pages;
    let whereCondition = Object.keys(whereAndObject).length || Object.keys(whereOrObject).length ? " where  " : "";
    let params = [];
    let condition_flag = false;
    if (Object.keys(whereAndObject).length > 0) {
        for (var whereKey in whereAndObject) {
            whereCondition += (condition_flag == true) ? " and " : " ";
            whereCondition += whereKey + " =  ? ";
            params.push(whereAndObject[whereKey]);
            condition_flag = true;
        }
    }

    if (Object.keys(whereOrObject).length > 0) {
        for (var whereKey in whereOrObject) {
            whereCondition += (condition_flag == true) ? " or " : " ";
            whereCondition += whereKey + " =  ? ";
            params.push(whereOrObject[whereKey]);
            condition_flag = true;
        }
    }
    whereCondition += " limit  " + offset + "," + limit;
    return { whereCondition: whereCondition, params: params };
}

function formQuery(whereObj = {}) {
    let { andOp, orOp, orLikeOp, andLikeOp, andOpNe, offset, limit } = whereObj;
    let whereCondition = "";
    let condition_flag = false;
    let params = [];
    if (typeof andOp == "object" && Object.keys(andOp).length > 0) {
        for (var keyOp in andOp) {
            whereCondition += (condition_flag == true) ? " and " : " ";
            whereCondition += keyOp + "  =  ? ";
            params.push(andOp[keyOp]);
            condition_flag = true;
        }
    }
    if (typeof orOp == "object" && Object.keys(orOp).length > 0) {
        for (var keyOp in orOp) {
            whereCondition += (condition_flag == true) ? " or " : " ";
            whereCondition += keyOp + "  =  ? ";
            params.push(orOp[keyOp]);
            condition_flag = true;
        }
    }

    if (typeof andLikeOp == "object" && Object.keys(andLikeOp).length > 0) {
        for (var keyOp in andLikeOp) {
            whereCondition += (condition_flag == true) ? " and " : " ";
            whereCondition += keyOp + " like   ? ";
            params.push(andLikeOp[keyOp]);
            condition_flag = true;
        }
    }

    if (typeof orLikeOp == "object" && Object.keys(orLikeOp).length > 0) {
        for (var keyOp in orLikeOp) {
            whereCondition += (condition_flag == true) ? " or " : " ";
            whereCondition += keyOp + "  =  ? ";
            params.push(orLikeOp[keyOp]);
            condition_flag = true;
        }
    }

    if (typeof andOpNe == "object" && Object.keys(andOpNe).length > 0) {
        for (var keyOp in andOpNe) {
            whereCondition += (condition_flag == true) ? " and " : " ";
            whereCondition += keyOp + "  <>  ? ";
            params.push(andOpNe[keyOp]);
            condition_flag = true;
        }
    }

    let where = (whereCondition.length) ? " where " + whereCondition : " ";
    let limitcond = "";
    if (typeof offset != "undefined" && typeof limit != "undefined") {
        limitcond = " limit  " + offset + "," + limit;
    }
    let query = where + limitcond;
    return { query: query, params: params };
}
function pick(object, keys) {
    return keys.reduce((obj, key) => {
        if (object && object.hasOwnProperty(key)) {
            obj[key] = object[key];
        }
        return obj;
    }, {});
}


module.exports = {
    getNestedObjectsValue,
    pick,
    formWhereQuery,
    formQuery
}