const _ = require('lodash')
const grpc = require(process.env.NODE_ENV == 'unittest' ? '../test/mocks' : './grpc')

// If Events variable disable 
let nats, rabbitmqpublisher
if (process.env.DISABLE_EVENTS != `true`) {
  nats = require(process.env.NODE_ENV == 'unittest' ? '../test/mocks' : './nats')
  rabbitmqpublisher = require('./rabbitmqpublisher')
}

module.exports = {
  request(channel, request, context, callback) {
    // gRPC is default. If ENABLE_SYSNATS env is set then nats is enabled for sending request
    if (process.env.ENABLE_SYSNATS) {
      if (!context) {
        context = grpc.getMetaData ? grpc.getMetaData() : { getMap: function () { } }
      }
      nats.request(channel, request, context, callback)
    } else {
      const currentcontext = context.getMap()
      if (process.env.NODE_ENV === 'staging' && _.has(currentcontext, 'x-breeze-id')) {
        if (!_.has(_.pick(context.getMap(), ['x-breeze-internal']))) {
          context = context.clone()
          context.set('x-breeze-internal', 'true')
        }
      }
      grpc.request(channel, request, context, callback)
    }
  },
  publish(channel, message, _onpublish) {
    if (process.env.ENABLE_RABBITMQ == `true`) {
      rabbitmqpublisher.publish(channel, message, _onpublish)
    } else {
      nats.publish(channel, message)
    }
  },

  /**
   * RabbitMQ publisher closed at gracefulexit
   */
  getrabbitmqpublisher() {
    return rabbitmqpublisher.getrabbitmqpublisher()
  },

  /**
   * Republish on `requeue`== true from consumer/subscriber only
   * @param {*} channel msg.fields.routingKey
   * @param {*} binary msg.content
   * @param {*} option  {headers:{}}
   * @param {*} _onpublish callback optional
   */
  republish(channel, binary, option, _onpublish) {
    rabbitmqpublisher.republish(channel, binary, option, _onpublish)
  },

  /**
   * Rabbitmq task's message sender
   * @param {*} queuename target queuename as `${servicename}_${w}${process.env.SQS_POSTFIX}`
   * @param {*} msg message in Stringify format
   * @param {*} option optional {}
   * @param {*} onsend optional callback after execute
   * @date 16 April 2019
   */
  taskmessagesender(queuename, msg, option = {}, onsend) {
    rabbitmqpublisher.sendtoqueue(queuename, msg, option, onsend)
  }
}
