const async = require('async')
const _ = require('lodash')
const servicename = require('../package.json').name
const log = require('../sys/log')
const AWS = require('aws-sdk')
const sqs = new AWS.SQS({ apiVersion: '2012-11-05' })
const amqp = require('amqp-connection-manager')
const workers = require('../tasks')

let qurls_existing = []
let qurls_reqd = []

function listQueues(callback) {
  var params = {
    QueueNamePrefix: `${servicename}_`
  }
  sqs.listQueues(params, function(err, data) {
    log(`Response from listqs ${JSON.stringify(data)}`)
    if (data && data.QueueUrls && data.QueueUrls.length > 0) {
      qurls_existing = data.QueueUrls
      log(`QURLS Existing ${qurls_existing}`)
    }
    callback(err)
  })
}

function createMissingQueues(callback) {
  async.forEachOf(
    qurls_reqd,
    (rq, index, _callback) => {
      if (!qurls_existing.find(eq => eq == rq)) {
        sqs.createQueue(
          {
            QueueName: `${servicename}_${workers[index]}${process.env.SQS_POSTFIX}.fifo`,
            Attributes: {
              FifoQueue: 'true',
              VisibilityTimeout: '600',
              ContentBasedDeduplication: 'true'
            }
          },
          (err, data) => {
            if (err)
              log(`Error creating Queue ${servicename}_${workers[index]}_${process.env.SQS_POSTFIX} : ${err}`, true)
            else if (data && data.QueueUrl)
              log(`Created SQS Queue ${servicename}_${workers[index]}${process.env.SQS_POSTFIX}`)
            _callback(null)
          }
        )
      }
    },
    err => {
      callback(null)
    }
  )
}

if (workers.length) {
  _.each(workers, w => {
    qurls_reqd.push(
      `https://sqs.${process.env.AWS_REGION}.amazonaws.com/${process.env.AWS_ACCOUNT_NUMBER}/${servicename}_${w}${
        process.env.SQS_POSTFIX
      }.fifo`
    )
  })
  log(`QURLS reqd ${qurls_reqd}`)

  async.waterfall([listQueues, createMissingQueues], err => {
    if (err) log(err, true)
  })

  /************Ensure auto scaler, RABBITMQ queue exists ***********/
  if (process.env.ENABLE_RABBITMQ_TASKS === 'true') {
    const heartbeat = Number(process.env.RABBITMQ_HEARTBEAT) || 60
    let rbtmqworkerwrapperconn, rbtmqworkerwrapperchannel
    const queueconfig = [
      `amqp://${process.env.RABBITMQ_USER}:${process.env.RABBITMQ_PASSWORD}@${
        process.env.RABBITMQ_HOST_TASKS
      }?heartbeat=${heartbeat}`
    ]
    rbtmqworkerwrapperconn = amqp.connect(queueconfig)
    rbtmqworkerwrapperconn.on('connect', () => log(`Task init queue's connection is created.`.cyan.italic))
    rbtmqworkerwrapperconn.on('disconnect', params => {
      log(`Task init queue connection is close due to below erros.`.cyan, true)
      log(params.err.stack, true, false)
    })
    rbtmqworkerwrapperconn.on('error', error => {
      log(`Task init queue connection catch the unhandled erros, connection closed`.cyan, true)
      log(error, true, false)
    })
    rbtmqworkerwrapperchannel = rbtmqworkerwrapperconn.createChannel({
      setup: chn => {
        chn.on('error', error => {
          log(`Task init queue channel catch the unhandled erros, channel closed`.cyan, true)
          log(error, true, false)
        })
        const bindSubcriberEvent = [chn.prefetch(Number(process.env.RABBITMQ_WORKER_PREFETCH) || 1)]
        _.each(workers, worker => {
          //Bind queue consumer
          bindSubcriberEvent.push(
            chn.assertQueue(`${servicename}_${worker}${process.env.SQS_POSTFIX}`, {
              exclusive: process.env.RABBITMQ_OPT_EXCLUSIVE == 'true' ? true : false,
              durable: process.env.RABBITMQ_OPT_DURABLE == 'true' ? true : false,
              autoDelete: process.env.RABBITMQ_OPT_QUEUE_DEL == 'true' ? true : false
            })
          )
        })
        return Promise.all(bindSubcriberEvent)
      }
    })
    rbtmqworkerwrapperchannel.waitForConnect()
  }
}
