/**
 * Wrapper for nats channels basis channels.js and reactions.js
 */
const async = require('async')
const NATS = require('nats')
const _ = require('lodash')
const log = require('./log')

/**
 * Init comms so RabbitMQ or Nats publisher's could get connected
 * Instead to wait from channels
 */
const comms = require('./comms')
/**
 * RabbitMQ Setup
 */
let rabbitmqsubconn,
  rabbitmqsubchannel,
  rabbitmqsuballchannel = null,
  getrabbitmqconfig = null,
  rxnats = null,
  sysnats = null,
  Recorder = null,
  rabbitmqcacheinvalidchannel = null

// Disable the publisher events
if (process.env.DISABLE_EVENTS != `true`) {
  if (process.env.ENABLE_RABBITMQ == `true`) {
    const amqp = require('amqp-connection-manager')
    const heartbeat = process.env.RABBITMQ_HEARTBEAT ? Number(process.env.RABBITMQ_HEARTBEAT) : 60 // default 60 seconds

    let queueconfig = [
      `amqp://${process.env.RABBITMQ_USER}:${process.env.RABBITMQ_PASSWORD}@${
      process.env.RABBITMQ_HOST
      }?heartbeat=${heartbeat}`
    ]
    if (process.env.RABBITMQ_HOSTS) {
      let hosts = process.env.RABBITMQ_HOSTS.split(',')
      if (hosts && hosts.length) {
        hosts = _.shuffle(hosts) //shuffle IPs to help in load balance
        queueconfig = hosts.map(
          host => `amqp://${process.env.RABBITMQ_USER}:${process.env.RABBITMQ_PASSWORD}@${host}?heartbeat=${heartbeat}`
        )
      }
    }

    // Create a new connection manager
    rabbitmqsubconn = amqp.connect(queueconfig)
    rabbitmqsubconn.on('connect', () => log(`Reaction subscriber's connection is created.`.yellow.italic))
    rabbitmqsubconn.on('disconnect', params => {
      log(`Reaction subscriber connection catch the unhandled errors.`, true)
      log(params.err.message, true)
    })
    rabbitmqsubconn.on('error', error => {
      log(`Reaction subscriber connection catch unhandled error.`, true)
      log(error, true, false)
    })
    rabbitmqsubconn.on('blocked', reason => {
      log(`Reaction subscriber connection stop due to low resources like RAM`.red, true)
      log(reason, true, false)
    })
    rabbitmqsubconn.on('unblocked', () => {
      log(`Reaction connection has resume the subscribing`.green, null)
    })
  } else {
    // creating array of sysnats servers.
    const sysnatsservers = {
      servers: process.env.SYSNATS_HOSTS
        ? process.env.SYSNATS_HOSTS.split(',').map(
          host =>
            `nats://${process.env.SYSNATS_USER}:${process.env.SYSNATS_PASSWORD}@${host}:${process.env.SYSNATS_PORT}`
        )
        : [
          `nats://${process.env.SYSNATS_USER}:${process.env.SYSNATS_PASSWORD}@${process.env.SYSNATS_HOST}:${
          process.env.SYSNATS_PORT
          }`
        ]
    }
    // creating array of rxnats servers.
    const rxnatsservers = {
      servers: process.env.RXNATS_HOSTS
        ? process.env.RXNATS_HOSTS.split(',').map(
          host => `nats://${process.env.RXNATS_USER}:${process.env.RXNATS_PASSWORD}@${host}:${process.env.RXNATS_PORT}`
        )
        : [
          `nats://${process.env.RXNATS_USER}:${process.env.RXNATS_PASSWORD}@${process.env.RXNATS_HOST}:${
          process.env.RXNATS_PORT
          }`
        ]
    }
    //connecting to sysnats if ENABLE_SYSNATS exists
    sysnats =
      process.env.ENABLE_SYSNATS == `true`
        ? NATS.connect({ ...sysnatsservers, preserveBuffers: true, encoding: 'binary' })
        : null
    //connecting to rxnats
    rxnats = NATS.connect({
      ...rxnatsservers,
      preserveBuffers: true,
      encoding: 'binary'
    })
  }
}

const channels = require('../channels')
const reactions = require('../reactions')
const pjson = require('../package.json')
const protoroot = require('../protobuf/build')
const microtime = require('microtime')
let counters = require('./counters')
const grpc = require('./grpc')
const { grpcnorthsouthserver, grpceastwestserver, grpcclient } = grpc
const reactionsall = require(`../reactions/all`)

// for testing : to disable cache handler
let cachehandler
if (process.env.DISABLE_CACHE != 'true') {
  cachehandler = require('./cachehandler')

  if (process.env.NODE_ENV === 'staging') {
    Recorder = require('./middleware/recorder')
  }
}

module.exports = function () {
  const synchronousnorthsouthchannels = []
  const synchronouseastwestchannels = []
  const _reactions = []
  const servicename = pjson.name
  const grpcnorthsouthobj = {}
  const grpceastwestobj = {}
  const channelsinvalidationevents = []

  _.each(channels, function (options, functionname) {
    const channel = {
      fullname: `${servicename}/${functionname}`,
      name: `${functionname}`,
      execution: require(`../channels/${functionname}`)
    }
    if (options) {
      if ('fakekey' in options) {
        channel.fakekey = options.fakekey
        channel.prefixfunction = options.prefixfunction
        channel.invalidationevents = options.invalidationevents
        //build array of object channels => use on invalidate cache
        options.invalidationevents.forEach(event =>
          channelsinvalidationevents.push({ event, fakekey: options.fakekey })
        )
      }
      if (options[process.env.NORTH_SOUTH || 'northsouth']) {
        synchronousnorthsouthchannels.push(channel)
      }
      if (options[process.env.EAST_WEST || 'eastwest']) {
        synchronouseastwestchannels.push(channel)
      }
    } else {
      log(`Please set options for channel ${functionname} in /channels.js file`.red.bold)
    }
  })

  /**
   * Fetch data from persistet db.
   */
  function executefunc(request, metadata, channel, callback) {
    channel.execution(request, metadata, function (err, objectresponse) {
      //execute callback on response
      callback(err, objectresponse)

      //Set cache if channel is cacheable
      if (channel.fakekey) {
        //Convert object to binary
        let responsebinary = protoroot[servicename][channel.name]['response'].encode(objectresponse).finish()
        //log(responsebinary)
        cachehandler.setcache(cachehandler.getprefix(request), channel.fakekey, responsebinary, err => {
          if (err) {
            log(err, true)
            // log(`Caching failed for channel[fakekey]: ${channel.fakekey}`)
          } else {
            log(`Data has cached on redis for channel[fakekey]: ${channel.fakekey}`)
          }
        })
      }

      const currentcontext = metadata.getMap()
      if (process.env.NODE_ENV === 'staging' && _.has(currentcontext, 'x-breeze-id')) {
        const recorder = new Recorder()
        recorder.save(
          recorder.capture(channel.fullname, { request, metadata }, err ? { err: err.message } : objectresponse)
        )
      }
      if (process.env.ENABLE_COUNTERS == 'true') counters.decrementnorthsouthchannelcount(channel.name)
    })
  }

  //Updated for using binary protobuf requests and responses
  async.each(synchronousnorthsouthchannels, channel => {
    if (process.env.ENABLE_SYSNATS) {
      sysnats.subscribe(`${channel.fullname}`, { queue: `${channel.fullname}` }, function (request, replyto) {
        if (process.env.ENABLE_COUNTERS == 'true') counters.incrementchannelcount(channel.name)
        const protomsg = protoroot[servicename][channel.name]['request'].decode(request)
        channel.execution(protomsg, protomsg.ctx, function (err, response) {
          if (protomsg.ctx && protomsg.ctx.trail) {
            response.ctx = protomsg.ctx
            response.ctx.trail.push({ servicename: servicename, microtime: microtime.now(), isreq: false })
          } else {
            log(`WARNING : No existing NATS context received for channel ${channel.name}`, true)
          }
          let responseobj = protoroot[servicename][channel.name]['response'].create(response)
          let responsebinary = protoroot[servicename][channel.name]['response'].encode(responseobj).finish()
          sysnats.publish(replyto, responsebinary)
          if (process.env.ENABLE_COUNTERS == 'true') counters.decrementchannelcount(channel.name)
        })
      })
    }

    grpcnorthsouthobj[channel.name] = function (call, callback) {
      if (process.env.ENABLE_COUNTERS == 'true') counters.incrementnorthsouthchannelcount(channel.name)
      // const metadata = _.pick(call.metadata, ['x-request-id', 'x-b3-traceid', 'x-b3-spanid', 'x-b3-parentspanid', 'x-b3-sampled', 'x-b3-flags', 'x-ot-span-context'])
      let { metadata, request } = call
      if (!metadata || typeof metadata.set !== 'function') {
        metadata = grpc.getMetaData()
      }
      metadata.set(process.env.NORTH_SOUTH || 'northsouth', 'true')

      /**
       * Get cached data if has channel.fake exist
       * @cached channels
       * Cache hit if miss
       */
      if (channel.fakekey) {
        cachehandler.getcache(cachehandler.getprefix(request), channel.fakekey, (err, binaryresponse) => {
          if (err || !binaryresponse) {
            if (err) log(err, true)
            // log(`\tNo cached data available`)
            //Channel is cacheable but cache does not exist.
            log('Cache not found!')
            executefunc(request, metadata, channel, callback)
          } else {
            //Cache exist !, convert binary to object & return response
            log('Cache found!')
            const objectresponse = protoroot[servicename][channel.name]['response'].decode(binaryresponse)
            callback(null, objectresponse)

            const currentcontext = metadata.getMap()
            if (process.env.NODE_ENV === 'staging' && _.has(currentcontext, 'x-breeze-id')) {
              const recorder = new Recorder()
              recorder.save(
                recorder.capture(channel.fullname, { request, metadata }, err ? { err: err.message } : objectresponse)
              )
            }
            if (process.env.ENABLE_COUNTERS == 'true') counters.decrementnorthsouthchannelcount(channel.name)
          }
        })
      } else {
        /**
         * Uncached channels, execute as usuals
         * Get from from persistent database
         */

        executefunc(request, metadata, channel, callback)
      }
    }
  })

  async.each(synchronouseastwestchannels, channel => {
    if (process.env.ENABLE_SYSNATS) {
      sysnats.subscribe(`${channel.fullname}`, { queue: `${channel.fullname}` }, function (request, replyto) {
        if (process.env.ENABLE_COUNTERS == 'true') counters.incrementchannelcount(channel.name)
        const protomsg = protoroot[servicename][channel.name]['request'].decode(request)
        channel.execution(protomsg, protomsg.ctx, function (err, response) {
          if (protomsg.ctx && protomsg.ctx.trail) {
            response.ctx = protomsg.ctx
            response.ctx.trail.push({ servicename: servicename, microtime: microtime.now(), isreq: false })
          } // else {
          //   log(`WARNING : No existing NATS context received for channel ${channel.name}`, true)
          // }
          let responseobj = protoroot[servicename][channel.name]['response'].create(response)
          let responsebinary = protoroot[servicename][channel.name]['response'].encode(responseobj).finish()
          sysnats.publish(replyto, responsebinary)
          if (process.env.ENABLE_COUNTERS == 'true') counters.decrementchannelcount(channel.name)
        })
      })
    }

    grpceastwestobj[channel.name] = function (call, callback) {
      if (process.env.ENABLE_COUNTERS == 'true') counters.incrementeastwestchannelcount(channel.name)
      // const metadata = _.pick(call.metadata, ['x-request-id', 'x-b3-traceid', 'x-b3-spanid', 'x-b3-parentspanid', 'x-b3-sampled', 'x-b3-flags', 'x-ot-span-context'])
      let { metadata, request } = call
      if (!metadata || typeof metadata.set !== 'function') {
        metadata = grpc.getMetaData()
      }
      metadata.set(process.env.EAST_WEST || 'eastwest', 'true')
      channel.execution(request, metadata, function (err, response) {
        callback(err, response)
        const currentcontext = metadata.getMap()
        if (process.env.NODE_ENV === 'staging' && _.has(currentcontext, 'x-breeze-id')) {
          const recorder = new Recorder()
          recorder.save(
            recorder.capture(channel.fullname, { request, metadata }, err ? { err: err.message } : response)
          )
        }
        if (process.env.ENABLE_COUNTERS == 'true') counters.decrementeastwestchannelcount(channel.name)
      })
    }
  })

  grpc.listennorthsouth(grpcnorthsouthobj)
  grpc.listeneastwest(grpceastwestobj)

  _.each(reactions, function (filename, channelname) {
    const channel = {
      fullname: `${channelname}`,
      filename: `${filename}`,
      execution: require(`../reactions/${filename}`) //load package at once
    }
    return _reactions.push(channel)
  })

  /**
   * Requeue features
   * Direct send to queue
   * Consumer will be same
   * @param {*} msg consume msg
   */
  function retryqueue(msg) {
    const headers = {
      retry: (msg.properties.headers.retry || 0) + 1,
      requeue: true,
      routingKey: msg.fields.routingKey,
      exchange: msg.fields.exchange
    }
    const exponenttimer = Number(process.env.RETRY_EXPONENT_TIME) || 500
    const mls = Math.pow(2, headers.retry) * exponenttimer + Math.floor(Math.random() * 50)
    log(
      `REQUEUE: messageId ${msg.properties.messageId}, on reaction "${msg.fields.routingKey}", retry: ${
      headers.retry
      }, exponetial backoff: ${mls}ms`
    )
    setTimeout(() => {
      comms.republish(msg.fields.routingKey, msg.content, {
        headers,
        messageId: msg.properties.messageId
      })
    }, mls)
  }

  /**
   * Bind RabbitMQ subribers if `process.env.ENABLE_RABBITMQ`:true
   * Define `routing key`(channelname) on `assertExchange` from `process.env`
   * `Routing key` or channelname has subscribed as conditional '#' `#` or eventname
   * `Below consumer samples has implemented to handle both individual & '#' event`
   * @link https://github.com/benbria/node-amqp-connection-manager/blob/master/examples/pubsub-subscriber.js
   */
  // Disable publisher events variables 
  if (process.env.DISABLE_EVENTS != `true`) {
    if (process.env.ENABLE_RABBITMQ == `true`) {
      //Subscriber particular routes bind from reactions.js
      rabbitmqsubchannel = rabbitmqsubconn.createChannel({
        setup: chn => {
          chn.on('error', err => {
            log(`Reaction subscriber catch the unhandled error`, true)
            log(err, true, true)
          })
          const bindSubcriberEvent = [
            chn.assertQueue(pjson.name, {
              exclusive: process.env.RABBITMQ_OPT_EXCLUSIVE == 'true' ? true : false,
              durable: process.env.RABBITMQ_OPT_DURABLE == 'true' ? true : false,
              autoDelete: process.env.RABBITMQ_OPT_QUEUE_DEL == 'true' ? true : false
            }),
            chn.assertExchange(process.env.RABBITMQ_EXCHANGE, `topic`, {
              durable: process.env.RABBITMQ_OPT_DURABLE == 'true' ? true : false
            }),
            chn.prefetch(process.env.RABBITMQ_PREFETCH ? Number(process.env.RABBITMQ_PREFETCH) : 10)
          ]
          //Bind Bind for _reactions array
          _reactions.forEach(reaction => {
            bindSubcriberEvent.push(chn.bindQueue(pjson.name, process.env.RABBITMQ_EXCHANGE, reaction.fullname))
          })
          //Bind  consumer for _reactions array
          bindSubcriberEvent.push(
            chn.consume(pjson.name, msg => {
              //case: msg consume from sendToQueue
              if (msg.properties.headers.requeue) {
                msg.fields.routingKey = msg.properties.headers.routingKey
                msg.fields.exchange = msg.properties.headers.exchange
              }
              const protopackage = _.get(protoroot, msg.fields.routingKey)
              const protomsg = protopackage['event'].decode(msg.content)
              const reaction = _reactions.find(obj => {
                return obj.fullname == msg.fields.routingKey
              })
              // console.log(`\nEvent subscriber msg on key "${msg.fields.routingKey}", `, msg)
              if (reaction && reaction.execution) {
                reaction.execution(protomsg, function (err, requeue) {
                  if (err) {
                    if (
                      requeue == true &&
                      msg.properties.headers.retry < (Number(process.env.RABBITMQ_REQUEUE_RETRY) || 3)
                    ) {
                      retryqueue(msg)
                      chn.ack(msg)
                    } else {
                      log(
                        `Skipped reaction ${msg.fields.routingKey}, on message ${JSON.stringify(msg.properties)}.`,
                        true
                      )
                      chn.nack(msg, true, false)
                    }
                  } else {
                    // console.log(`ACK Done ${msg.properties.messageId}\n`)
                    chn.ack(msg)
                  }
                })
              } else {
                // If allUpTo:true, all outstanding messages prior to and including the given message are rejected.
                //requeue: false
                chn.nack(msg, true, false)
                log(`No handler exist for reaction '${msg.fields.routingKey}'`, true)
              }
            })
          )
          /**
           * Bind all subscribers
           * Take input as array of `assertQueue`, `assertExchange`, `prefetch`, `bindQueue` and `consume` method
           */
          return Promise.all(bindSubcriberEvent)
        }
      })
      rabbitmqsubchannel
        .waitForConnect()
        .then(function () {
          log(`Reaction subscriber is listening.`.yellow.italic)
        })
        .catch(err => {
          log(`Reaction subscriber is refused to consume message.`, true)
          log(err.stack, true, true)
        })

      //Subscribe all events as queuename=> servicename+"_all"
      if (process.env.RABBITMQ_SUBSCRIBE_ALL == `true`) {
        rabbitmqsuballchannel = rabbitmqsubconn.createChannel({
          setup: ch => {
            ch.on('error', err => {
              log(`Reaction global subscriber catch the unhandled error.`, true)
              log(err, true, true)
            })
            return Promise.all([
              ch.assertQueue(`${pjson.name}_all`, {
                exclusive: process.env.RABBITMQ_OPT_EXCLUSIVE == 'true' ? true : false,
                durable: process.env.RABBITMQ_OPT_DURABLE == 'true' ? true : false,
                autoDelete: process.env.RABBITMQ_OPT_QUEUE_DEL == 'true' ? true : false
              }),
              ch.assertExchange(process.env.RABBITMQ_EXCHANGE, `topic`, {
                durable: process.env.RABBITMQ_OPT_DURABLE == 'true' ? true : false
              }),
              ch.prefetch(process.env.RABBITMQ_PREFETCH ? Number(process.env.RABBITMQ_PREFETCH) : 10),
              ch.bindQueue(`${pjson.name}_all`, process.env.RABBITMQ_EXCHANGE, `#`),
              ch.consume(`${pjson.name}_all`, msg => {
                // console.log(`\nGlobal subscriber msg on key "${msg.fields.routingKey}",`, JSON.stringify(msg))
                reactionsall(msg.fields.routingKey, msg.content, function (err, requeue) {
                  if (err) {
                    if (
                      requeue == true &&
                      msg.properties.headers.retry < (Number(process.env.RABBITMQ_REQUEUE_RETRY) || 3)
                    ) {
                      retryqueue(msg)
                      ch.ack(msg)
                    } else {
                      log(
                        `Skipped reaction ${msg.fields.routingKey}, on message ${JSON.stringify(msg.properties)}.`,
                        true
                      )
                      log(err, true)
                      ch.nack(msg, true, false)
                    }
                  } else {
                    // console.log(`ACK Done ${msg.properties.messageId}\n`)
                    ch.ack(msg)
                  }
                })
              })
            ])
          }
        })
        rabbitmqsuballchannel
          .waitForConnect()
          .then(function () {
            log(`Reaction global subscriber is listening.`.blue.italic)
          })
          .catch(err => {
            log(`Reaction global refused to consume message.`, true)
            log(err.stack, true, true)
          })
      }

      //******** BEGIN DELETE REACTION CACHE SECTION
      if (channelsinvalidationevents.length) {
        rabbitmqcacheinvalidchannel = rabbitmqsubconn.createChannel({
          setup: chdel => {
            chdel.on('error', err => {
              log(`Invalidate cache subscriber channel catch the unhandled error`, true)
              log(err, true, true)
            })
            const bindchachedelete = [
              chdel.assertQueue(`invalidation_${pjson.name}`, {
                exclusive: process.env.RABBITMQ_OPT_EXCLUSIVE == 'true' ? true : false,
                durable: process.env.RABBITMQ_OPT_DURABLE == 'true' ? true : false,
                autoDelete: process.env.RABBITMQ_OPT_QUEUE_DEL == 'true' ? true : false
              }),
              chdel.assertExchange(process.env.RABBITMQ_EXCHANGE, `topic`, {
                durable: process.env.RABBITMQ_OPT_DURABLE == 'true' ? true : false
              }),
              chdel.prefetch(process.env.RABBITMQ_PREFETCH ? Number(process.env.RABBITMQ_PREFETCH) : 10)
            ]
            //Bind queue for channelsinvalidationevents array
            channelsinvalidationevents.forEach(invalidate => {
              bindchachedelete.push(
                chdel.bindQueue(`invalidation_${pjson.name}`, process.env.RABBITMQ_EXCHANGE, invalidate.event)
              )
            })

            //Bind  consumer for channelsinvalidationevents array
            bindchachedelete.push(
              chdel.consume(`invalidation_${pjson.name}`, msg => {
                const protopackage = _.get(protoroot, msg.fields.routingKey)
                const protomsg = protopackage['event'].decode(msg.content)
                const invalidate = channelsinvalidationevents.find(obj => {
                  return obj.event == msg.fields.routingKey
                })
                // console.log(`Subscribed invalidate cache routes key ${msg.fields.routingKey} received, `, protomsg)
                if (invalidate) {
                  // console.log(msg.fields.routingKey)
                  cachehandler.deletecache(cachehandler.getprefix(protomsg), invalidate.fakekey)
                  chdel.ack(msg) //Callback not required
                } else {
                  chdel.nack(msg, true, false)
                  log(`Rejecting Invalidate cache '${msg.fields.routingKey}' reaction`, true)
                }
              })
            )
            return Promise.all(bindchachedelete)
          }
        })

        rabbitmqcacheinvalidchannel
          .waitForConnect()
          .then(function () {
            log(`Invalidate cache channel is running.`.magenta.bold)
          })
          .catch(err => {
            log(`Invalidate cache channel is refused to consume message.`, true)
            log(err.stack, true, true)
          })
      }
      //******** END DELETE REACTION CACHE SECTION

      /**
       * Forward RabbitMQ publisher & subscriber's channels, connections for graceful exit
       * Publisher channel & connection fetched from `rabbitmqpublisher.js` via `comms.js`
       */
      getrabbitmqconfig = () => {
        const { rabbitmqpubchannel, rabbitmqpubconn } = comms.getrabbitmqpublisher()
        return {
          rabbitmqsuballchannel,
          rabbitmqsubchannel,
          rabbitmqsubconn,
          rabbitmqpubchannel,
          rabbitmqpubconn,
          rabbitmqcacheinvalidchannel
        }
      }
    } else {
      /**
       * Bind subscribe events for delete cache.
       * Channel must have fakekey.
       * channelsinvalidationevents : [{event, fakekey}]
       *  queue: invalidation_$servicename
       */
      async.each(channelsinvalidationevents, invalidate => {
        rxnats.subscribe(invalidate.event, { queue: `invalidation_${servicename}` }, request => {
          const protopackage = _.get(protoroot, `${invalidate.event}`)
          const protomsg = protopackage['event'].decode(request)
          //protomsg must have token & customerid
          cachehandler.deletecache(cachehandler.getprefix(protomsg), invalidate.fakekey)
        })
      })

      //'#' conditional listener
      if (process.env.RXNATS_SUBSCRIBE_ALL == 'true') {
        rxnats.subscribe(`>`, { queue: `${servicename}` }, function (request, reply, event) {
          if (process.env.ENABLE_COUNTERS == 'true') counters.incrementreactioncount(`${servicename}/${event}`)
          reactionsall(event, request, function (err) {
            if (err) {
              log(err, true)
            }
            if (process.env.ENABLE_COUNTERS == 'true') counters.decrementreactioncount(`${servicename}/${event}`)
          })
        })
      }

      async.each(_reactions, reaction => {
        rxnats.subscribe(`${reaction.fullname}`, { queue: `${pjson.name}` }, function (request) {
          if (process.env.ENABLE_COUNTERS == 'true') counters.incrementreactioncount(reaction.fullname)
          const protopackage = _.get(protoroot, reaction.fullname)
          const protomsg = protopackage['event'].decode(request)
          reaction.execution(protomsg, function (err) {
            if (err) {
              log(err, true, true)
            }
            if (process.env.ENABLE_COUNTERS == 'true') counters.decrementreactioncount(reaction.fullname)
          })
        })
      })
    }
  }

  log(
    `Service ${pjson.name} is running as node version ${process.version} on ${new Date().toLocaleString()}`.bold.green
  )
  return {
    sysnats,
    rxnats,
    grpcnorthsouthserver,
    grpceastwestserver,
    grpcclient,
    getrabbitmqconfig
  }
}
