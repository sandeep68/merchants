let channelcounters = {}
let reactioncounters = {}
let routecounters = {}
let allchannels = 0
let allreactions = 0
let allroutes = 0
let all = 0
let alleastwestchannels = 0
let allnorthsouthchannels = 0
let rabbitmqpublishercounter = 0

let channelnorthsouthcounters = {}
let channeleastwestcounters = {}

module.exports = {
  incrementchannelcount: channelname => {
    if (channelcounters[channelname]) channelcounters[channelname]++
    else channelcounters[channelname] = 1
    allchannels++
    all++
  },
  decrementchannelcount: channelname => {
    channelcounters[channelname]--
    allchannels--
    all--
  },
  incrementeastwestchannelcount: channelname => {
    if (channeleastwestcounters[channelname]) channeleastwestcounters[channelname]++
    else channeleastwestcounters[channelname] = 1
    allchannels++
    all++
    alleastwestchannels++
  },
  decrementeastwestchannelcount: channelname => {
    channeleastwestcounters[channelname]--
    allchannels--
    all--
    alleastwestchannels--
  },
  incrementnorthsouthchannelcount: channelname => {
    if (channelnorthsouthcounters[channelname]) channelnorthsouthcounters[channelname]++
    else channelnorthsouthcounters[channelname] = 1
    allchannels++
    all++
    allnorthsouthchannels++
  },
  decrementnorthsouthchannelcount: channelname => {
    channelnorthsouthcounters[channelname]--
    allchannels--
    all--
    allnorthsouthchannels--
  },

  incrementreactioncount: reactionname => {
    if (reactioncounters[reactionname]) reactioncounters[reactionname]++
    else reactioncounters[reactionname] = 1
    allreactions++
    all++
  },
  decrementreactioncount: reactionname => {
    reactioncounters[reactionname]--
    allreactions--
    all--
  },
  incrementroutecount: route => {
    if (routecounters[route]) routecounters[route]++
    else routecounters[route] = 1

    allroutes++
    all++
  },
  decrementroutecount: route => {
    routecounters[route]--
    allroutes--
    all--
  },
  getall: () => {
    return {
      channelcounters,
      reactioncounters,
      routecounters,
      allchannels,
      allreactions,
      allroutes,
      all,
      allnorthsouthchannels,
      alleastwestchannels,
      rabbitmqpublishercounter
    }
  },
  incrementpublishcount: () => {
    rabbitmqpublishercounter++
  },
  decrementpublishcount: () => {
    rabbitmqpublishercounter--
  }
}
