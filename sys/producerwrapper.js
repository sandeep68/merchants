const comms = require('./comms')
const AWS = require('aws-sdk')
const sha256 = require('sha256')
const log = require('./log')

module.exports = {
  sendmessage(queuename, message, runid, completioncount, batchcount, callback) {
    if (process.env.ENABLE_RABBITMQ_TASKS === 'true') {
      const messageBody = {
        data: message,
        completioncount,
        batchcount,
        processid: runid
      }

      comms.taskmessagesender(queuename, messageBody, {}, () => {
        callback(null)
      })
    } else {
      const SQS = new AWS.SQS({ region: process.env.AWS_REGION })
      let queueurl = `https://sqs.${process.env.AWS_REGION}.amazonaws.com/${
        process.env.AWS_ACCOUNT_NUMBER
      }/${queuename}.fifo`

      let params = {
        MessageBody: JSON.stringify({
          data: message,
          completioncount,
          batchcount,
          processid: runid
        }),
        QueueUrl: queueurl,
        MessageGroupId: `${runid}`,
        DelaySeconds: 0
      }

      log(`queueurl: ${queueurl}`)

      params.MessageDeduplicationId = sha256(params.MessageBody)

      SQS.sendMessage(params, (err, res) => {
        log(`Sent: ${completioncount}`)
        if (err) log(err, true)
        if (callback) callback(null)
      })
    }
  }
}
