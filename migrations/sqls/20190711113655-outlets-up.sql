/* Replace with your SQL commands */
CREATE TABLE `outlets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `retailerid` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `mangerid` int(11) DEFAULT '0',
  `address` text,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `pincode` int(11) DEFAULT NULL,
  `latitude` decimal(10,7) DEFAULT NULL,
  `longitude` decimal(10,7) DEFAULT NULL,
  `pos` smallint(6) DEFAULT NULL,
  `poynt` smallint(6) DEFAULT NULL,
  `web` smallint(6) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `isactive` char(1) DEFAULT 'Y',
  `deleted` char(1) DEFAULT 'N',
  PRIMARY KEY (`id`)
)