/* Replace with your SQL commands */
ALTER TABLE `outlets` 
ADD COLUMN `contactname` VARCHAR(100) NULL DEFAULT NULL AFTER `web`,
ADD COLUMN `phone` VARCHAR(45) NULL DEFAULT NULL AFTER `contactname`,
ADD COLUMN `email` VARCHAR(255) NULL DEFAULT NULL AFTER `phone`;