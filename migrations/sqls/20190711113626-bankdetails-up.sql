/* Replace with your SQL commands */
CREATE TABLE `bankdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `retailerid` varchar(45) DEFAULT NULL,
  `accountnumber` varchar(45) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `ifsc` varchar(15) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `isactive` char(1) DEFAULT 'Y',
  `deleted` char(1) DEFAULT 'N',
  PRIMARY KEY (`id`)
)