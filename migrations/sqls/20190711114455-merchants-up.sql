/* Replace with your SQL commands */
CREATE TABLE `merchants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aggregatorid` int(11) DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `address` text,
  `managerid` int(11) DEFAULT '0',
  `pan` varchar(11) DEFAULT NULL,
  `servicefee` decimal(10,2) DEFAULT NULL COMMENT 'Percentage',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `isactive` char(1) DEFAULT 'Y',
  `deleted` char(1) DEFAULT 'N',
  PRIMARY KEY (`id`)
)