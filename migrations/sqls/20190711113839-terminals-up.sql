/* Replace with your SQL commands */
CREATE TABLE `terminals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `outletid` int(11) DEFAULT '0',
  `terminal_id` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL COMMENT 'type (mPOS/TPL/4.5/6.3)',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `isactive` char(1) DEFAULT 'Y',
  `deleted` char(1) DEFAULT 'N',
  PRIMARY KEY (`id`)
)