/* Replace with your SQL commands */
ALTER TABLE `merchants` 
ADD COLUMN `contactname` VARCHAR(100) NULL DEFAULT NULL AFTER `pan`,
ADD COLUMN `phone` VARCHAR(45) NULL DEFAULT NULL AFTER `contactname`,
ADD COLUMN `email` VARCHAR(255) NULL DEFAULT NULL AFTER `phone`;
