/* Replace with your SQL commands */
ALTER TABLE `outlets` 
CHANGE COLUMN `address` `area` TEXT NULL DEFAULT NULL ,
ADD COLUMN `region` VARCHAR(45) NULL DEFAULT NULL AFTER `pincode`;
