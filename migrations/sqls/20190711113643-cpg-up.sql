/* Replace with your SQL commands */

CREATE TABLE `cpg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aggregatorid` int(11) DEFAULT '0',
  `merchantid` int(11) DEFAULT NULL,
  `retailerid` int(11) NOT NULL,
  `form` varchar(45) DEFAULT NULL COMMENT '(eGV/Physical)',
  `denomination` int(11) DEFAULT NULL,
  `prefix` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `isactive` char(1) DEFAULT 'Y',
  `deleted` char(1) DEFAULT 'N',
  PRIMARY KEY (`id`)
) 