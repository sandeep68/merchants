/* Replace with your SQL commands */
ALTER TABLE `retailers` 
ADD COLUMN `contactname` VARCHAR(100) NULL DEFAULT NULL AFTER `gstcertificate`,
ADD COLUMN `phone` VARCHAR(45) NULL DEFAULT NULL AFTER `contactname`,
ADD COLUMN `email` VARCHAR(255) NULL DEFAULT NULL AFTER `phone`;