/* Replace with your SQL commands */
CREATE TABLE `cpgheads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cpgid` INT NULL,
  `head` VARCHAR(45) NULL,
  `pointer` VARCHAR(45) NULL,
  `poolid` INT NULL,
  `booklethead` VARCHAR(45) NULL,
  `bookletpointer` VARCHAR(45) NULL,
  `bookletpoolid` INT NULL,
  `isactive` char(1) DEFAULT 'Y',
  `deleted` char(1) DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`));
