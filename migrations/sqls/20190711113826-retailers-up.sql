/* Replace with your SQL commands */
CREATE TABLE `retailers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merchantid` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `managerid` int(11) DEFAULT NULL,
  `gstno` varchar(20) DEFAULT NULL,
  `gstaddress` text,
  `categoryid` int(11) DEFAULT NULL,
  `subcategoryid` int(11) DEFAULT NULL,
  `servicefee` decimal(10,2) DEFAULT NULL COMMENT 'percentage',
  `pan` text,
  `cheque` text,
  `gstcertificate` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `isactive` char(1) DEFAULT 'Y',
  `deleted` char(1) DEFAULT 'N',
  PRIMARY KEY (`id`)
)