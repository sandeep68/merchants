const pjson = require('../package.json')
require('colors')
const fs = require('fs')
const path = require('path')
const log = require('../sys/log')
const grpcLibrary = require('grpc')
const mocha = require('mocha')
const { describe, it, before, afterEach } = mocha
const chai = require('chai')
const { expect } = chai
const rewire = require('rewire')
const sinon = require('sinon')
const sinonChai = require('sinon-chai')
const _ = require('lodash')
const services = {
  channels: require('../channels'),
  reactions: require('../reactions')
}
require('env2')('./devenv.json')
chai.use(sinonChai)
let mockgetrequest, mockpostrequest, commsmocks, mockfunctions

// Returns if a value is a string
function isString(value) {
  return typeof value === 'string' || value instanceof String
}
// Returns if a value is an array
function isArray(value) {
  return value && typeof value === 'object' && value.constructor === Array
}
// Returns if a value is an object
function isObject(value) {
  return value && typeof value === 'object' && value.constructor === Object
}
// Describes all the unit tests for the all the channels of a service
describe(`Unit tests for ${pjson.name} service`.yellow.bold, function() {
  const sandbox = sinon.createSandbox()
  afterEach(() => {
    sandbox.restore()
  })
  _.each(services, function(service, key) {
    _.each(services[key], function(name, val) {
      if (key === 'channels') {
        name = val
      }
      describe(`${name} ${key.substr(0, key.length - 1)} test suit`.blue.bold, function() {
        const tpath = path.resolve(__dirname, 'declarations', key, `${name}.json`)
        if (!fs.existsSync(tpath)) {
          log(`${tpath} not found`, true)
          return
        }
        const declarations = require(`./declarations/${key}/${name}.json`)
        let channel = require(`../${key}/${name}`)
        // Global mocks for all the tests of a channel
        if (_.has(declarations, 'cases') && isArray(declarations.cases)) {
          _.each(declarations.cases, function(tcase) {
            if (_.has(tcase, 'name') && isString(tcase.name)) {
              describe('', function() {
                if (_.has(tcase, 'mocks') && isObject(tcase.mocks)) {
                  before(function() {
                    mockgetrequest = null
                    mockpostrequest = null
                    mockfunctions = {}
                    commsmocks = {}
                    channel = rewire(`../${key}/${name}`)
                    if (_.has(tcase.mocks, 'requestmocks') && isObject(tcase.mocks.requestmocks)) {
                      if (_.has(tcase.mocks.requestmocks, 'get') && isArray(tcase.mocks.requestmocks.get)) {
                        mockgetrequest = sandbox.stub(channel.__get__('request'), 'get')
                        _.each(tcase.mocks.requestmocks.get, function(get, i) {
                          mockgetrequest.onCall(i).callsArgWith(1, null, {}, get.mockresponse)
                        })
                      }
                      if (_.has(tcase.mocks.requestmocks, 'post') && isArray(tcase.mocks.requestmocks.post)) {
                        mockpostrequest = sandbox.stub(channel.__get__('request'), 'post')
                        _.each(tcase.mocks.requestmocks.post, function(post, i) {
                          mockpostrequest.onCall(i).callsArgWith(2, null, {}, post.mockresponse)
                        })
                      }
                    }
                    if (_.has(tcase.mocks, 'functionmocks') && isObject(tcase.mocks.functionmocks)) {
                      _.each(tcase.mocks.functionmocks, function(functionmock, key) {
                        let mockfunction = key.split(/\./)
                        let mockobject
                        if (mockfunction.length > 1) {
                          mockobject = mockfunction.splice(0, 1).join('')
                          mockfunction = mockfunction.join('.')
                          mockfunctions[mockfunction] = sandbox.stub(channel.__get__(mockobject), mockfunction)
                          if (isObject(functionmock.mockresponse)) {
                            mockfunctions[mockfunction]
                              .onCall(0)
                              .callsArgWith(functionmock.expectedinput.length, null, functionmock.mockresponse)
                          } else {
                            mockfunctions[mockfunction]
                              .onCall(0)
                              .callsArgWith(functionmock.expectedinput.length, null, ...functionmock.mockresponse)
                          }
                        }
                      })
                    }
                    if (_.has(tcase.mocks, 'commsmocks') && isObject(tcase.mocks.commsmocks)) {
                      _.each(tcase.mocks.commsmocks, function(commsmock, key) {
                        let mockfunction = key.split(/\./)
                        let mockobject
                        if (mockfunction.length === 2) {
                          mockobject = mockfunction.splice(0, 1).join('')
                          mockfunction = mockfunction.join('.')
                          commsmocks[mockfunction] = sandbox.stub(channel.__get__(mockobject), mockfunction)
                          if (mockfunction === 'request') {
                            _.each(tcase.mocks.commsmocks[key], function(commsmock, i) {
                              commsmocks[mockfunction]
                                .onCall(i)
                                .callsArgWith(3, commsmock.expectederror, commsmock.expectedresponse)
                            })
                          }
                        }
                      })
                    }
                  })
                }
                it(tcase.name, function(done) {
                  const request = tcase.request
                  if (key === 'channels') {
                    const ctx = new grpcLibrary.Metadata()
                    if (_.has(tcase, process.env.EAST_WEST || 'eastwest')) {
                      ctx.set(process.env.EAST_WEST || 'eastwest', 'true')
                    }
                    if (_.has(tcase, process.env.NORTH_SOUTH || 'northsouth')) {
                      ctx.set(process.env.NORTH_SOUTH || 'northsouth', 'true')
                    }
                    channel(request, ctx, function(err, response) {
                      if (_.has(tcase, 'error')) {
                        expect(err.message).to.equal(tcase.error)
                      }
                      if (_.has(tcase.mocks, 'commsmocks') && isObject(tcase.mocks.commsmocks)) {
                        _.each(tcase.mocks.commsmocks, function(commsmock, key) {
                          let mockfunction = key.split(/\./)
                          if (mockfunction.length === 2) {
                            mockfunction.splice(0, 1)
                            mockfunction = mockfunction.join('.')
                            _.each(tcase.mocks.commsmocks[key], function(commsmock) {
                              if (mockfunction === 'request') {
                                expect(commsmocks[mockfunction]).to.have.been.called.calledWith(commsmock.channel)
                              }
                              if (mockfunction === 'publish') {
                                expect(commsmocks[mockfunction]).to.have.been.called.calledWith(
                                  commsmock.reaction,
                                  commsmock.expectedrequest
                                )
                              }
                            })
                          }
                        })
                      }
                      if (_.has(tcase.mocks, 'requestmocks') && isObject(tcase.mocks.requestmocks)) {
                        if (_.has(tcase.mocks.requestmocks, 'get') && isArray(tcase.mocks.requestmocks.get)) {
                          _.each(tcase.mocks.requestmocks.get, function(get, i) {
                            if (_.has(get, 'url')) {
                              expect(mockgetrequest.args[i][0]).to.be.equal(get.url)
                            }
                          })
                        }
                        if (_.has(tcase.mocks.requestmocks, 'post') && isArray(tcase.mocks.requestmocks.post)) {
                          _.each(tcase.mocks.requestmocks.post, function(post, i) {
                            if (_.has(post, 'url')) {
                              expect(mockpostrequest.args[i][0]).to.be.equal(post.url)
                            }
                            if (_.has(post, 'expectedrequest')) {
                              expect(mockpostrequest.args[i][1]).to.be.deep.equal(post.expectedrequest)
                            }
                          })
                        }
                      }
                      if (_.has(tcase.mocks, 'functionmocks') && isObject(tcase.mocks.functionmocks)) {
                        _.each(tcase.mocks.functionmocks, function(functionmock, key) {
                          let mockfunction = key.split(/\./)
                          if (mockfunction.length > 1) {
                            mockfunction.splice(0, 1)
                            mockfunction = mockfunction.join('.')
                            if (functionmock.expectedinput !== undefined) {
                              if (isObject(functionmock.expectedinput)) {
                                expect(mockfunctions[mockfunction]).to.have.been.called.calledOnceWith(
                                  functionmock.expectedinput
                                )
                              } else {
                                expect(mockfunctions[mockfunction]).to.have.been.called.calledOnceWith(
                                  ...functionmock.expectedinput
                                )
                              }
                            } else {
                              expect(mockfunctions[mockfunction]).to.have.been.called.calledOnce
                            }
                          }
                        })
                      }
                      if (!_.has(tcase, 'error') && _.has(tcase, 'asserts') && isArray(tcase.asserts)) {
                        _.each(tcase.asserts, function(_resp) {
                          const assertArr = _resp.assert.split(/\./)
                          const assertFunction = assertArr.splice(assertArr.length - 1)
                          if (_resp.key === '') {
                            _.get(expect(response), assertArr.join('.'))[assertFunction.join()](_resp.value)
                          } else {
                            _.get(expect(_.get(response, _resp.key)), assertArr.join('.'))[assertFunction.join()](
                              _resp.value
                            )
                          }
                        })
                      }
                    })
                  } else if (key === 'reactions') {
                    channel(request, err => log(err, true))
                    if (_.has(tcase.mocks, 'requestmocks') && isObject(tcase.mocks.requestmocks)) {
                      if (_.has(tcase.mocks.requestmocks, 'get') && isArray(tcase.mocks.requestmocks.get)) {
                        _.each(tcase.mocks.requestmocks.get, function(get, i) {
                          if (_.has(get, 'url')) {
                            expect(mockgetrequest.args[i][0]).to.be.equal(get.url)
                          }
                        })
                      }
                      if (_.has(tcase.mocks.requestmocks, 'post') && isArray(tcase.mocks.requestmocks.post)) {
                        _.each(tcase.mocks.requestmocks.post, function(post, i) {
                          if (_.has(post, 'url')) {
                            expect(mockpostrequest.args[i][0]).to.be.equal(post.url)
                          }
                          if (_.has(post, 'expectedrequest')) {
                            expect(mockpostrequest.args[i][1]).to.be.deep.equal(post.expectedrequest)
                          }
                        })
                      }
                    }
                    if (_.has(tcase.mocks, 'functionmocks') && isObject(tcase.mocks.functionmocks)) {
                      _.each(tcase.mocks.functionmocks, function(functionmock, key) {
                        let mockfunction = key.split(/\./)
                        if (mockfunction.length > 1) {
                          mockfunction.splice(0, 1)
                          mockfunction = mockfunction.join('.')
                          if (functionmock.expectedinput !== undefined) {
                            if (isObject(functionmock.expectedinput)) {
                              expect(mockfunctions[mockfunction]).to.have.been.called.calledOnceWith(
                                functionmock.expectedinput
                              )
                            } else {
                              expect(mockfunctions[mockfunction]).to.have.been.called.calledOnceWith(
                                ...functionmock.expectedinput
                              )
                            }
                          } else {
                            expect(mockfunctions[mockfunction]).to.have.been.called.calledOnce
                          }
                        }
                      })
                    }
                    if (_.has(tcase.mocks, 'commsmocks') && isObject(tcase.mocks.commsmocks)) {
                      _.each(tcase.mocks.commsmocks, function(commsmock, key) {
                        let mockfunction = key.split(/\./)
                        if (mockfunction.length === 2) {
                          mockfunction.splice(0, 1)
                          mockfunction = mockfunction.join('.')
                          _.each(tcase.mocks.commsmocks[key], function(commsmock) {
                            if (mockfunction === 'request') {
                              expect(commsmocks[mockfunction]).to.have.been.called.calledWith(commsmock.channel)
                            }
                            if (mockfunction === 'publish') {
                              expect(commsmocks[mockfunction]).to.have.been.called.calledWith(
                                commsmock.reaction,
                                commsmock.expectedrequest
                              )
                            }
                          })
                        }
                      })
                    }
                  }
                  done()
                })
              })
            }
          })
        }
      })
    })
  })
})
