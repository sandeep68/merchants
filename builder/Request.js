class CreateMerchant {
  create(obj = {}) {
    let { terminals, cpg } = obj
    let merchant = {}
    merchant = this.merchantObj(obj)
    merchant.retailer = this.retailerObj(obj.retailer)
    merchant.bank = this.bankObj(obj.retailer)
    merchant.outlet = this.outletObj(obj.outlet)
    merchant.terminals = terminals
    merchant.cpg = cpg
    return merchant
  }

  merchantObj(obj) {
    let { name, aggregatorid = 0, address, pan, servicefee, contactname, phone, email } = obj
    let merchant = { name, aggregatorid, address, pan, servicefee, contactname, phone, email }
    return merchant
  }

  cpgObj(obj) {
    let { form, denomination, denominationid, isspc, isdeferred, name = "" } = obj;
    let cpg = { form, denomination, denominationid, isspc, isdeferred, name };
    return cpg;
  }

  retailerObj(obj = {}) {
    let { id, aggregatorid = 0, gstno, gstaddress, pan, cheque, prefix, gstcertificate, name, contactname, phone, email, categoryid, subcategoryid, servicefee } = obj
    let retailer = { id, aggregatorid, gstno, gstaddress, pan, cheque, prefix, gstcertificate, name, contactname, phone, email, categoryid, subcategoryid, servicefee }
    return retailer
  }

  outletObj(obj = {}) {
    let { id, name, area, city, state, region, pincode, latitude, longitude, sapsitecode, storeid, pos, poynt, web, contactname, phone, email, gstno } = obj
    let outlet = { id, name, area, city, state, region, pincode, latitude, longitude, sapsitecode, storeid, pos, poynt, web, contactname, phone, email, gstno }
    return outlet
  }

  bankObj(obj = {}) {
    let { bankname, ifsc, accountnumber } = obj
    let bank = { bankname, ifsc, accountnumber }
    return bank
  }

  terminalObj(obj) {
    let { terminal_id, type } = obj
    let terminal = { terminal_id, type };
    return terminal;
  }
}

class UpdateObj {
  managerObj(obj = {}) {
    let { contactname, phone, email } = obj
    let manager = { contactname, phone, email }
    return manager
  }

  updateMerchantObj(obj) {
    let merchantObj = {}
    merchantObj = {
      merchantid: obj.merchantid,
      ...CreateMerchant.prototype.merchantObj.call(this, obj)
    }
    return merchantObj
  }

  updateRetailerObj(obj) {
    let retailerObj = {}
    retailerObj = {
      retailerid: obj.retailerid,
      ...CreateMerchant.prototype.retailerObj.call(this, obj),
      ...CreateMerchant.prototype.bankObj.call(this, obj)
    }
    return retailerObj
  }

  updateOutletObj(obj) {
    let outletObj = {}
    outletObj = {
      outletid: obj.outletid,
      ...CreateMerchant.prototype.outletObj.call(this, obj),
    }
    return outletObj
  }

  updateTerminalObj(obj) {
    let terminalObj = {}
    terminalObj = {
      terminalid: obj.terminalid,
      ...CreateMerchant.prototype.terminalObj.call(this, obj),
    }
    return terminalObj
  }
}


module.exports = {
  CreateMerchant,
  UpdateObj
}
