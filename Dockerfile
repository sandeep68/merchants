FROM node:10.15.0-alpine
WORKDIR /service
COPY package*.json ./
# build deps needed for node-gyp
RUN apk add --no-cache --virtual .build-deps alpine-sdk python \
    && npm install --production --silent \
    && apk del .build-deps
CMD ["/bin/sh", "./node_modules/protobufjs/bin/pbjs -t static-module -w commonjs -o ./protobuf/build.js ./protobuf/bbpc/* ./protobuf/campaign/* ./protobuf/comms/* ./protobuf/commsv2/* ./protobuf/customers/* ./protobuf/events/* ./protobuf/khaata/* ./protobuf/payback/* ./protobuf/payments/* ./protobuf/prepaid/* ./protobuf/pricematch/* ./protobuf/programs/* ./protobuf/recharges/* ./protobuf/metaservice/*"]
COPY . .
EXPOSE 3000 3001 3002
USER node
CMD ["node","service.js"]