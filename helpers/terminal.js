const UpdateTerminalValidator = require('../validators/update-terminal').UpdateTerminalValidator
const Supervisor = require('../util/supervisor')
const TerminalService = require('../core/terminal')
const CommonValidator = require("../validators/common-validator");
const RequestProcessing = require("../validators/request-processing");
const ApprovalService = require('../core/approval');

const MESSAGES = require('../constant/messages');


async function detail(req, res, callback) {
    try {
        if (res.hasOwnProperty('errors')) {
            return callback({
                status: MESSAGES.RESPONSE_STATUS.failed,
                message: res.errors
            });
        }
        let respData = await TerminalService.detail(req)
        if (respData.hasOwnProperty("status")) {
            callback(respData);
        } else {
            callback({
                status: MESSAGES.RESPONSE_STATUS.success,
                message: MESSAGES.RESPONSE_STATUS.completed,
                data: respData
            });
        }
    } catch (error) {
        callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
    }
}

async function list(req, res, callback) {
    try {
        if (res.hasOwnProperty('errors')) {
            return callback({
                status: MESSAGES.RESPONSE_STATUS.failed,
                message: res.errors
            });
        }
        let respData = await TerminalService.list(req)
        if (respData.hasOwnProperty("status")) {
            callback(respData);
        } else {
            callback({
                status: MESSAGES.RESPONSE_STATUS.success,
                message: MESSAGES.RESPONSE_STATUS.completed,
                data: respData
            });
        }
    } catch (error) {
        callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
    }
}

async function search(req, res, callback) {
    try {
        if (res.hasOwnProperty('errors')) {
            return callback({
                status: MESSAGES.RESPONSE_STATUS.failed,
                message: res.errors
            });
        }
        let respData = await TerminalService.search(req)
        if (respData.hasOwnProperty("status")) {
            callback(respData);
        } else {
            callback({
                status: MESSAGES.RESPONSE_STATUS.success,
                message: MESSAGES.RESPONSE_STATUS.completed,
                data: respData
            });
        }
    } catch (error) {
        callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
    }
}

/**
 * Logic:
 * Validate Terminal Data
 * Get TerminalId from Request Body
 *  if ID not found return TerminalId not found
 * Select respective Terminal if found
 * Create Terminal update request
 * Return success response
 * @param {*} req
 * @param {*} res
 * @param {*} callback
 */
async function updatemaker(req, res, callback) {
    try {
        if (res.hasOwnProperty('errors')) {
            return callback({
                status: MESSAGES.RESPONSE_STATUS.failed,
                message: res.errors
            });
        }
        let respData = await TerminalService.updateTerminalRequest(req.data)
        if (respData.hasOwnProperty("status")) {
            callback(respData);
        } else {
            callback({
                status: MESSAGES.RESPONSE_STATUS.success,
                message: MESSAGES.RESPONSE_STATUS.completed,
                data: respData
            });
        }
    } catch (error) {
        callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
    }
}

/**
 * Logic:
 * Get Request Id and applicable status to process the request
 * Validate the Data by Id and Status
 * @param {*} req
 * @param {*} res 
 * @param {*} callback 
 */
async function updatechecker(req, res, callback) {
    try {
        if (res.hasOwnProperty('errors')) {
            return callback({
                status: MESSAGES.RESPONSE_STATUS.failed,
                message: res.errors
            });
        }
        let respData = await ApprovalService.updateTerminal(req.data)
        if (respData.hasOwnProperty("status")) {
            callback(respData);
        } else {
            callback({
                status: MESSAGES.RESPONSE_STATUS.success,
                message: MESSAGES.RESPONSE_STATUS.completed,
                data: respData
            });
        }
    } catch (error) {
        callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
    }
}
module.exports = {
    detail: Supervisor.superviseAction(detail, new CommonValidator.GetDetailByIdValidator()),
    search: Supervisor.superviseAction(search, new CommonValidator.SearchValidator()),
    list,
    updatemaker: Supervisor.superviseAction(updatemaker, new UpdateTerminalValidator()),
    updatechecker: Supervisor.superviseAction(updatechecker, new RequestProcessing.NewRequestProcessingValidator()),
}
