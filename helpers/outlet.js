const UpdateOutletValidator = require('../validators/update-outlet').UpdateOutletValidator
const Supervisor = require('../util/supervisor')
const OutletService = require('../core/outlet')
const RequestProcessing = require("../validators/request-processing");
const ApprovalService = require('../core/approval');
const CommonValidator = require("../validators/common-validator");
const MESSAGES = require("../constant/messages");

async function detail(req, res, callback) {
  try {
    if (res.hasOwnProperty('errors')) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = await OutletService.detail(req)
    if (respData.hasOwnProperty("status")) {
      callback(respData);
    } else {
      callback({
        status: MESSAGES.RESPONSE_STATUS.success,
        message: MESSAGES.RESPONSE_STATUS.completed,
        data: respData
      });
    }
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
  }
}

async function list(req, res, callback) {
  try {
    if (res.hasOwnProperty('errors')) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = await OutletService.list(req)
    if (respData.hasOwnProperty("status")) {
      callback(respData);
    } else {
      callback({
        status: MESSAGES.RESPONSE_STATUS.success,
        message: MESSAGES.RESPONSE_STATUS.completed,
        data: respData
      });
    }
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
  }
}

async function search(req, res, callback) {
  try {
    if (res.hasOwnProperty('errors')) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = await OutletService.search(req)
    if (respData.hasOwnProperty("status")) {
      callback(respData);
    } else {
      callback({
        status: MESSAGES.RESPONSE_STATUS.success,
        message: MESSAGES.RESPONSE_STATUS.completed,
        data: respData
      });
    }
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
  }
}

/**
 * Logic:
 * Validate Outlet Data
 * Get OutletId from Request Body
 *  if ID not found return OutletId not found
 * Select respective Outlet if found
 * Create Outlet update request
 * Return success response
 * @param {*} req
 * @param {*} res
 * @param {*} callback
 */
async function updatemaker(req, res, callback) {
  try {
    if (res.hasOwnProperty('errors')) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = await OutletService.updateOutletRequest(req.data)
    if (respData.hasOwnProperty("status")) {
      callback(respData);
    } else {
      callback({
        status: MESSAGES.RESPONSE_STATUS.success,
        message: MESSAGES.RESPONSE_STATUS.completed,
        data: respData
      });
    }
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
  }
}

/**
 * Logic:
 * Get Request Id and applicable status to process the request
 * Validate the Data by Id and Status
 * @param {*} req
 * @param {*} res 
 * @param {*} callback 
 */
async function updatechecker(req, res, callback) {
  try {
    if (res.hasOwnProperty('errors')) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = await ApprovalService.updateOutlet(req.data)
    if (respData.hasOwnProperty("status")) {
      callback(respData);
    } else {
      callback({
        status: MESSAGES.RESPONSE_STATUS.success,
        message: MESSAGES.RESPONSE_STATUS.completed,
        data: respData
      });
    }
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
  }
}

module.exports = {
  detail: Supervisor.superviseAction(detail, new CommonValidator.GetDetailByIdValidator()),
  search: Supervisor.superviseAction(search, new CommonValidator.SearchByIDNameValidator()),
  list,
  updatemaker: Supervisor.superviseAction(updatemaker, new UpdateOutletValidator()),
  updatechecker: Supervisor.superviseAction(updatechecker, new RequestProcessing.NewRequestProcessingValidator()),
}
