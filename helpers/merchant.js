const CreateMerchantValidator = require('../validators/create-merchant').CreateMerchantValidator
const UpdateMerchantValidator = require('../validators/update-merchant').UpdateMerchantValidator
const CommonValidator = require("../validators/common-validator");
const RequestProcessing = require("../validators/request-processing");
const Supervisor = require('../util/supervisor')
const MerchantService = require('../core/merchant')
const ApprovalService = require('../core/approval')
const MESSAGES = require("../constant/messages");
const DENOMINATIONS = require("../constant/variables").DENOMINATIONS;


/**
 * Logic:
 * validate user input
 * Create merchant request for Approval
 * @param {*} req
 * @param {*} res 
 * @param {*} callback 
 */
async function create(req, res, callback) {
  try {
    if (res.hasOwnProperty("errors")) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = await MerchantService.createMerchantRequest(req.data);
    if (respData.hasOwnProperty("status")) {
      callback(respData);
    } else {
      callback({
        status: MESSAGES.RESPONSE_STATUS.success,
        message: MESSAGES.RESPONSE_STATUS.completed,
        data: respData
      });
    }
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
  }
}



/**
 * Logic:
 * Get Request Id and applicable status to process the request
 * Validate the Data by Id and Status
 * @param {*} req
 * @param {*} res 
 * @param {*} callback 
 */
async function processNewMerchant(req, res, callback) {
  try {
    if (res.hasOwnProperty('errors')) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = await ApprovalService.createMerchant(req.data)
    if (respData.hasOwnProperty("status")) {
      callback(respData);
    } else {
      callback({
        status: MESSAGES.RESPONSE_STATUS.success,
        message: MESSAGES.RESPONSE_STATUS.completed,
        data: respData
      });
    }
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
  }
}

/**
 * Logic:
 * Get the  request from pending approval to 
 * Create merchant and get mid
 *  if cpg then creat cpg entry using merchant id
 * Create Retailer using mid and get rid
 *  create manager entry using mid,rid and update maid into retailter
 * Create multiple CPG entry using rid
 * Create outlets using rid and mid
 * Create multiple terminal using outletid
 * @param {*} req
 * @param {*} res 
 * @param {*} callback 
 */
async function createNewMerchant(req, res, callback) {
  try {
    if (res.hasOwnProperty('errors')) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = await MerchantService.requestNewMerchantProcess(req)
    callback({
      status: MESSAGES.RESPONSE_STATUS.success,
      message: MESSAGES.RESPONSE_STATUS.completed,
      data: respData
    });
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
  }
}

/**
 * Logic:
 * Get Merchant Detail Based on Id
 * Create merchant and get mid
 * @param {*} req
 * @param {*} res 
 * @param {*} callback 
 */

async function detail(req, res, callback) {
  try {
    if (res.hasOwnProperty('errors')) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = await MerchantService.detail(req)
    if (respData.hasOwnProperty("status")) {
      callback(respData);
    } else {
      callback({
        status: MESSAGES.RESPONSE_STATUS.success,
        message: MESSAGES.RESPONSE_STATUS.completed,
        data: respData
      });
    }
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
  }
}

/**
 * Logic:
 * Merchant Search Based on Name
 * @param {*} req
 * @param {*} res 
 * @param {*} callback 
 */

async function search(req, res, callback) {
  try {
    if (res.hasOwnProperty('errors')) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = await MerchantService.search(req)
    if (respData.hasOwnProperty("status")) {
      callback(respData);
    } else {
      callback({
        status: MESSAGES.RESPONSE_STATUS.success,
        message: MESSAGES.RESPONSE_STATUS.completed,
        data: respData
      });
    }
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
  }
}

/**
 * Logic:
 * Validate Merchant Data
 * Get MerchantId from Request Body
 *  if ID not found return MerchantId not found
 * Select respective Merchant if found
 * Create merchant update request
 * Return success response
 * @param {*} req
 * @param {*} res
 * @param {*} callback
 */
async function updatemaker(req, res, callback) {
  try {
    if (res.hasOwnProperty('errors')) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = await MerchantService.updateMerchantRequest(req.data)
    if (respData.hasOwnProperty("status")) {
      callback(respData);
    } else {
      callback({
        status: MESSAGES.RESPONSE_STATUS.success,
        message: MESSAGES.RESPONSE_STATUS.completed,
        data: respData
      });
    }
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
  }
}

/**
 * Logic:
 * Get Request Id and applicable status to process the request
 * Validate the Data by Id and Status
 * @param {*} req
 * @param {*} res 
 * @param {*} callback 
 */
async function updatechecker(req, res, callback) {
  try {
    if (res.hasOwnProperty('errors')) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = await ApprovalService.updateMerchant(req.data)
    if (respData.hasOwnProperty("status")) {
      callback(respData);
    } else {
      callback({
        status: MESSAGES.RESPONSE_STATUS.success,
        message: MESSAGES.RESPONSE_STATUS.completed,
        data: respData
      });
    }
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
  }
}

async function deletemaker(req, res, callback) {
  try {
    if (res.hasOwnProperty('errors')) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = await ApprovalService.deleteActorsMaker(req)
    if (respData.hasOwnProperty("status")) {
      callback(respData);
    } else {
      callback({
        status: MESSAGES.RESPONSE_STATUS.success,
        message: MESSAGES.RESPONSE_STATUS.completed,
        data: respData
      });
    }
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
  }
}

async function deletechecker(req, res, callback) {
  try {
    if (res.hasOwnProperty('errors')) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = await ApprovalService.deleteActorsChecker(req)
    if (respData.hasOwnProperty("status")) {
      callback(respData);
    } else {
      callback({
        status: MESSAGES.RESPONSE_STATUS.success,
        message: MESSAGES.RESPONSE_STATUS.completed,
        data: respData
      });
    }
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
  }
}

/**
 * Logic:
 * Validate user input and check for listvalue
 * Depending on ListValue send response
 * @param {*} req
 * @param {*} res 
 * @param {*} callback 
 */
async function pendinglist(req, res, callback) {
  try {
    if (res.hasOwnProperty("errors")) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = await MerchantService.pendinglist(req.data);
    if (respData.hasOwnProperty("status")) {
      callback(respData);
    } else {
      callback({
        status: MESSAGES.RESPONSE_STATUS.success,
        message: MESSAGES.RESPONSE_STATUS.completed,
        total: respData.length,
        data: respData
      });
    }
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, data: error.message })
  }
}


/**
 * Logic:
 * Return all merchants
 * @param {*} req
 * @param {*} res 
 * @param {*} callback 
 */

async function merchantlist(req, res, callback) {
  try {
    if (res.hasOwnProperty("errors")) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = await MerchantService.merchantlist();
    if (respData.hasOwnProperty("status")) {
      callback(respData);
    } else {
      callback({
        status: MESSAGES.RESPONSE_STATUS.success,
        message: MESSAGES.RESPONSE_STATUS.completed,
        total: respData.count,
        data: respData.dataset
      });
    }
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, data: error.message })
  }
}

/**
 * Logic:
 * Return all denominations
 * 
 * @param {*} req
 * @param {*} res 
 * @param {*} callback 
 */
async function denominationlist(req, res, callback) {
  try {
    if (res.hasOwnProperty("errors")) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = [];
    DENOMINATIONS.forEach(element => {
      respData.push({ value: element.id, label: element.value });
    });
    callback({
      status: MESSAGES.RESPONSE_STATUS.success,
      message: MESSAGES.RESPONSE_STATUS.completed,
      data: respData
    });
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, data: error.message })
  }
}

module.exports = {
  create: Supervisor.superviseAction(create, new CreateMerchantValidator()),
  processNewMerchant: Supervisor.superviseAction(processNewMerchant, new RequestProcessing.NewRequestProcessingValidator()),
  updatechecker: Supervisor.superviseAction(updatechecker, new RequestProcessing.NewRequestProcessingValidator()),
  createNewMerchant,
  detail: Supervisor.superviseAction(detail, new CommonValidator.GetDetailByIdValidator()),
  search: Supervisor.superviseAction(search, new CommonValidator.SearchValidator()),
  updatemaker: Supervisor.superviseAction(updatemaker, new UpdateMerchantValidator()),
  deletemaker,
  deletechecker: Supervisor.superviseAction(deletechecker, new RequestProcessing.NewRequestProcessingValidator()),
  pendinglist: Supervisor.superviseAction(pendinglist, new CommonValidator.ListActorValidator()),
  merchantlist,
  denominationlist
}