const UpdateRetailerValidator = require('../validators/update-retailer').UpdateRetailerValidator
const CommonValidator = require("../validators/common-validator");
const Supervisor = require('../util/supervisor')
const RetailerService = require('../core/retailer')
const RequestProcessing = require("../validators/request-processing");
const ApprovalService = require('../core/approval');
const MESSAGES = require("../constant/messages");

async function detail(req, res, callback) {
  try {
    if (res.hasOwnProperty('errors')) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = await RetailerService.detail(req)
    if (respData.hasOwnProperty("status")) {
      callback(respData);
    } else {
      callback({
        status: MESSAGES.RESPONSE_STATUS.success,
        message: MESSAGES.RESPONSE_STATUS.completed,
        data: respData
      });
    }
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
  }
}

async function search(req, res, callback) {
  try {
    if (res.hasOwnProperty('errors')) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = await RetailerService.search(req)
    if (respData.hasOwnProperty("status")) {
      callback(respData);
    } else {
      callback({
        status: MESSAGES.RESPONSE_STATUS.success,
        message: MESSAGES.RESPONSE_STATUS.completed,
        data: respData
      });
    }
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
  }
}

/**
 * Logic:
 * Validate Retailer Data
 * Get RetailerId from Request Body
 *  if ID not found return RetailerId not found
 * Select respective Retailer if found
 * Create Retailer update request
 * Return success response
 * @param {*} req
 * @param {*} res
 * @param {*} callback
 */
async function updatemaker(req, res, callback) {
  try {
    if (res.hasOwnProperty('errors')) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = await RetailerService.updateRetailerRequest(req.data)
    if (respData.hasOwnProperty("status")) {
      callback(respData);
    } else {
      callback({
        status: MESSAGES.RESPONSE_STATUS.success,
        message: MESSAGES.RESPONSE_STATUS.completed,
        data: respData
      });
    }
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
  }
}

/**
 * Logic:
 * Get Request Id and applicable status to process the request
 * Validate the Data by Id and Status
 * @param {*} req
 * @param {*} res 
 * @param {*} callback 
 */
async function updatechecker(req, res, callback) {
  try {
    if (res.hasOwnProperty('errors')) {
      return callback({
        status: MESSAGES.RESPONSE_STATUS.failed,
        message: res.errors
      });
    }
    let respData = await ApprovalService.updateRetailer(req.data)
    if (respData.hasOwnProperty("status")) {
      callback(respData);
    } else {
      callback({
        status: MESSAGES.RESPONSE_STATUS.success,
        message: MESSAGES.RESPONSE_STATUS.completed,
        data: respData
      });
    }
  } catch (error) {
    callback({ status: MESSAGES.RESPONSE_STATUS.failed, message: error.message })
  }
}
module.exports = {
  detail: Supervisor.superviseAction(detail, new CommonValidator.GetDetailByIdValidator()),
  search: Supervisor.superviseAction(search, new CommonValidator.SearchByIDNameValidator()),
  updatemaker: Supervisor.superviseAction(updatemaker, new UpdateRetailerValidator()),
  updatechecker: Supervisor.superviseAction(updatechecker, new RequestProcessing.NewRequestProcessingValidator()),
}
