/* 
eastwest = true means internal calls, 
northsouth = true means external,
if both eastwest and northsouth are set then handle internal and external
*/
const channels = {
  commsexample: {
    northsouth: true,
    eastwest: true
  },

  commsresponseexample: {
    // fakekey: 'a',
    // prefixfunction: 'getprefix',
    // invalidationevents: ['random.event'],
    northsouth: true,
    eastwest: true
  }
}

module.exports = channels
