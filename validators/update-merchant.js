const ERROR_MESSAGE = require('../constant/messages').validate
const REGEX = require('../constant/variables').REGEX
const Merchant = require("./master/merchant").Merchant
const VALIDATERULE = require("../constant/validator-rule")
class UpdateMerchantValidator extends Merchant {
  constructor() {
    super();
    this.merchantid = VALIDATERULE.requirenumber;
  }
}

module.exports = {
  UpdateMerchantValidator
}
