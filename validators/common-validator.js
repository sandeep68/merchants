const ERROR_MESSAGE = require("../constant/messages").validate;
const ACTION = require("../constant/variables");
const VALIDATERULE = require("../constant/validator-rule")
class SearchValidator {
    constructor() {
        this.name = VALIDATERULE.requirestring;
    }
}

class SearchByIDNameValidator {
    constructor() {
        this.id = VALIDATERULE.requirenumber;
        this.name = {
            required: true,
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD
            }
        };
    }
}

class GetDetailByIdValidator {
    constructor() {
        this.id = VALIDATERULE.requirenumber;
    }
}

class DeleteActorValidator {
    constructor() {
        this.id = VALIDATERULE.requirenumber;

        this.type = {
            required: true,
            rules: {
                checkKeyExist: (value) => {
                    return ACTION.ACTORS.find(function (ob) { return ob.type == value; }) != undefined ? true : false;
                }
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                checkKeyExist: ERROR_MESSAGE.INVALID_REQUEST
            }
        };
    }
}

class ListActorValidator {
    constructor() {
        this.role = {
            required: true,
            rules: {
                checkKeyExist: (value) => {
                    return ACTION.ROLES.find(function (ob) { return ob.type == value; }) != undefined ? true : false;
                }
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                checkKeyExist: ERROR_MESSAGE.INVALID_REQUEST
            }
        };
        this.type = {
            required: true,
            rules: {
                checkKeyExist: (value) => {
                    return ACTION.ACTORS.find(function (ob) { return ob.type == value; }) != undefined ? true : false;
                }
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                checkKeyExist: ERROR_MESSAGE.INVALID_REQUEST
            }
        };
    }
}

module.exports = {
    SearchValidator,
    GetDetailByIdValidator,
    SearchByIDNameValidator,
    DeleteActorValidator,
    ListActorValidator
};