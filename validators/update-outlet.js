const ERROR_MESSAGE = require('../constant/messages').validate
const REGEX = require('../constant/variables').REGEX
const Outlet = require("./master/outlet").Outlet
const VALIDATERULE = require("../constant/validator-rule")
class UpdateOutletValidator extends Outlet {
  constructor() {
    super();
    this.outletid = VALIDATERULE.requirenumber;
  }
}
module.exports = {
  UpdateOutletValidator
}
