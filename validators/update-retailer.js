const ERROR_MESSAGE = require('../constant/messages').validate
const REGEX = require('../constant/variables').REGEX
const Retailer = require("./master/retailer").Retailer
const VALIDATERULE = require("../constant/validator-rule")
class UpdateRetailerValidator extends Retailer {
  constructor() {
    super();
    this.retailerid = VALIDATERULE.requirenumber;
  }
}

module.exports = {
  UpdateRetailerValidator
}
