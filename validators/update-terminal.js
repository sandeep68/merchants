const ERROR_MESSAGE = require('../constant/messages').validate
const REGEX = require('../constant/variables').REGEX
const Terminal = require("./master/terminal").Terminal
const VALIDATERULE = require("../constant/validator-rule")
class UpdateTerminalValidator extends Terminal {
    constructor() {
        super();
        this.terminalid = VALIDATERULE.requirenumber;
    }
}

module.exports = {
    UpdateTerminalValidator
}
