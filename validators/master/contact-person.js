const ERROR_MESSAGE = require('../../constant/messages').validate
const REGEX = require('../../constant/variables').REGEX
class ContactPerson {
    constructor() {
        this.contactname = {
            required: true,
            rules: {
                notEmpty: true
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY
            }
        }
        this.phone = {
            required: true,
            rules: {
                notEmpty: true
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY
            }
        }
        this.email = {
            required: true,
            rules: {
                match: REGEX.EMAIL
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                match: ERROR_MESSAGE.INVALID_REQUEST
            }
        }
    }
}

module.exports = {
    ContactPerson
}