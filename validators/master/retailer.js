const ERROR_MESSAGE = require('../../constant/messages').validate
const REGEX = require('../../constant/variables').REGEX
const ContactPerson = require("./contact-person").ContactPerson
const VALIDATERULE = require("../../constant/validator-rule")
class Retailer extends ContactPerson {
    constructor() {
        super();
        this.name = {
            required: true,
            type: 'string',
            rules: {
                notEmpty: true
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY,
                type: ERROR_MESSAGE.INVALID_REQUEST
            }
        }
        this.gstno = {
            required: true,
            rules: {
                notEmpty: true,
                match: REGEX.GSTCERTIFICATE
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY,
                match: ERROR_MESSAGE.INVALID_REQUEST
            }
        }
        this.gstaddress = {
            required: true,
            type: 'string',
            rules: {
                notEmpty: true
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                type: ERROR_MESSAGE.INVALID_REQUEST,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY
            }
        }
        this.categoryid = VALIDATERULE.requirenumber
        this.subcategoryid = VALIDATERULE.requirenumber
        this.servicefee = {
            required: true,
            rules: {
                notEmpty: true
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY
            }
        }
        this.pan = {
            required: true,
            type: 'string',
            rules: {
                notEmpty: true
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                type: ERROR_MESSAGE.INVALID_REQUEST,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY
            }
        }
        this.cheque = {
            required: true,
            type: 'string',
            rules: {
                notEmpty: true
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                type: ERROR_MESSAGE.INVALID_REQUEST,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY
            }
        }
        this.prefix = {
            required: true,
            type: 'string',
            rules: {
                notEmpty: true
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                type: ERROR_MESSAGE.INVALID_REQUEST,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY
            }
        }
        this.gstcertificate = {
            required: true,
            type: 'string',
            rules: {
                notEmpty: true
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                type: ERROR_MESSAGE.INVALID_REQUEST,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY
            }
        }
        this.bankname = {
            required: false
        }
        this.accountnumber = {
            required: false
        }
        this.ifsc = {
            required: false
        }
    }
}

module.exports = {
    Retailer
}

