const ERROR_MESSAGE = require('../../constant/messages').validate
const REGEX = require('../../constant/variables').REGEX
const ContactPerson = require("./contact-person").ContactPerson
class Merchant extends ContactPerson {
    constructor() {
        super();
        this.name = {
            required: true,
            type: 'string',
            rules: {
                notEmpty: true
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY,
                type: ERROR_MESSAGE.INVALID_REQUEST
            }
        }
        this.address = {
            required: true,
            type: 'string',
            rules: {
                notEmpty: true
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY,
                type: ERROR_MESSAGE.INVALID_REQUEST
            }
        }
        this.pan = {
            required: true,
            type: 'string',
            rules: {
                match: REGEX.PAN
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                type: ERROR_MESSAGE.INVALID_REQUEST,
                match: ERROR_MESSAGE.INVALID_PAN
            }
        }
    }
}

module.exports = {
    Merchant
}