const ERROR_MESSAGE = require('../../constant/messages').validate
const REGEX = require('../../constant/variables').REGEX
class Terminal {
    constructor() {
        this.terminal_id = {
            required: true,
            rules: {
                notEmpty: true
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY
            }
        }
        this.type = {
            required: true,
            rules: {
                notEmpty: true
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY
            }
        }
    }
}

module.exports = {
    Terminal
}
