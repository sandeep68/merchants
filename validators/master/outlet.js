const ERROR_MESSAGE = require('../../constant/messages').validate
const REGEX = require('../../constant/variables').REGEX
const ContactPerson = require("./contact-person").ContactPerson
class Outlet extends ContactPerson {
    constructor() {
        super();
        this.name = {
            required: true,
            type: 'string',
            rules: {
                notEmpty: true
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY,
                type: ERROR_MESSAGE.INVALID_REQUEST
            }
        }
        this.area = {
            required: true,
            type: 'string',
            rules: {
                notEmpty: true
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY,
                type: ERROR_MESSAGE.INVALID_REQUEST
            }
        }
        this.city = {
            required: true,
            type: 'string',
            rules: {
                notEmpty: true
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY,
                type: ERROR_MESSAGE.INVALID_REQUEST
            }
        }
        this.state = {
            required: true,
            type: 'string',
            rules: {
                notEmpty: true
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY,
                type: ERROR_MESSAGE.INVALID_REQUEST
            }
        }
        this.region = {
            rules: {
                oneOf: ['Andhra Pradesh & Telangana States', 'South', 'East', 'West', 'Mumbai', 'North', 'NCR']
            },
            errors: {
                oneOf: ERROR_MESSAGE.INVALID_REQUEST
            }
        }
        this.pincode = {
            required: true,
            rules: {
                notEmpty: true,
                match: REGEX.PINCODE
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY,
                match: ERROR_MESSAGE.INVALID_PINCODE
            }
        }
        this.latitude = {
            required: true,
            rules: {
                notEmpty: true,
                match: REGEX.LATITUDE
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY,
                match: ERROR_MESSAGE.INVALID_LATITUDE
            }
        }
        this.longitude = {
            required: true,
            rules: {
                notEmpty: true,
                match: REGEX.LONGITUDE
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY,
                match: ERROR_MESSAGE.INVALID_LONGITUDE
            }
        }
        this.sapsitecode = {
            required: true,
            rules: {
                notEmpty: true
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY
            }
        }
        this.storeid = {
            required: true,
            rules: {
                notEmpty: true
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY
            }
        }
        this.pos = {
            required: true,
            rules: {
                notEmpty: true,
                match: REGEX.BOOLEAN_NUM
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY,
                match: ERROR_MESSAGE.INVALID_BOOLEAN_NUM
            }
        }
        this.poynt = {
            required: true,
            rules: {
                notEmpty: true,
                match: REGEX.BOOLEAN_NUM
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY,
                match: ERROR_MESSAGE.INVALID_BOOLEAN_NUM
            }
        }
        this.web = {
            required: true,
            rules: {
                notEmpty: true,
                match: REGEX.BOOLEAN_NUM
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY,
                match: ERROR_MESSAGE.INVALID_BOOLEAN_NUM
            }
        }
        this.gstno = {
            required: true,
            rules: {
                notEmpty: true,
                match: REGEX.GSTCERTIFICATE
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY,
                match: ERROR_MESSAGE.INVALID_REQUEST
            }
        }
    }
}
module.exports = {
    Outlet
}
