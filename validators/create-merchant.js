const ERROR_MESSAGE = require("../constant/messages").validate;
const REGEX = require("../constant/variables").REGEX;
const variables = require("../constant/variables");
let Retailer = require("./master/retailer").Retailer;
let Outlet = require("./master/outlet").Outlet;
let Terminal = require("./master/terminal").Terminal;
const VALIDATERULE = require("../constant/validator-rule")
class CreateMerchantValidator {
  constructor() {
    this.id = {
      required: false
    };
    this.name = {
      required: true,
      type: 'string',
      rules: {
        notEmpty: true
      },
      errors: {
        required: ERROR_MESSAGE.REQUIRED_FIELD,
        notEmpty: ERROR_MESSAGE.NOT_EMPTY,
        type: ERROR_MESSAGE.INVALID_REQUEST
      }
    };
    this.address = {
      required: true,
      type: 'string',
      rules: {
        notEmpty: true
      },
      errors: {
        required: ERROR_MESSAGE.REQUIRED_FIELD,
        notEmpty: ERROR_MESSAGE.NOT_EMPTY,
        type: ERROR_MESSAGE.INVALID_REQUEST
      }
    };
    this.pan = {
      required: true,
      type: 'string',
      rules: {
        notEmpty: true
      },
      errors: {
        required: ERROR_MESSAGE.REQUIRED_FIELD,
        notEmpty: ERROR_MESSAGE.NOT_EMPTY,
        type: ERROR_MESSAGE.INVALID_REQUEST
      }
    };
    this.contactname = {
      required: true,
      rules: {
        notEmpty: true
      },
      errors: {
        required: ERROR_MESSAGE.REQUIRED_FIELD,
        notEmpty: ERROR_MESSAGE.NOT_EMPTY
      }
    };
    this.phone = {
      required: true,
      rules: {
        notEmpty: true,
        match: REGEX.MOBILE
      },
      errors: {
        required: ERROR_MESSAGE.REQUIRED_FIELD,
        match: ERROR_MESSAGE.INVALID_REQUEST
      }
    };
    this.email = {
      required: true,
      rules: {
        match: REGEX.EMAIL
      },
      errors: {
        required: ERROR_MESSAGE.REQUIRED_FIELD,
        match: ERROR_MESSAGE.INVALID_REQUEST
      }
    };
    this.retailer = {
      required: true,
      model: {
        id: {
          required: false
        }
      },
      errors: {
        required: ERROR_MESSAGE.REQUIRED_FIELD,
      }
    };

    Object.assign(this.retailer.model, new Retailer());
    this.outlet = {
      required: true,
      model: {
        id: {
          required: false
        }
      },
      errors: {
        required: ERROR_MESSAGE.REQUIRED_FIELD
      }
    };
    Object.assign(this.outlet.model, new Outlet());
    this.terminals = {
      required: true,
      errors: {
        required: ERROR_MESSAGE.REQUIRED_FIELD
      }
    };
    this.terminals["model"] = Object.assign(new Terminal());
    this.cpg = {
      required: true,
      type: "array",
      model: {
        form: {
          required: true,
          rules: {
            notEmpty: true,
            oneOf: variables.CPG_FROM
          },
          errors: {
            required: ERROR_MESSAGE.REQUIRED_FIELD,
            notEmpty: ERROR_MESSAGE.NOT_EMPTY,
            oneOf: ERROR_MESSAGE.INVALID_REQUEST
          }
        },
        denomination: VALIDATERULE.requirenumber,
        ismerchant: {
          required: false,
          rules: {
            oneOf: ["true", "false"]
          },
          errors: {
            oneOf: ERROR_MESSAGE.INVALID_REQUEST
          }
        },
        isspc: {
          required: false,
          rules: {
            oneOf: variables.BOOLEAN_CHAR
          },
          errors: {
            oneOf: ERROR_MESSAGE.INVALID_REQUEST
          }
        },
        isdeferred: {
          required: false,
          rules: {
            oneOf: variables.BOOLEAN_CHAR
          },
          errors: {
            oneOf: ERROR_MESSAGE.INVALID_REQUEST
          }
        }
      },
      errors: {
        required: ERROR_MESSAGE.REQUIRED_FIELD
      }
    };

  }


}

module.exports = {
  CreateMerchantValidator
}
