const ERROR_MESSAGE = require("../constant/messages").validate;
const ACTION = require("../constant/variables");
const VALIDATERULE = require("../constant/validator-rule")
class NewRequestProcessingValidator {
    constructor() {
        this.id = VALIDATERULE.requirenumber;
        this.status = {
            required: true,
            rules: {
                notEmpty: true,
                checkKeyExist: (value) => {
                    return ACTION.APPROVALS_ACTION_STATUS.REQUESTED.applicable.includes(value)
                }
            },
            errors: {
                required: ERROR_MESSAGE.REQUIRED_FIELD,
                notEmpty: ERROR_MESSAGE.NOT_EMPTY,
                checkKeyExist: ERROR_MESSAGE.INVALID_REQUEST
            }
        };
    }
}

module.exports = {
    NewRequestProcessingValidator
};