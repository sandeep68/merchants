//constant
const ACTION = require("../constant/variables");
const MESSAGES = require("../constant/messages");
//core-Service
const MerchantService = require("../core/merchant");
const RetailerService = require("../core/retailer");
const OutletService = require("../core/outlet");
const TerminalService = require("../core/terminal");
//models
const PendingApproval = require("../persistence/pendingapproval");
const Merchant = require("../persistence/merchant");
const Retailer = require("../persistence/retailer");
const Outlet = require("../persistence/outlet");
const Terminal = require("../persistence/terminal");

/**
 * Process The New Merchant Request 
 */
const createMerchant = async (payload) => {
    let { id, status } = payload;
    let approvalRequest = { id, status };
    return await _validateNewMerchantApproval(approvalRequest);
}

const _validateNewMerchantApproval = async (data) => {
    var { id, status } = data;
    let pendingapprovalObj = {
        status: ACTION.APPROVALS_ACTION_STATUS.REQUESTED.value,
        id: id
    };
    let merchantApproval = await PendingApproval.select({ "andOp": pendingapprovalObj, offset: 0, limit: 1 });
    if (merchantApproval.length) {
        if (status == ACTION.APPROVALS_ACTION_STATUS.APPROVED.value) {
            var approvalId = id;
            let { request } = merchantApproval[0];
            //Validate For Unique StoreId and Prefix
            let validateMerchant = await MerchantService.validateMerchantPayload(JSON.parse(request));
            if (validateMerchant) {
                return validateMerchant;
            }
            let response = await MerchantService.requestNewMerchantProcess(JSON.parse(request));
            if (response.hasOwnProperty("id")) {
                await PendingApproval.update({ status: ACTION.APPROVALS_ACTION_STATUS.APPROVED.value, merchantid: response.id }, { id: approvalId });
            } else {
                await PendingApproval.update({ status: ACTION.APPROVALS_ACTION_STATUS.FAILED.value }, { id: approvalId });
            }
            return { id };
        } else {
            await PendingApproval.update({ status }, { id });
            return { id };
        }
    } else {
        return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.INVALID_REQUEST };
    }
}

/**
 * Process The Modify Merchant Request 
 */
const updateMerchant = async (payload) => {
    let { id, status } = payload;
    let approvalRequest = { id, status };
    return await _validateUpdateMerchantApproval(approvalRequest);
}
const _validateUpdateMerchantApproval = async (data) => {
    var { id, status } = data;
    let pendingapprovalObj = {
        status: ACTION.APPROVALS_ACTION_STATUS.REQUESTED.value,
        id: id
    };
    let merchantApproval = await PendingApproval.select({ "andOp": pendingapprovalObj, offset: 0, limit: 1 });
    if (merchantApproval.length) {
        if (status == ACTION.APPROVALS_ACTION_STATUS.APPROVED.value) {
            let { request, merchantid } = merchantApproval[0];
            if (merchantid) {
                let response = await MerchantService.updateMerchant(JSON.parse(request));
                if (response.length > 0) {
                    await PendingApproval.update({ status: ACTION.APPROVALS_ACTION_STATUS.APPROVED.value }, { id });
                } else {
                    await PendingApproval.update({ status: ACTION.APPROVALS_ACTION_STATUS.FAILED.value }, { id });
                }
                return { id };
            } else {
                await PendingApproval.update({ status }, { id });
                return { id };
            }
        } else {
            await PendingApproval.update({ status }, { id });
            return { id };
        }
    } else {
        return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.INVALID_REQUEST };
    }
}
/**
 * Process The Modify Retailer Request 
 */
const updateRetailer = async (payload) => {
    let { id, status } = payload;
    let approvalRequest = { id, status };
    return await _validateUpdateRetailerApproval(approvalRequest);
}
const _validateUpdateRetailerApproval = async (data) => {
    var { id, status } = data;
    let pendingapprovalObj = {
        status: ACTION.APPROVALS_ACTION_STATUS.REQUESTED.value,
        id: id
    };
    let retailersApproval = await PendingApproval.select({ "andOp": pendingapprovalObj, offset: 0, limit: 1 });
    if (retailersApproval.length) {
        if (status == ACTION.APPROVALS_ACTION_STATUS.APPROVED.value) {
            let { request, retailerid } = retailersApproval[0];
            if (retailerid) {
                let response = await RetailerService.updateRetailer(JSON.parse(request));
                if (response.length > 0) {
                    await PendingApproval.update({ status: ACTION.APPROVALS_ACTION_STATUS.APPROVED.value }, { id });
                } else {
                    await PendingApproval.update({ status: ACTION.APPROVALS_ACTION_STATUS.FAILED.value }, { id });
                }
                return { id };
            } else {
                return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.INVALID_REQUEST };
            }
        } else {
            await PendingApproval.update({ status }, { id });
            return { id };
        }
    } else {
        return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.INVALID_REQUEST };
    }
}
/**
 * Process The Modify Outlet Request 
 */
const updateOutlet = async (payload) => {
    let { id, status } = payload;
    let approvalRequest = { id, status };
    return await _validateUpdateOutletApproval(approvalRequest);
}
const _validateUpdateOutletApproval = async (data) => {
    var { id, status } = data;
    let pendingapprovalObj = {
        status: ACTION.APPROVALS_ACTION_STATUS.REQUESTED.value,
        id: id
    };
    let outletsApproval = await PendingApproval.select({ "andOp": pendingapprovalObj, offset: 0, limit: 1 });
    if (outletsApproval.length) {
        if (status == ACTION.APPROVALS_ACTION_STATUS.APPROVED.value) {

            let { request, outletid } = outletsApproval[0];
            if (outletid) {
                let response = await OutletService.updateOutlet(JSON.parse(request));
                if (response.length > 0) {
                    await PendingApproval.update({ status: ACTION.APPROVALS_ACTION_STATUS.APPROVED.value }, { id });
                } else {
                    await PendingApproval.update({ status: ACTION.APPROVALS_ACTION_STATUS.FAILED.value }, { id });
                }
                return { id };
            } else {
                return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.INVALID_REQUEST };
            }
        } else {
            await PendingApproval.update({ status }, { id });
            return { id };
        }
    } else {
        return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.INVALID_REQUEST };
    }
}

/**
 * Process The Modify Terminal Request 
 */
const updateTerminal = async (payload) => {
    let { id, status } = payload;
    let approvalRequest = { id, status };
    return await _validateUpdateTerminalApproval(approvalRequest);
}
const _validateUpdateTerminalApproval = async (data) => {
    var { id, status } = data;
    let pendingapprovalObj = {
        status: ACTION.APPROVALS_ACTION_STATUS.REQUESTED.value,
        id: id
    };
    let terminalsApproval = await PendingApproval.select({ "andOp": pendingapprovalObj, offset: 0, limit: 1 });
    if (terminalsApproval.length) {
        if (status == ACTION.APPROVALS_ACTION_STATUS.APPROVED.value) {
            let { request, terminalid } = terminalsApproval[0];
            if (terminalid) {
                let response = await TerminalService.updateTerminal(JSON.parse(request));
                if (response.length > 0) {
                    await PendingApproval.update({ status: ACTION.APPROVALS_ACTION_STATUS.APPROVED.value }, { id });
                } else {
                    await PendingApproval.update({ status: ACTION.APPROVALS_ACTION_STATUS.FAILED.value }, { id });
                }
                return { id };
            } else {
                return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.INVALID_REQUEST };
            }
        } else {
            await PendingApproval.update({ status }, { id });
            return { id };
        }
    } else {
        return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.INVALID_REQUEST };
    }
}

/** 
 * Delete Maker For all the stake holder
*/
const deleteActorsMaker = async (payload) => {
    let validateReq = payload;
    let validateResp = await _validateDeleteActorsMaker(validateReq);
    if (validateResp.hasOwnProperty("status")) {
        return validateResp;
    } else {
        let approvalPayload = {
            action: ACTION.APPROVALS_ACTION.DELETE.value,
            status: ACTION.APPROVALS_ACTION_STATUS.REQUESTED.value,
            remarks: validateResp.remarks
        };
        let { id, type } = validateResp;
        let actor = ACTION.ACTORS.find(function (ob) { return ob.type == type; });
        approvalPayload[actor.approvalId] = id;
        var actorExist = [];
        switch (type) {
            case "merchant":
                actorExist = await Merchant.select({ "andOp": { id }, offset: 0, limit: 1 });
                break;
            case "retailer":
                actorExist = await Retailer.select({ "andOp": { id }, offset: 0, limit: 1 });
                break;
            case "outlet":
                actorExist = await Outlet.select({ "andOp": { id }, offset: 0, limit: 1 });
                break;
            case "terminal":
                actorExist = await Terminal.select({ "andOp": { id }, offset: 0, limit: 1 });
                break;
            default:
                actorExist = [];
        }

        if (actorExist.length) {
            let approvalResult = await PendingApproval.create(approvalPayload);
            return { "id": approvalResult["insertId"] }
        } else {
            return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.DATA_NOT_FOUND };
        }
    }
}
const _validateDeleteActorsMaker = async (validateData) => {
    let { merchantid, retailerid, outletid, terminalid, remarks } = validateData;
    let validData = { merchantid, retailerid, outletid, terminalid, remarks };
    Object.keys(validData).forEach(key => validData[key] === undefined && delete validData[key])
    let validate = false;
    let actorPayload = {};
    if (typeof validData == "object" && Object.keys(validData).length == 2 && typeof remarks != undefined) {

        if (typeof merchantid != undefined && !isNaN(merchantid)) {
            validate = true;
            actorPayload = { id: merchantid, type: "merchant" };
        }
        if (typeof retailerid != undefined && !isNaN(retailerid) && !Object.keys(actorPayload).length) {
            validate = true;
            actorPayload = { id: retailerid, type: "retailer" };
        }
        if (typeof outletid != undefined && !isNaN(outletid) && !Object.keys(actorPayload).length) {
            validate = true;
            actorPayload = { id: outletid, type: "outlet" };
        }
        if (typeof terminalid != undefined && !isNaN(terminalid) && !Object.keys(actorPayload).length) {
            validate = true;
            actorPayload = { id: terminalid, type: "terminal" };
        }

    }


    if (validate) {
        actorPayload["remarks"] = remarks;
        return actorPayload;
    } else {
        return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.INVALID_REQUEST };
    }
}

/** 
 * Delete Checker For all the stake holder
*/
const deleteActorsChecker = async (payload) => {
    let { id, status } = payload;
    let pendingapprovalObj = {
        status: ACTION.APPROVALS_ACTION_STATUS.REQUESTED.value,
        id: id,
        action: ACTION.APPROVALS_ACTION.DELETE.value
    };
    let deleteApproval = await PendingApproval.select({ "andOp": pendingapprovalObj, offset: 0, limit: 1 });
    if (deleteApproval.length) {
        let validateData = await _validateDeleteActorsChecker(deleteApproval[0]);
        if (validateData) {
            await PendingApproval.update({ status }, { id });
            return { id };
        } else {
            return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.INVALID_REQUEST };
        }
    } else {
        return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.DATA_NOT_FOUND };
    }
}

const _validateDeleteActorsChecker = async (options) => {
    let { merchantid, retailerid, outletid, terminalid } = options;
    let validateData = options;
    let deleted = false;
    var respDeleted = {};
    switch (true) {
        case validateData.merchantid > 0:
            respDeleted = await Merchant.delete({ id: merchantid });
            break;
        case validateData.retailerid > 0:
            respDeleted = await Retailer.delete({ id: retailerid });
            break;
        case validateData.outletid > 0:
            respDeleted = await Outlet.delete({ id: outletid });
            break;
        case validateData.terminalid > 0:
            respDeleted = await Terminal.delete({ id: terminalid });
            break;
        default:
            respDeleted = "";
    }
    return (respDeleted.hasOwnProperty("changedRows")) ? ((respDeleted.changedRows) ? true : false) : false;

}
module.exports = {
    createMerchant,
    updateMerchant,
    updateRetailer,
    updateOutlet,
    updateTerminal,
    deleteActorsMaker,
    deleteActorsChecker
}