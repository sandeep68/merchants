//Builder
const UpdateObj = require('../builder/Request').UpdateObj
const ObjectUtils = require("../util/objectUtils")
//Constant
const ACTION = require("../constant/variables");
const MESSAGES = require("../constant/messages");
//Models
const Terminal = require('../persistence/terminal')
const PendingApproval = require("../persistence/pendingapproval");

const updateTerminal = async data => {
    // Process the req data and create object
    let payload = data
    let updateTerminalData = new UpdateObj()
    let updateTerminalObj = updateTerminalData.updateTerminalObj(payload)
    // Fire SQL Queries
    let updateTerminal = await Terminal.update(updateTerminalObj, { id: updateTerminalObj.terminalid })
    // Return Result
    return updateTerminal
}

const detail = async (payload) => {
    let { id } = payload;
    let terminal = await Terminal.select({ "andLikeOp": { id }, offset: 0, limit: 1 });
    if (terminal.length) {
        return terminal[0];
    } else {
        return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.DATA_NOT_FOUND };
    }
}

const list = async () => {
    let terminal = await Terminal.select();
    if (terminal.length) {
        return terminal;
    } else {
        return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.DATA_NOT_FOUND };
    }
}

const search = async (payload) => {
    let { name } = payload;
    let terminals = await Terminal.select({ "andLikeOp": { "name": "%" + name + "%" }, offset: 0, limit: 10 });
    if (terminals.length) {
        let terminal = [];
        terminals.forEach(element => {
            terminal.push({ value: element.id, label: element.name });
        });
        return terminal;
    } else {
        return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.DATA_NOT_FOUND };
    }
}

const updateTerminalRequest = async (data) => {
    let payload = data
    let { managerid } = data;
    let updateTerminalData = new UpdateObj()
    let updateTerminalObj = updateTerminalData.updateTerminalObj(payload)
    let terminals = await Terminal.select({ "andOp": { "id": updateTerminalObj.terminalid }, offset: 0, limit: 1 });
    if (terminals.length) {
        let approvalData = {
            action: ACTION.APPROVALS_ACTION.EDIT.value,
            terminalid: updateTerminalObj.terminalid,
            request: JSON.stringify(updateTerminalObj),
            managerid: managerid,
            status: ACTION.APPROVALS_ACTION_STATUS.REQUESTED.value
        };
        let approvalResult = await PendingApproval.create(approvalData);
        return { "id": approvalResult["insertId"] }
    } else {
        return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.DATA_NOT_FOUND };
    }
}
module.exports = {
    detail,
    search,
    list,
    updateTerminal,
    updateTerminalRequest
}