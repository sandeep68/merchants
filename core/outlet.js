//Builder
const UpdateObj = require('../builder/Request').UpdateObj
const ObjectUtils = require("../util/objectUtils")
//Constant
const ACTION = require("../constant/variables");
const MESSAGES = require("../constant/messages");
//Models
const Outlet = require('../persistence/outlet')
const PendingApproval = require("../persistence/pendingapproval");

const updateOutlet = async data => {
  // Process the req data and create object
  let payload = data
  let updateOutletData = new UpdateObj()
  let updateOutletObj = updateOutletData.updateOutletObj(payload)
  // Fire SQL Queries
  let updateOutlet = await Outlet.updateOutlet(updateOutletObj)
  // Return Result
  return updateOutlet
}

const detail = async (payload) => {
  let { id } = payload;
  let outlet = await Outlet.select({ "andOp": { id }, offset: 0, limit: 1 });
  if (outlet.length) {
    return outlet[0];
  } else {
    return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.DATA_NOT_FOUND };
  }
}

const list = async () => {
  let outlet = await Outlet.select();
  if (outlet.length) {
    return outlet;
  } else {
    return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.DATA_NOT_FOUND };
  }
}

const search = async (payload) => {
  let { id, name } = payload;
  let outlets = await Outlet.select({ "andOp": { retailerid: id }, "andLikeOp": { "name": "%" + name + "%" }, offset: 0, limit: 10 });
  if (outlets.length) {
    let outlet = [];
    outlets.forEach(element => {
      outlet.push({ value: element.id, label: element.name });
    });
    return outlet;
  } else {
    return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.DATA_NOT_FOUND };
  }
}

const updateOutletRequest = async (data) => {
  let payload = data;
  let { managerid } = payload;
  let updateOutlet = new UpdateObj();
  let outletPayload = updateOutlet.updateOutletObj(payload);
  let outlets = await Outlet.select({ "andOp": { "id": outletPayload.outletid }, offset: 0, limit: 1 });
  if (outlets.length) {
    let outletStore = await Outlet.select({
      "andOp": { "storeid": outletPayload.storeid, "retailerid": outlets[0].retailerid },
      "andOpNe": { "id": outletPayload.outletid },
      offset: 0, limit: 1
    });
    if (!outletStore.length) {
      let approvalData = {
        action: ACTION.APPROVALS_ACTION.EDIT.value,
        outletid: outletPayload.outletid,
        request: JSON.stringify(outletPayload),
        managerid: managerid,
        status: ACTION.APPROVALS_ACTION_STATUS.REQUESTED.value
      };
      let approvalResult = await PendingApproval.create(approvalData);
      return { "id": approvalResult["insertId"] }
    } else {
      return { outlet_storeid: MESSAGES.validate.DUPLICATE_ENTRY };
    }
  } else {
    return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.DATA_NOT_FOUND };
  }
}
module.exports = {
  updateOutlet,
  detail,
  search,
  list,
  updateOutletRequest
}
