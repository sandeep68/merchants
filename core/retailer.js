//Builder
const UpdateObj = require('../builder/Request').UpdateObj
//constant
const MESSAGES = require("../constant/messages");
const ACTION = require("../constant/variables");
//Models
const Retailer = require('../persistence/retailer')
const Bank = require('../persistence/bankdetails')
const PendingApproval = require("../persistence/pendingapproval")

const updateRetailer = async data => {
  // Process the req data and create object
  let payload = data
  let updateRetailerData = new UpdateObj()
  let updateRetailer = updateRetailerData.updateRetailerObj(payload)
  // Get SQL Queries
  let getRetailerUpdateQuery = await Retailer.updateRetailer(updateRetailer)
  let getBankUpdateQuery = await Bank.updateBank(updateRetailer)
  // Fire Query in Transaction
  let updateAll = await Retailer.updateRetailerBank(getRetailerUpdateQuery, getBankUpdateQuery)
  // Return Result
  return updateAll
}

const detail = async (payload) => {
  let { id } = payload;
  let retailer = await Retailer.selectBankDetail({ "andOp": { "ret.id": id }, offset: 0, limit: 1 });
  if (retailer.length) {
    return retailer[0];
  } else {
    return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.DATA_NOT_FOUND };
  }
}

const search = async (payload) => {
  let { id, name } = payload;
  let retailers = await Retailer.select({ "andOp": { merchantid: id }, "andLikeOp": { "name": "%" + name + "%" }, offset: 0, limit: 10 });
  if (retailers.length) {
    let retailer = [];
    retailers.forEach(element => {
      retailer.push({ value: element.id, label: element.name });
    });
    return retailer;
  } else {
    return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.DATA_NOT_FOUND };
  }
}

const updateRetailerRequest = async (data) => {
  let payload = data;
  let { managerid } = data;
  let updateRetailer = new UpdateObj();
  let retailerPayload = updateRetailer.updateRetailerObj(payload);
  let retailers = await Retailer.select({ "andOp": { "id": retailerPayload.retailerid }, offset: 0, limit: 1 });
  if (retailers.length) {
    let retailerPrefix = await Retailer.select({
      "andOp": { "prefix": retailerPayload.prefix },
      "andOpNe": { "id": retailerPayload.retailerid },
      offset: 0, limit: 1
    });
    if (retailerPrefix.length) {
      return { prefix: MESSAGES.validate.DUPLICATE_ENTRY };
    }
    let approvalData = {
      action: ACTION.APPROVALS_ACTION.EDIT.value,
      retailerid: retailerPayload.retailerid,
      request: JSON.stringify(retailerPayload),
      managerid: managerid,
      status: ACTION.APPROVALS_ACTION_STATUS.REQUESTED.value
    };
    let approvalResult = await PendingApproval.create(approvalData);
    return { "id": approvalResult["insertId"] }
  } else {
    return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.DATA_NOT_FOUND };
  }
}


module.exports = {
  updateRetailer,
  detail,
  search,
  updateRetailerRequest
}
