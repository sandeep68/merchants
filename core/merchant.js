//Constant
const ACTION = require("../constant/variables");
const MESSAGES = require("../constant/messages");
//Builder
const CreateMerchant = require("../builder/Request").CreateMerchant;
const UpdateObj = require("../builder/Request").UpdateObj;

//Models
const Merchant = require("../persistence/merchant");
const Retailer = require("../persistence/retailer");
const Cpg = require("../persistence/cpg");
const Cpghead = require("../persistence/cpghead");
const Outlet = require("../persistence/outlet");
const Terminal = require("../persistence/terminal");
const BankDetails = require("../persistence/bankdetails");
const PendingApproval = require("../persistence/pendingapproval");


const createMerchantRequest = async (data) => {
    let payload = data;
    let { managerid } = payload;
    let createMerchant = new CreateMerchant();
    let merchantPayload = createMerchant.create(payload);
    let validateMerchant = await validateMerchantPayload(merchantPayload);
    if (!validateMerchant) {
        for (let i = 0; i < merchantPayload.cpg.length; i++) {
            let cpgElem = merchantPayload.cpg[i];
            let validateDenom = ACTION.DENOMINATIONS.find((d) => { return d.value == cpgElem.denomination })
            merchantPayload.cpg[i]["denominationid"] = validateDenom.id;
        }
        let approvalData = {
            action: ACTION.APPROVALS_ACTION.ADD.value,
            merchantid: -1,
            request: JSON.stringify(merchantPayload),
            managerid: managerid,
            status: ACTION.APPROVALS_ACTION_STATUS.REQUESTED.value
        };
        let approvalResult = await PendingApproval.create(approvalData);
        return { "id": approvalResult["insertId"] };
    } else {
        return validateMerchant;
    }
}
const updateMerchantRequest = async (data) => {
    let payload = data;
    let { managerid } = payload;
    let updateMerchant = new UpdateObj();
    let merchantPayload = updateMerchant.updateMerchantObj(payload);
    let merchants = await Merchant.select({ "andOp": { "id": merchantPayload.merchantid }, offset: 0, limit: 1 });
    if (merchants.length) {
        let approvalData = {
            action: ACTION.APPROVALS_ACTION.EDIT.value,
            merchantid: merchantPayload.merchantid,
            request: JSON.stringify(merchantPayload),
            managerid: managerid,
            status: ACTION.APPROVALS_ACTION_STATUS.REQUESTED.value
        };
        let approvalResult = await PendingApproval.create(approvalData);
        return { "id": approvalResult["insertId"] }
    } else {
        return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.DATA_NOT_FOUND };
    }
}
const requestNewMerchantProcess = async (merchantPayload) => {
    //create merchant 
    let cpgmerchantflag = false;
    if (!merchantPayload.hasOwnProperty("id")) {
        merchantPayload["isactive"] = "Y";
        let merchantResp = await Merchant.create(merchantPayload);
        merchantPayload["id"] = merchantResp["insertId"];
        cpgmerchantflag = true;
    }
    //create retailer
    let cpgretailerflag = false;
    merchantPayload["retailer"]["merchantid"] = merchantPayload["id"];
    if (!merchantPayload.retailer.hasOwnProperty("id")) {
        let retailerResp = await Retailer.create(merchantPayload.retailer);
        merchantPayload["retailer"]["id"] = retailerResp["insertId"];
        let getRetailer = await Retailer.select({ "andOp": { id: retailerResp["insertId"] } });
        merchantPayload["retailer"]["bin"] = getRetailer[0]["bin"];
        merchantPayload["retailer"]["bookletbin"] = getRetailer[0]["bookletbin"];
        cpgretailerflag = true;
    }
    //create bankdetails
    merchantPayload["bank"]["retailerid"] = merchantPayload["retailer"]["id"];
    let { accountnumber, bankname, ifsc } = merchantPayload["bank"];
    if (typeof accountnumber != "undefined" && typeof bankname != "undefined" && typeof ifsc != "undefined") {
        await BankDetails.create(merchantPayload["bank"]);
    }

    //create cpg
    let headcounter = 1;
    for (let cpgi = 0; cpgi < merchantPayload.cpg.length; cpgi++) {
        let cpgPayload = merchantPayload.cpg[cpgi];
        let cpgElem = merchantPayload.cpg[cpgi];
        let cpgids;
        if (cpgElem["ismerchant"] == "true") {
            //cpg name logic for merchant
            cpgPayload["name"] = merchantPayload["name"] + "-" + cpgElem["denomination"] + "-" + cpgElem["form"]
            cpgPayload["merchantid"] = merchantPayload["id"];
        } else {
            //cpg name logic for retailer
            cpgPayload["name"] = merchantPayload["retailer"]["prefix"] + "-" + cpgElem["denomination"] + "-" + cpgElem["form"]
            cpgPayload["retailerid"] = merchantPayload["retailer"]["id"];
        }
        if (cpgPayload["isspc"] == "Y") {
            cpgPayload["name"] = cpgPayload["name"] + "-" + "spc";
        }

        cpginsertflag = false;
        if (cpgElem["ismerchant"] == "true") {
            if (cpgmerchantflag) {
                cpginsertflag = true;
            }
        } else {
            if (cpgretailerflag) {
                cpginsertflag = true;
            }
        }

        if (cpginsertflag) {
            cpgids = await Cpg.create(cpgPayload);
            await Cpghead.create({
                cpgid: cpgids["insertId"],
                headcounter: headcounter,
                bin: merchantPayload["retailer"]["bin"],
                bookletbin: merchantPayload["retailer"]["bookletbin"]
            }).catch(err => {
                console.log(err);
            });
            headcounter = headcounter + 1;
        }
    }

    //create outlet
    merchantPayload["outlet"]["retailerid"] = merchantPayload["retailer"]["id"];
    if (!merchantPayload.outlet.hasOwnProperty("id")) {
        let outletResp = await Outlet.create(merchantPayload.outlet);
        merchantPayload["outlet"]["id"] = outletResp["insertId"];
    }
    //create Terminal
    merchantPayload.terminals.forEach(async terminalElem => {
        let terminalPayload = terminalElem;
        terminalPayload["outletid"] = merchantPayload["outlet"]["id"];
        await Terminal.create(terminalPayload);
    });
    return { id: merchantPayload.id };
}
const updateMerchant = async (data) => {
    // Process the req data and create object
    let payload = data
    let updateMerchantData = new UpdateObj()
    let updateMerchant = updateMerchantData.updateMerchantObj(payload)
    // Get SQL Queries
    let getMerchantUpdateQuery = await Merchant.updateMerchant(updateMerchant)
    // let getManagerUpdateQuery = await Manager.updateManager(updateMerchant)
    //let getCPGUpdateQuery = await Cpg.updateCPG(updateMerchant)
    // Fire Query in Transaction
    let updateAll = await Merchant.updateMerchantManagerCpg(getMerchantUpdateQuery)
    // Return Result
    return updateAll
}
const detail = async (payload) => {
    let { id } = payload;
    let merchant = await Merchant.getMerchantDetailWithCpg({ "andOp": { "mer.id": id }, offset: 0, limit: 1 });
    if (merchant.length) {
        return merchant[0];
    } else {
        return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.DATA_NOT_FOUND };
    }
}
const search = async (payload) => {
    let { name } = payload;
    let merchants = await Merchant.select({ "andLikeOp": { "name": "%" + name + "%" }, offset: 0, limit: 10 });
    if (merchants.length) {
        let merchant = [];
        merchants.forEach(element => {
            merchant.push({ value: element.id, label: element.name });
        });
        return merchant;
    } else {
        return { status: MESSAGES.RESPONSE_STATUS.failed, message: MESSAGES.validate.DATA_NOT_FOUND };
    }
}
const validateMerchantPayload = async (data) => {
    let merchantPayload = data;
    let retailerPrfixCond = {
        "andOp": { prefix: merchantPayload.retailer.prefix, aggregatorid: merchantPayload.aggregatorid },
        offset: 0, limit: 1
    };
    //storeId Logic 
    if (typeof merchantPayload["retailer"]["id"] != "undefined") {
        retailerPrfixCond["andOpNe"] = { id: merchantPayload.retailer.id };
        let outlet = [];
        if (typeof merchantPayload["outlet"]["id"] == "undefined") {
            outlet = await Outlet.select({
                "andOp": { storeid: merchantPayload.outlet.storeid, retailerid: merchantPayload.retailer.id },
                offset: 0, limit: 1
            });
        }
        if (typeof merchantPayload["outlet"]["id"] != "undefined") {
            outlet = await Outlet.select(
                {
                    "andOp": { storeid: merchantPayload.outlet.storeid, retailerid: merchantPayload.retailer.id },
                    "andOpNe": { id: merchantPayload.outlet.id },
                    offset: 0, limit: 1
                });
        }
        if (outlet.length) {
            return { outlet_storeid: MESSAGES.validate.DUPLICATE_ENTRY };
        }
    }
    // Retailer Prefix Duplicacy Logic Check
    let retailerPrefix = await Retailer.select(retailerPrfixCond);
    if (retailerPrefix.length) {
        return { status: MESSAGES.RESPONSE_STATUS.failed, message: "retailer prefix " + MESSAGES.validate.DUPLICATE_ENTRY };
    }

    //Denomination Logic

    for (let i = 0; i < merchantPayload.cpg.length; i++) {
        let cpgElem = merchantPayload.cpg[i];
        let validateDenom = ACTION.DENOMINATIONS.find((d) => { return d.value == cpgElem.denomination })
        if (typeof validateDenom == "undefined") {
            return { status: MESSAGES.RESPONSE_STATUS.failed, message: "Denomination " + MESSAGES.validate.INVALID_REQUEST };
        }
    }
    return false;
};
const pendinglist = async (data) => {
    let role = data.role;
    let type = data.type;
    let managerid = data.managerid;
    let queryCol = "";
    let resArr = [];

    ACTION.ACTORS.map((value) => {
        if (type === value.type) {
            queryCol = value.approvalId;
        }
    })

    let list = await PendingApproval.pendinglist(role, queryCol, managerid);
    if (list.length) {
        list.forEach(data => {
            resArr.push({
                reqId: data.id,
                [`req` + type.charAt(0).toUpperCase() + type.slice(1)]: JSON.parse(data.request),
                reqStatus: data.status.toUpperCase(),
                reqCreatedAt: data.created_at.toLocaleString(),
                reqMaker: data.name,
                reqRemarks: (data.remarks == null) ? '' : data.remarks
            })
        });
        return resArr;
    } else {
        return { status: MESSAGES.RESPONSE_STATUS.failed, errors: MESSAGES.validate.DATA_NOT_FOUND };
    }
}
const merchantlist = async () => {
    let list = await Merchant.merchantlist();
    if (list.length) {
        return {
            count: list.length,
            dataset: list
        };
    } else {
        return { status: MESSAGES.RESPONSE_STATUS.failed, errors: MESSAGES.validate.DATA_NOT_FOUND };
    }
}


module.exports = {
    createMerchantRequest,
    updateMerchantRequest,
    updateMerchant,
    requestNewMerchantProcess,
    detail,
    search,
    validateMerchantPayload,
    pendinglist,
    merchantlist
}

// const requestProcessOld = async (id) => {
//     //validateData 

//     //create Manager Entry
//     let merchantResp = await Merchant.create(merchantPayload);
//     merchantPayload["manager"]["merchantid"] = merchantResp["insertId"];
//     await Manager.create(merchantPayload.manager);
//     //create retailer
//     merchantPayload["retailer"]["merchantid"] = merchantResp["insertId"];
//     let retailerResp = await Retailer.create(merchantPayload.retailer);
//     merchantPayload["retailer"]["manager"]["merchantid"] = merchantResp["insertId"];
//     merchantPayload["retailer"]["manager"]["retailerid"] = retailerResp["insertId"];
//     await Manager.create(merchantPayload.retailer.manager);
//     //create cpg
//     merchantPayload.cpg.forEach(async cpgElem => {
//         let cpgPayload = cpgElem;
//         if (cpgElem["ismerchant"]) {
//             cpgPayload["merchantid"] = merchantResp["insertId"];
//         } else {
//             cpgPayload["merchantid"] = retailerResp["insertId"];
//         }
//         await Cpg.create(cpgPayload);
//     });
//     //create outlet
//     merchantPayload["outlet"]["retailerid"] = retailerResp["insertId"];
//     let outletResp = await Outlet.create(merchantPayload.outlet);
//     merchantPayload["outlet"]["manager"]["retailerid"] = retailerResp["insertId"];
//     merchantPayload["outlet"]["manager"]["merchantid"] = merchantResp["insertId"];
//     merchantPayload["outlet"]["manager"]["outletid"] = outletResp["insertId"];
//     await Manager.create(merchantPayload.outlet.manager);
//     //create Terminal
//     merchantPayload.terminals.forEach(async terminalElem => {
//         let terminalPayload = terminalElem;
//         terminalPayload["outletid"] = outletResp["insertId"];
//         await Terminal.create(terminalPayload);
//     });
// }

// const _validateMerchantPayload = async (merchantPayload) => {
//     if (
//         merchantPayload["manager"]["phone"] == merchantPayload["retailer"]["manager"]["phone"] ||
//         merchantPayload["manager"]["phone"] == merchantPayload["outlet"]["manager"]["phone"] ||
//         merchantPayload["retailer"]["manager"]["phone"] == merchantPayload["outlet"]["manager"]["phone"]
//     ) {
//         return { status: RESPONSE_STATUS.failed, message: { manager_phone: ERROR_MESSAGE.UNIQUE_VALUE } };
//     }

//     if (!merchantPayload.hasOwnProperty("id")) {
//         let merchantManagerPayload = ObjectUtils.pick(merchantPayload["manager"], ["phone"]);
//         let merchantManagerPhone = await Manager.getcount(merchantManagerPayload);
//         if (merchantManagerPhone) {
//             return { status: RESPONSE_STATUS.failed, message: { merchant_manager_phone: ERROR_MESSAGE.DUPLICATE_ENTRY } };
//         }
//     }
//     if (!merchantPayload["retailer"].hasOwnProperty("id")) {
//         let retailerManagerPhone = await Manager.getcount({ "phone": merchantPayload["retailer"]["manager"]["phone"] });
//         if (retailerManagerPhone) {
//             return { status: RESPONSE_STATUS.failed, message: { retailer_manager_phone: ERROR_MESSAGE.DUPLICATE_ENTRY } };
//         }
//     }
//     if (!merchantPayload["outlet"]["manager"].hasOwnProperty("id")) {
//         let outletManagerPhone = await Manager.getcount({ "phone": merchantPayload["outlet"]["manager"]["phone"] });
//         if (outletManagerPhone) {
//             return { status: RESPONSE_STATUS.failed, message: { outlet_manager_phone: ERROR_MESSAGE.DUPLICATE_ENTRY } };
//         }
//     }
//     return { status: RESPONSE_STATUS.success };
// }

