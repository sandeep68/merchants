/**
 * Alter protobuf generated yml file
 * Append linkerd2 propertiy `isRetryable`: true
 */
const jsyaml = require('js-yaml')
const fs = require('fs')
const log = require('./sys/log')

/**
 * Test run command
 * `node generate.linkerd.profile.js dump/src.yaml dump/dest.yaml`
 */

//remove 2 args as `node` && script names
const args = process.argv.slice(2)
let yamlsrc, yamldest
if (args && args[0] && args[1]) {
  yamlsrc = args[0]
  yamldest = args[1]
  log(`yaml source ${yamlsrc}`)
  log(`yaml destination ${yamldest}`)
} else {
  log('Source & Destination yaml as arguments has required', true)
  process.exit()
}

try {
  // Load yaml file using jsyaml
  fs.readFile(yamlsrc, 'utf8', (err, data) => {
    if (err) throw err
    //Parse yaml
    const jsonObject = jsyaml.safeLoad(data)
    if (jsonObject && jsonObject.spec && jsonObject.spec.routes && jsonObject.spec.routes.length) {
      //Write current timestamp
      if (jsonObject.metadata) {
        jsonObject.metadata.creationTimestamp = new Date().toISOString()
      }

      //iterate condition array of object, set isRetryable:true
      jsonObject.spec.routes = jsonObject.spec.routes.map(route => {
        if (route.condition && typeof route.condition == 'object') {
          route.condition.isRetryable = true
        } else {
          route.condition = { isRetryable: true }
        }
        return route
      })

      //Create json to yaml
      const yamlString = jsyaml.safeDump(jsonObject)
      //Write to destionation
      fs.writeFile(yamldest, yamlString, function(err) {
        if (err) {
          return log(err, true)
        }
        log(`Updated yaml file has saved to destination.`)
        process.exit()
      })
    } else {
      log(`Invalid desired protobuf yaml imput !`, true)
    }
  })
} catch (e) {
  log(e, true)
}
