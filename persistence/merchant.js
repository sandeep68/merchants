const mysql = require('../connectors/mysql')
const ObjectUtils = require("../util/objectUtils");

class Merchant {
  create(obj) {
    let { aggregatorid = 0, name, address, pan, contactname, phone, email, servicefee = 0, isactive = "N", deleted = "N" } = obj;
    let merchant_payload = { aggregatorid, name, address, pan, contactname, phone, email, servicefee, isactive, deleted };
    var data_set = Object.keys(merchant_payload).map(function (key) {
      return merchant_payload[key];
    });
    let query = `INSERT INTO merchants (aggregatorid, name, address,  pan, contactname, phone, email, servicefee, isactive, deleted) VALUES ?`;
    return new Promise((resolve, reject) => {
      mysql.query(query, [[data_set]], (err, resp) => {
        if (err) {
          reject(err);
        } else {
          resolve(resp);
        }
      });
    });
  }

  updateMerchant(merchantPayload) {
    return new Promise(resolve => {
      let query = `UPDATE merchants SET name=? , address=? , pan=? , servicefee=? ,contactname = ?, phone = ?, email = ? WHERE id=?`
      let params = [
        merchantPayload.name,
        merchantPayload.address,
        merchantPayload.pan,
        merchantPayload.servicefee,
        merchantPayload.contactname,
        merchantPayload.phone,
        merchantPayload.email,
        merchantPayload.merchantid
      ]
      resolve({ query, params })
    })
  }

  delete(obj) {
    let { id } = obj;
    let query = `UPDATE merchants SET deleted = "Y" WHERE id= ` + id;
    let params = { id };
    return new Promise((resolve, reject) => {
      mysql.query(query, [], (err, resp) => {
        if (err) {
          reject(err);
        } else {
          resolve(resp);
        }
      });
    });
  }
  updateMerchantManagerCpg(merchant) {
    return new Promise((resolve, reject) => {
      merchant.query = merchant.query || ''
      // manager.query = manager.query || ''
      //cpg.query = cpg.query || ''

      let updateParams = [...merchant.params]

      let updateTxn = `
                      START TRANSACTION;

                      ${merchant.query};

                      COMMIT;
                    `
      mysql.query(updateTxn, updateParams, (err, res) => {
        if (err) reject(err)
        else resolve(res)
      })
    })
  }

  updateCorporateManager(corporateid, managerid, manager) {
    return new Promise((resolve, reject) => {
      let query = `UPDATE managers SET name=? , phone=? , email=? , designation=? , salutation=? WHERE id=? AND corporateid=?`
      let params = [
        manager.name,
        manager.phone,
        manager.email,
        manager.designation,
        manager.salutation,
        managerid,
        corporateid
      ]

      mysql.query(query, params, (err, res) => {
        if (err) reject(err)
        else resolve(res)
      })
    })
  }

  getMerchantDetailWithCpg(whereObj = {}) {
    if (whereObj.hasOwnProperty("andOp")) {
      whereObj["andOp"]["mer.isactive"] = "Y";
      whereObj["andOp"]["mer.deleted"] = "N";
      // whereObj["andOp"]["cpg.isactive"] = "Y";
      // whereObj["andOp"]["cpg.deleted"] = "N";
    } else {
      whereObj["andOp"] = { "mer.isactive": "Y", "mer.deleted": "N" };
    }
    let { query, params } = ObjectUtils.formQuery(whereObj);
    let mysql_query = "SELECT mer.*,cpg.form,cpg.denomination,cpg.name as cpgname FROM merchants mer left join cpg on mer.id = cpg.merchantid " + query;
    return new Promise((resolve, reject) => {
      mysql.query(mysql_query, params, (err, resp) => {
        if (err) {
          reject(err);
        } else {
          resolve(resp)
        }
      })
    });
  }

  select(whereAndObject = {}) {
    if (whereAndObject.hasOwnProperty("andOp")) {
      whereAndObject["andOp"]["isactive"] = "Y";
      whereAndObject["andOp"]["deleted"] = "N";
    } else {
      whereAndObject["andOp"] = { "isactive": "Y", "deleted": "N" };
    }
    let { query, params } = ObjectUtils.formQuery(whereAndObject);
    let mysql_query = "SELECT * FROM merchants " + query;
    return new Promise((resolve, reject) => {
      mysql.query(mysql_query, params, (err, resp) => {
        if (err) {
          reject(err);
        } else {
          resolve(resp)
        }
      })
    });
  }


  merchantlist() {

    let query = `SELECT merchants.*, CONCAT(managers.firstname,"",managers.lastname) as makername
                 FROM merchants
                 INNER JOIN pendingapprovals ON merchants.id = pendingapprovals.merchantid
                 INNER JOIN managers ON managers.id = pendingapprovals.managerid
                 AND merchants.isactive = 'Y' AND merchants.deleted = 'N'
                `
    return new Promise((resolve, reject) => {
      mysql.query(query, [], (err, resp) => {
        if (err) {
          reject(err);
        } else {
          resolve(resp)
        }
      })
    });
  }

}

module.exports = new Merchant()
