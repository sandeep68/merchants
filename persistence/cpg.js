const mysql = require('../connectors/mysql')
const log = require('../sys/log')

class Cpg {

    create(obj) {
        let { aggregatorid = 0, merchantid = 0, retailerid = 0, form, isspc, isdeferred, denomination, name = "", isactive = "Y", deleted = "N" } = obj;

        let payload = { aggregatorid, merchantid, retailerid, form, isspc, isdeferred, denomination, name, isactive, deleted };
        var data_set = Object.keys(payload).map(function (key) {
            return payload[key];
        });
        let query = `INSERT INTO cpg (aggregatorid, merchantid, retailerid, form, isspc, isdeferred, denomination, name, isactive, deleted) VALUES ?`;
        //(${aggregatorid}, ${merchantid}, ${retailerid}, '${form}', '${isspc}', '${isdeferred}', ${denomination}, '${name}', '${isactive}', '${deleted}')`;
        return new Promise((resolve, reject) => {
            mysql.query(query, [[data_set]], (err, resp) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(resp);
                }
            });
        });
    }

    updateCPG(cpgPayload) {
        return new Promise(resolve => {
            let query = `UPDATE cpg SET prefix=? , denomination=? , form=? WHERE merchantid=?`
            let params = [cpgPayload.prefix, cpgPayload.denomination, cpgPayload.form, cpgPayload.merchantid]
            resolve({ query, params })
        })
    }

}

module.exports = new Cpg()
