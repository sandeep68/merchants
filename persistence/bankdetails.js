const mysql = require('../connectors/mysql')
const log = require('../sys/log')

class Bankdetails {
  create(obj) {
    let { retailerid, accountnumber, bankname, ifsc, isactive = 'Y', deleted = 'N' } = obj

    let payload = { retailerid, accountnumber, name: bankname, ifsc, isactive, deleted }
    var data_set = Object.keys(payload).map(function (key) {
      return payload[key]
    })
    let query = `INSERT INTO bankdetails (retailerid, accountnumber, name, ifsc, isactive, deleted) VALUES ?`
    return new Promise((resolve, reject) => {
      mysql.query(query, [[data_set]], (err, resp) => {
        if (err) {
          reject(err)
        } else {
          resolve(resp)
        }
      })
    })
  }

  updateBank(bankPayload) {
    return new Promise(resolve => {
      let query = `UPDATE bankdetails SET name=? , accountnumber=? , ifsc=? WHERE retailerid=?`
      let params = [bankPayload.bankname, bankPayload.accountnumber, bankPayload.ifsc, bankPayload.retailerid]
      resolve({ query, params })
    })
  }
}

module.exports = new Bankdetails()
