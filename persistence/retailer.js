const ObjectUtils = require("../util/objectUtils")
const mysql = require('../connectors/mysql')
const log = require('../sys/log')
let binhead = 700000, bookletbinhead = 710000;

class Retailer {
  async create(obj) {
    let lastInsert = await this.fetchLastInsert();
    let bin, bookletbin;
    if (lastInsert.length > 0) {
      // bin = lastInsert[0]["bin"];
      // bookletbin = lastInsert[0]["bookletbin"];
      // let newbin = (bin < binhead) ? binhead : bin + 1;
      // let newbookletbin = (bookletbin < bookletbinhead) ? bookletbinhead : bookletbin + 1;
      bin = await this._generateBin(lastInsert[0]["bin"]);
      bookletbin = await this._generateBookletBin(lastInsert[0]["bookletbin"]);
    } else {
      bin = binhead;
      bookletbin = bookletbinhead;
    }

    let { aggregatorid, merchantid, name, gstno, gstaddress, categoryid,
      subcategoryid, servicefee, pan, cheque, gstcertificate, prefix, contactname, phone, email, isactive = "Y", deleted = "N" } = obj;

    let payload = {
      aggregatorid, merchantid, name, gstno, gstaddress, categoryid,
      subcategoryid, servicefee, pan, cheque, gstcertificate, prefix, bin, bookletbin, contactname, phone, email, isactive, deleted
    };
    var data_set = Object.keys(payload).map(function (key) {
      return payload[key];
    });

    let query = `INSERT INTO retailers (aggregatorid,merchantid, name,  gstno, gstaddress, categoryid,
            subcategoryid, servicefee, pan, cheque, gstcertificate,prefix, bin , bookletbin , contactname, phone, email, isactive, deleted) VALUES ?`;
    return new Promise((resolve, reject) => {
      mysql.query(query, [[data_set]], (err, resp) => {
        if (err) {
          reject(err);
        } else {
          resolve(resp);
        }
      });
    });
  }

  fetchLastInsert() {
    return new Promise((resolve, reject) => {
      let query = "SELECT * FROM retailers order by id desc limit 1";
      mysql.query(query, [], (err, resp) => {
        if (err)
          reject(err)
        else
          resolve(resp)
      })
    })
  }
  updateRetailer(retailerPayload) {
    return new Promise(resolve => {
      let query = `UPDATE retailers SET name=? , gstno=? , gstaddress=? , categoryid=?, subcategoryid=?, servicefee=? , pan = ?, cheque = ?, gstcertificate = ?,prefix = ?,contactname = ? , phone = ?, email = ? WHERE id=?`
      let params = [
        retailerPayload.name,
        retailerPayload.gstno,
        retailerPayload.gstaddress,
        retailerPayload.categoryid,
        retailerPayload.subcategoryid,
        retailerPayload.servicefee,
        retailerPayload.pan,
        retailerPayload.cheque,
        retailerPayload.gstcertificate,
        retailerPayload.prefix,
        retailerPayload.contactname,
        retailerPayload.phone,
        retailerPayload.email,
        retailerPayload.retailerid
      ]
      resolve({ query, params })
    })
  }

  updateRetailerBank(retailer, bank) {
    return new Promise((resolve, reject) => {
      retailer.query = retailer.query || ''
      bank.query = bank.query || ''
      let updateParams = [...retailer.params, ...bank.params]
      let updateTxn = `
                      START TRANSACTION;

                      ${retailer.query};

                      ${bank.query};

                      COMMIT;
                    `
      mysql.query(updateTxn, updateParams, (err, res) => {
        if (err) reject(err)
        else resolve(res)
      })
    })
  }

  select(whereAndObject = {}) {
    if (whereAndObject.hasOwnProperty("andOp")) {
      whereAndObject["andOp"]["isactive"] = "Y";
      whereAndObject["andOp"]["deleted"] = "N";
    } else {
      whereAndObject["andOp"] = { "isactive": "Y", "deleted": "N" };
    }
    let { query, params } = ObjectUtils.formQuery(whereAndObject);
    let mysql_query = "SELECT * FROM retailers " + query;
    return new Promise((resolve, reject) => {
      mysql.query(mysql_query, params, (err, resp) => {
        if (err) {
          reject(err);
        } else {
          resolve(resp)
        }
      })
    });
  }

  selectBankDetail(whereAndObject = {}) {
    if (whereAndObject.hasOwnProperty("andOp")) {
      whereAndObject["andOp"]["ret.isactive"] = "Y";
      whereAndObject["andOp"]["ret.deleted"] = "N";
    } else {
      whereAndObject["andOp"] = { "ret.isactive": "Y", "ret.deleted": "N" };
    }
    let { query, params } = ObjectUtils.formQuery(whereAndObject);
    let mysql_query = "SELECT ret.* , bd.id as bankId,bd.accountnumber,bd.name as bankname,bd.ifsc  FROM retailers ret left JOIN bankdetails bd on ret.id = bd.retailerid " + query;
    return new Promise((resolve, reject) => {
      mysql.query(mysql_query, params, (err, resp) => {
        if (err) {
          reject(err);
        } else {
          resolve(resp)
        }
      })
    });
  }

  delete(obj) {
    let { id } = obj;
    let query = `UPDATE retailers SET deleted = "Y" WHERE id=` + id;
    return new Promise((resolve, reject) => {
      mysql.query(query, [], (err, resp) => {
        if (err) {
          reject(err);
        } else {
          resolve(resp);
        }
      });
    });
  }

  async _generateBin(num) {
    let numbers = num;
    let numberArray = Array.from(String(numbers), Number);

    let position1 = 0;
    let position2 = 0;
    let temp = 0;
    for (let i = 0; i < numberArray.length; i++) {
      if (i == 1) {
        position1 = numberArray[i]
      }
      if (i == 2) {
        position2 = numberArray[i]
      }
    }

    temp = "" + position1 + position2;
    temp = parseInt(temp) + 1;

    temp = (temp > 10) ? temp : String(temp).padStart(2, 0);
    let numberArray2 = Array.from(String(temp), Number);


    for (let i = 0; i < numberArray.length; i++) {
      if (temp > 99) {
        if (i == 0) {
          numberArray[i] = numberArray[i] + 1;
          numberArray[i + 1] = 0;
          numberArray[i + 2] = 1;
        }
      } else {
        if (i == 1) {
          numberArray[i] = numberArray2[0]
        }
        if (i == 2) {
          numberArray[i] = numberArray2[1]
        }
      }
    }
    let retailerbin = numberArray.join('');
    let retailer = await this.select({ "andOp": { bin: retailerbin } })
    if (retailer.length) {
      return _generateBin(retailerbin);
    } else {
      return retailerbin;
    }
  }

  async _generateBookletBin(num) {
    let numbers = num;
    let numberArray = Array.from(String(numbers), Number);

    let position1 = 0;
    let position2 = 0;
    let temp = 0;
    for (let i = 0; i < numberArray.length; i++) {
      if (i == 1) {
        position1 = numberArray[i]
      }
      if (i == 2) {
        position2 = numberArray[i]
      }
    }

    temp = "" + position1 + position2;
    temp = parseInt(temp) + 1;

    temp = (temp > 10) ? temp : String(temp).padStart(2, 0);
    let numberArray2 = Array.from(String(temp), Number);


    for (let i = 0; i < numberArray.length; i++) {
      if (temp > 99) {
        if (i == 0) {
          numberArray[i] = numberArray[i] + 1;
          //Only the Difference is here with bin
          numberArray[i + 1] = 1;
          numberArray[i + 2] = 0;
        }
      } else {
        if (i == 1) {
          numberArray[i] = numberArray2[0]
        }
        if (i == 2) {
          numberArray[i] = numberArray2[1]
        }
      }
    }
    let retailerbookletbin = numberArray.join('');
    let retailer = await this.select({ "andOp": { bookletbin: retailerbookletbin } })
    if (retailer.length) {
      return _generateBookletBin(retailerbookletbin);
    } else {
      return retailerbookletbin;
    }
  }
}

module.exports = new Retailer();


