const mysql = require('../connectors/mysql')
const log = require('../sys/log')

class Cpghead {

    create(obj) {
        let randomPoolid = this.createRandomPool();
        let randomCpgPoolid = this.createRandomPool(randomPoolid);
        let { cpgid, head = 0, pointer = 0, poolid = randomPoolid, booklethead = 0, bin, bookletbin, headcounter, bookletpointer = 0, bookletpoolid = randomCpgPoolid, isactive = "Y", deleted = "N" } = obj;
        let payload = { cpgid, head, pointer, poolid, booklethead, bookletpointer, bookletpoolid, isactive, deleted };
        payload["head"] = bin + headcounter;
        payload["booklethead"] = bookletbin + headcounter;
        var data_set = Object.keys(payload).map(function (key) {
            return payload[key];
        });
        let query = `INSERT INTO cpgheads (cpgid, head, pointer, poolid, booklethead, bookletpointer, bookletpoolid, isactive, deleted) VALUES ?`;
        return new Promise((resolve, reject) => {
            mysql.query(query, [[data_set]], (err, resp) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(resp);
                }
            });
        });
    }

    createRandomPool(notnum = false) {
        let min = 1;
        let max = process.env.NUM_OF_POOLS;
        let finalNumber = Math.floor(Math.random() * (+max - +min)) + +min;
        if (notnum == finalNumber) {
            return (finalNumber == parseInt(max)) ? finalNumber - 1 : (finalNumber == min) ? finalNumber + 1 : finalNumber - 1;
        } else {
            return finalNumber;
        }
    }
}

module.exports = new Cpghead()
