const mysql = require('../connectors/mysql')
const log = require('../sys/log')

class Category {

    create(obj) {
        let { name, parentcategoryid = 0, deleted = "N" } = obj;

        let payload = { name, parentcategoryid, deleted };
        var data_set = Object.keys(payload).map(function (key) {
            return payload[key];
        });
        let query = `INSERT INTO category (name, parentcategoryid, deleted) VALUES ?`;
        return new Promise((resolve, reject) => {
            mysql.query(query, [[data_set]], (err, resp) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(resp);
                }
            });
        });
    }
    update(obj = [], where) {


    }
}

module.exports = new Category;