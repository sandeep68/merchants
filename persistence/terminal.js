//Builder
const ObjectUtils = require("../util/objectUtils")

const mysql = require('../connectors/mysql')
const log = require('../sys/log')

class Terminal {

    create(obj) {
        let { outletid = 0, terminal_id, type, isactive = "Y", deleted = "N" } = obj;

        let payload = { outletid, terminal_id, type, isactive, deleted };
        var data_set = Object.keys(payload).map(function (key) {
            return payload[key];
        });
        let query = `INSERT INTO terminals (outletid, terminal_id, type, isactive, deleted) VALUES ?`;
        return new Promise((resolve, reject) => {
            mysql.query(query, [[data_set]], (err, resp) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(resp);
                }
            });
        });
    }

    select(whereAndObject = {}) {
        if (whereAndObject.hasOwnProperty("andOp")) {
            whereAndObject["andOp"]["isactive"] = "Y";
            whereAndObject["andOp"]["deleted"] = "N";
        } else {
            whereAndObject["andOp"] = { "isactive": "Y", "deleted": "N" };
        }
        let { query, params } = ObjectUtils.formQuery(whereAndObject);
        let mysql_query = "SELECT * FROM terminals " + query;
        return new Promise((resolve, reject) => {
            mysql.query(mysql_query, params, (err, resp) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(resp)
                }
            })
        });
    }

    delete(obj) {
        let { id } = obj;
        let query = `UPDATE terminals SET deleted = "Y" WHERE id=` + id;;
        let params = { id };
        return new Promise((resolve, reject) => {
            mysql.query(query, [], (err, resp) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(resp);
                }
            });
        });
    }

    update(obj = {}, where = {}) {
        let updateData = "";
        let whereData = "";
        let data_set = [];

        let { terminal_id, type } = obj;

        if (typeof terminal_id != "undefined") {
            updateData += " terminal_id = ?";
            data_set.push(terminal_id);
        }

        if (typeof type != "undefined") {
            updateData += ", type = ?";
            data_set.push(type);
        }

        let { id } = where;
        if (typeof id != "undefined") {
            whereData += " id =  ? ";
            data_set.push(id);
        }

        let query = `UPDATE terminals SET ` + updateData + ` WHERE ` + whereData;

        return new Promise((resolve, reject) => {
            mysql.query(query, data_set, (err, res) => {
                if (err)
                    reject(err)
                else
                    resolve(res)
            })
        })

    }
}

module.exports = new Terminal;