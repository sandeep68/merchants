const mysql = require('../connectors/mysql')
const log = require('../sys/log')
const ObjectUtils = require("../util/objectUtils")

class Pendingapprovals {

    create(obj) {
        let { action, corporateid = 0, branchid = 0, merchantid = 0, retailerid = 0, outletid = 0, terminalid = 0, managerid, request, status, isactive = "Y", deleted = "N", remarks = null } = obj;

        let payload = { action, corporateid, branchid, merchantid, retailerid, outletid, terminalid, managerid, request, status, isactive, deleted, remarks };
        var data_set = Object.keys(payload).map(function (key) {
            return payload[key];
        });
        let query = `INSERT INTO pendingapprovals (action, corporateid, branchid, merchantid, retailerid, outletid, terminalid, managerid, request, status, isactive, deleted,remarks) VALUES ?`;
        return new Promise((resolve, reject) => {
            mysql.query(query, [[data_set]], (err, resp) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(resp);
                }
            });
        });
    }
    select(whereAndObject = {}) {
        if (whereAndObject.hasOwnProperty("andOp")) {
            whereAndObject["andOp"]["isactive"] = "Y";
            whereAndObject["andOp"]["deleted"] = "N";
        } else {
            whereAndObject["andOp"] = { "isactive": "Y", "deleted": "N" };
        }
        let { query, params } = ObjectUtils.formQuery(whereAndObject);
        let mysql_query = "SELECT * FROM pendingapprovals " + query;
        return new Promise((resolve, reject) => {
            mysql.query(mysql_query, params, (err, resp) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(resp)
                }
            })
        });
    }
    update(obj = {}, where = {}) {
        let updateData = "";
        let whereData = "";
        let data_set = [];

        let { action, corporateid, branchid, merchantid, retailerid, outletid, terminalid, request, status, isactive, deleted } = obj;

        if (typeof status != "undefined") {
            updateData += " status = ?";
            data_set.push(status);
        }

        if (typeof merchantid != "undefined") {
            updateData += ", merchantid = ?";
            data_set.push(merchantid);
        }

        let { id } = where;
        if (typeof id != "undefined") {
            whereData += " id =  ? ";
            data_set.push(id);
        }

        let query = `UPDATE pendingapprovals SET ` + updateData + ` WHERE ` + whereData;

        return new Promise((resolve, reject) => {
            mysql.query(query, data_set, (err, res) => {
                if (err)
                    reject(err)
                else
                    resolve(res)
            })
        })

    }

    /**
    * Get all pending list data
    * @param managerid
    * @param role
    * @param type
    */
    pendinglist(role, queryCol, managerid) {

        return new Promise((resolve, reject) => {

            let query = "";
            let params = managerid;

            if (role === "superadmin") {

                query = `
                            SELECT p.id, p.request, p.status, p.created_at, p.remarks, m.name
                            FROM pendingapprovals p
                            JOIN managers m on (m.id = p.managerid)
                            WHERE p.status IN ('requested', 'pending', 'rejected') and p.${queryCol} != 0
                        `;

            } else {

                query = `
                            SELECT p.id, p.request, p.status, p.created_at, p.remarks, m.name
                            FROM pendingapprovals p
                            JOIN managers m on (m.id = p.managerid)
                            WHERE p.status IN ('requested', 'pending', 'rejected') and managerid=? and p.${queryCol} != 0
                        `;
            }

            mysql.query(query, params, (err, res) => {
                if (err)
                    reject(err)
                else
                    resolve(res)
            })

        })
    }

}

module.exports = new Pendingapprovals;