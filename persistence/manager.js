const mysql = require('../connectors/mysql')
const log = require('../sys/log')

class Manager {

    create(obj) {
        let { partnerid = 0, corporateid = 0, branchid = 0,
            merchantid = 0, retailerid = 0, outletid = 0,
            name, phone, email, designation, salutation, apikey, isactive = "Y", deleted = "N" } = obj;
        let payload = {
            partnerid, corporateid, branchid, merchantid, retailerid, outletid,
            name, phone, email, designation, salutation, apikey, isactive, deleted
        };
        var data_set = Object.keys(payload).map(function (key) {
            return payload[key];
        });
        let query = `INSERT INTO managers (partnerid, corporateid, branchid, merchantid, retailerid, outletid,
            name, phone, email, designation, salutation, apikey, isactive, deleted) VALUES ?`;
        return new Promise((resolve, reject) => {
            mysql.query(query, [[data_set]], (err, resp) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(resp);
                }
            });
        });
    }

    getcount(whereObject) {
        let whereCondition = "";
        let params = [];
        if (Object.keys(whereObject).length > 0) {
            whereCondition += "where ";
            for (var whereKey in whereObject) {
                whereCondition += whereKey + " =  ?";
                params.push(whereObject[whereKey]);
            }
        }
        let query = "SELECT count(*) as count FROM managers " + whereCondition;
        return new Promise((resolve, reject) => {
            mysql.query(query, params, (err, resp) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(resp[0]["count"])
                }
            })
        });
    }

    updateManager(mgrPayload) {
        return new Promise(resolve => {
            let query = `UPDATE managers SET name=? , phone=? , email=? WHERE merchantid=?`
            let params = [mgrPayload.managername, mgrPayload.managerphone, mgrPayload.manageremail, mgrPayload.merchantid]
            resolve({ query, params })
        })
    }

    /**
     * Get manager details by api key
     * @param {key value} key 
     */
    getManagerById(id) {

        return new Promise((resolve, reject) => {
            let query = `SELECT * from managers WHERE id=? AND isactive='Y' AND deleted = 'N'`
            let params = [id]

            mysql.query(query, params, (err, res) => {
                if (err)
                    reject(err)
                else
                    resolve(res[0])
            })
        })
    }
}

module.exports = new Manager()
