const mysql = require('../connectors/mysql')
const log = require('../sys/log')
const ObjectUtils = require("../util/objectUtils")
class Outlet {
  create(obj) {
    let {
      retailerid,
      name,
      area,
      city,
      state,
      pincode,
      region,
      latitude,
      longitude,
      sapsitecode,
      storeid,
      pos,
      poynt,
      web,
      contactname,
      phone,
      email,
      gstno,
      isactive = 'Y',
      deleted = 'N'
    } = obj

    let payload = {
      retailerid,
      name,
      area,
      city,
      state,
      pincode,
      region,
      latitude,
      longitude,
      sapsitecode,
      storeid,
      pos,
      poynt,
      web,
      contactname,
      phone,
      email,
      gstno,
      isactive,
      deleted
    }
    var data_set = Object.keys(payload).map(function (key) {
      return payload[key]
    })
    let query = `INSERT INTO outlets (retailerid, name, area, city, state, pincode, region, latitude, longitude, sapsitecode, storeid, pos, poynt, web, contactname, phone, email,gstno, isactive, deleted) VALUES ?`
    return new Promise((resolve, reject) => {
      mysql.query(query, [[data_set]], (err, resp) => {
        if (err) {
          reject(err)
        } else {
          resolve(resp)
        }
      })
    })
  }

  updateOutlet(outletPayload) {
    let query = `UPDATE outlets SET name=? , area=? , city=? , state=?, pincode=?, region=? , latitude=? , longitude=? , sapsitecode=?,storeid=?, pos=? , poynt=?, web=? ,contactname=?, phone=?, email=? ,gstno=? WHERE id=?`
    let params = [
      outletPayload.name,
      outletPayload.area,
      outletPayload.city,
      outletPayload.state,
      outletPayload.pincode,
      outletPayload.region,
      outletPayload.latitude,
      outletPayload.longitude,
      outletPayload.sapsitecode,
      outletPayload.storeid,
      outletPayload.pos,
      outletPayload.poynt,
      outletPayload.web,
      outletPayload.contactname,
      outletPayload.phone,
      outletPayload.email,
      outletPayload.gstno,
      outletPayload.outletid
    ]
    return new Promise((resolve, reject) => {
      mysql.query(query, params, (err, resp) => {
        if (err) {
          reject(err)
        } else {
          resolve(resp)
        }
      })
    })
  }

  select(whereAndObject = {}) {
    if (whereAndObject.hasOwnProperty("andOp")) {
      whereAndObject["andOp"]["isactive"] = "Y";
      whereAndObject["andOp"]["deleted"] = "N";
    } else {
      whereAndObject["andOp"] = { "isactive": "Y", "deleted": "N" };
    }
    let { query, params } = ObjectUtils.formQuery(whereAndObject);
    let mysql_query = "SELECT * FROM outlets " + query;
    return new Promise((resolve, reject) => {
      mysql.query(mysql_query, params, (err, resp) => {
        if (err) {
          reject(err);
        } else {
          resolve(resp)
        }
      })
    });
  }

  delete(obj) {
    let { id } = obj;
    let query = `UPDATE outlets SET deleted = "Y" WHERE id=` + id;;
    let params = { id };
    return new Promise((resolve, reject) => {
      mysql.query(query, [], (err, resp) => {
        if (err) {
          reject(err);
        } else {
          resolve(resp);
        }
      });
    });
  }
}

module.exports = new Outlet();
