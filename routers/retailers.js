const express = require('express')
const retailerrouter = express.Router()
const retailerhelpers = require('../helpers/retailer')

// MIDDLEWARES
const validateManager = require('../middlewares/middleware');

retailerrouter.post('/detail', validateManager, (req, res, next) => {
  retailerhelpers.detail(req.body, res, data => {
    res.locals.data = data
    next()
  })
})

retailerrouter.post('/search', validateManager, (req, res, next) => {
  retailerhelpers.search(req.body, res, data => {
    res.locals.data = data
    next()
  })
})

retailerrouter.post('/update/request/maker', validateManager, (req, res, next) => {
  retailerhelpers.updatemaker(req.body, res, data => {
    res.locals.data = data
    next()
  })
})

retailerrouter.post('/update/request/checker', validateManager, (req, res, next) => {
  retailerhelpers.updatechecker(req.body, res, data => {
    res.locals.data = data
    next()
  })
})
module.exports = retailerrouter
