const express = require('express')
const terminalrouter = express.Router()
const terminalhelpers = require('../helpers/terminal')

// MIDDLEWARES
const validateManager = require('../middlewares/middleware');

terminalrouter.post('/detail', validateManager, (req, res, next) => {
    terminalhelpers.detail(req.body, res, data => {
        res.locals.data = data
        next()
    })
})

terminalrouter.post('/search', validateManager, (req, res, next) => {
    terminalhelpers.search(req.body, res, data => {
        res.locals.data = data
        next()
    })
})

terminalrouter.post('/list', validateManager, (req, res, next) => {
    terminalhelpers.list(req.body, res, data => {
        res.locals.data = data
        next()
    })
})

terminalrouter.post('/update/request/maker', validateManager, (req, res, next) => {
    terminalhelpers.updatemaker(req.body, res, data => {
        res.locals.data = data
        next()
    })
})

terminalrouter.post('/update/request/checker', validateManager, (req, res, next) => {
    terminalhelpers.updatechecker(req.body, res, data => {
        res.locals.data = data
        next()
    })
})
module.exports = terminalrouter
