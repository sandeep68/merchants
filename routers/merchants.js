const express = require('express')
const merchantrouter = express.Router()
const merchanthelpers = require('../helpers/merchant')

// MIDDLEWARES
const validateManager = require('../middlewares/middleware');
const useraccess = require('../middlewares/useraccess').validate;

// REAL VERSION
// merchantrouter.get('/', (req, res) => {
//   let realdata = 'This is merchant api'
//   res.json(realdata)
// })

merchantrouter.post('/create', validateManager, (req, res, next) => {
  merchanthelpers.create(req.body, res, data => {
    res.locals.data = data
    next()
  })
})

merchantrouter.post('/update/request/maker', validateManager, (req, res, next) => {
  merchanthelpers.updatemaker(req.body, res, data => {
    res.locals.data = data
    next()
  })
})


//Request Processing
merchantrouter.post('/new/request', validateManager, (req, res, next) => {
  merchanthelpers.processNewMerchant(req.body, res, data => {
    res.locals.data = data
    next()
  })
})

merchantrouter.post('/update/request/checker', validateManager, (req, res, next) => {
  merchanthelpers.updatechecker(req.body, res, data => {
    res.locals.data = data
    next()
  })
})

//Request Processing
merchantrouter.post('/new/create', validateManager, (req, res, next) => {
  merchanthelpers.createNewMerchant(req.body, res, data => {
    res.locals.data = data
    next()
  })
})

merchantrouter.post('/detail', validateManager, (req, res, next) => {
  merchanthelpers.detail(req.body, res, data => {
    res.locals.data = data
    next()
  })
})

merchantrouter.post('/search', validateManager, (req, res, next) => {
  merchanthelpers.search(req.body, res, data => {
    res.locals.data = data
    next()
  })
})

merchantrouter.post('/delete/request/maker', validateManager, (req, res, next) => {
  merchanthelpers.deletemaker(req.body, res, data => {
    res.locals.data = data
    next()
  })
})

merchantrouter.post('/delete/request/checker', validateManager, (req, res, next) => {
  merchanthelpers.deletechecker(req.body, res, data => {
    res.locals.data = data
    next()
  })
})

// Pending List
merchantrouter.post('/pendinglist', validateManager, (req, res, next) => {
  const role = res.locals.role;
  const ReqBody = { ...req.body, role };
  merchanthelpers.pendinglist(ReqBody, res, data => {
    res.locals.data = data
    next()
  })
})

// Merchant Lists
merchantrouter.post('/list', useraccess, (req, res, next) => {
  merchanthelpers.merchantlist(req.body, res, data => {
    res.locals.data = data
    next()
  })
})

// Merchant Lists
merchantrouter.post('/cpg/denominations', validateManager, (req, res, next) => {
  merchanthelpers.denominationlist(req.body, res, data => {
    res.locals.data = data
    next()
  })
})

module.exports = merchantrouter
