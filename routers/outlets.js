const express = require('express')
const outletrouter = express.Router()
const outlethelpers = require('../helpers/outlet')

// MIDDLEWARES
const validateManager = require('../middlewares/middleware');


outletrouter.post('/detail', validateManager, (req, res, next) => {
  outlethelpers.detail(req.body, res, data => {
    res.locals.data = data
    next()
  })
})

outletrouter.post('/search', validateManager, (req, res, next) => {
  outlethelpers.search(req.body, res, data => {
    res.locals.data = data
    next()
  })
})

outletrouter.post('/list', validateManager, (req, res, next) => {
  outlethelpers.list(req.body, res, data => {
    res.locals.data = data
    next()
  })
})

outletrouter.post('/update/request/maker', validateManager, (req, res, next) => {
  outlethelpers.updatemaker(req.body, res, data => {
    res.locals.data = data
    next()
  })
})

outletrouter.post('/update/request/checker', validateManager, (req, res, next) => {
  outlethelpers.updatechecker(req.body, res, data => {
    res.locals.data = data
    next()
  })
})
module.exports = outletrouter
