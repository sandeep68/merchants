const express = require('express')
const diagnosticrouter = express.Router()
let counters = require('../sys/counters')
const pjson = require('../package.json')

diagnosticrouter.get('/alive', (req, res, next) => {
  // res.send('Alive') // Old way to send response
  res.locals.data = 'Alive'
  next()
})

diagnosticrouter.get('/counters', (req, res) => {
  if (process.env.ENABLE_COUNTERS == 'true') res.send(JSON.stringify(counters.getall()))
  else res.send('Counters not enabled')
})

module.exports = diagnosticrouter
