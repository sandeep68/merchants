const Redis = require('ioredis')
const log = require('../sys/log')
const opts = {
  host: process.env.STREAMREDIS_HOST,
  port: process.env.STREAMREDIS_PORT,
  password: process.env.STREAMREDIS_PASSWORD
}
let redis = null
const getClient = function() {
  if (!redis || !(redis.status === 'connecting' || redis.status === 'connect' || redis.status === 'ready')) {
    //log('Renewing streamredis connection')
    redis = new Redis(opts)
    redis.createBuiltinCommand('xadd')
  }
  return redis
}
module.exports = { getClient }
