# MetaService

This service is intended to be a service template from which all other services shall be forked.
The name of the service is programmatically taken from `package.json` , the (sync/async) channels from `channels.js`

The service can subscribe to events generated from other services using `reactions.js` . This allows for more flexible namespacing (any service, any wildcard) than the async channels can permit.

## Dev Dependencies

- eslint
- env2

Put all the required env variables for dev in _devenv.json_ as shown in example. This should also be in .gitignore.

### Example devenv.json as per brahman dev

```
{
  "SECRET_SYSNATS_USER": "sysnatsconsumer",
  "SECRET_SYSNATS_PASSWORD": "devpassword",
  "SYSNATS_TIMEOUT": 10000,
  "RXNATS_TIMEOUT": 10000,
  "SECRET_RXNATS_USER": "rxnatsconsumer",
  "SECRET_RXNATS_PASSWORD": "devpassword",
  "SYSNATS_HOST": "sysnats",
  "SYSNATS_PORT": "4222",
  "RXNATS_HOST": "rxnats",
  "RXNATS_PORT": "4223",
  "MYSQL_HOST" : "mysql",
  "MYSQL_PORT" : 3306,
  "MYSQL_USER" : "fp",
  "MYSQL_PASSWORD" : "fp",
  "MYSQL_DATABASE" : "fp",
  "MYSQL_CONNECTION_LIMIT" : 100,
  "CACHEREDIS_HOST" : "cacheredis",
  "CACHEREDIS_PORT" : 6379,
  "CACHEREDIS_PASSWORD": "1f2d1e2e67df",
  "STREAMREDIS_HOST" : "streamredis",
  "STREAMREDIS_PORT" : 6381,
  "STREAMREDIS_PASSWORD": "1f2d1e2e67df",
  "SYSREDIS_HOST" : "sysredis",
  "SYSREDIS_PORT" : 6380,
  "SYSREDIS_PASSWORD": "1f2d1e2e67df"
  "GRPC_HOST" : "0.0.0.0",
  "GRPC_PORT" : 50051
}
```

## Important notes

Run `npm install` to install node_modules.

### NOTE 0

Add this to your .bashrc or .zshrc

```sh
export NODE_ENV="development"
```

### NOTE 1

`console.log` is disabled by eslint... use `sys/log` utility instead for output standardization of logs. Example

```js
const log = require ('../sys/log');

log('This is an error',true) # Second arg is boolean for error
log('This is info,not error');
```

### NOTE 2

Use the flags as shown in `npm install yourmodule --save --save-exact` for installing new npm modules.

## INTER SERVICE COMMS - PROTOCOL BUFFERS

We shall use proto3 version of protocol buffers for inter-service comms , as opposed to JSON.
We use the library protbufjs for this.

### protobufs conventions

The filename and package of protobufs should match closely with our convention of service and channel.
For example, in the **metaservice** - there is a synchronous channel called **commsexample**.
Thus its full NATS string `{servicename}/{channelname}` is `meta/commsexample`

Thus
As an example, the synchronous channel `meta/commsexample` will have both `request` and `response` message contracts in the file
`./protobuf/meta.commsexample.proto`. Refer to this file to understand more. Asynchronous channels wont have a `response` structure, obviously.
Reactions or events will have the final message as `event` instead of `request` or `response`.

**IMPORTANT** : All .proto files must import `comms.context` which is meant for message delivery diagnostics and monitoring, which the framework handles.

**IMPORTANT** : All `response` message definitions in protos should include `string err`

**BUILDING .proto FILES** : To build the .proto files for events, you will need to add your event proto file(s) and run the following command in metaservice repo:

```sh
./node_modules/protobufjs/bin/pbjs -t static-module -w commonjs -o protobuf/build.js protobuf/events/*
```

The file `protobuf/build.js` **must** exist with all the required compiled protobuf declarations, or the service framework will not work.

### Generating JS files from .proto files

Use the `pbjs` tool from the protobufjs module like this :

```sh
./node_modules/protobufjs/bin/pbjs -t static-module -w commonjs -o protobuf/meta/commsexample/request.js protobuf/meta/commsexample/request.proto
```

### Creating DB migration scripts

```sh
./node_modules/db-migrate/bin/db-migrate create <scriptname>
```

The above command will create new .js and .sql files(in sqls folder) with scriptname prefixed by the timestamp.
Mention your creation/alteration SQL commands in scriptname-up.sql
Mention the corresponding revert/drop commands in scriptname-down.sql

## Pushing a container to GCR

Note that this is only for now, before the CI/CD pipeline is figured, wherein git webhook should trigger docker builds.

Build the image with the correct registry/tag

```sh
docker build -t asia.gcr.io/future-pay-212215/<servicename>:<semver> .
```

Example :

```sh
docker build -t asia.gcr.io/future-pay-212215/metaservice:0.0.1 .
```

Docker login to GCR with the provided key file

```sh
docker login -u _json_key --password-stdin https://asia.gcr.io < /path/to/file.json
```

Push the image (shown example is of metaservice@0.0.1)

```sh
docker push asia.gcr.io/future-pay-212215/metaservice:0.0.1
```

## Writing unit tests.

Unit tests are declarative type.
Unit test must be declared in a JSON file adhering to given format.
Every `channel` or `reaction` must have it's own `JSON` file.
In case of channels, the file must places in `test/declarations/channels` folder, Similarly reactions must be in `test/declarations/reactions` folder.

> NOTE : For channels which support both northsouth and eastwest traffic please set `eastwest: true` or `northsouth: true` as the root entry for that particular case.

> eg

```
cases : [
  {
    eastwest : true,
    name : "Check getbalance for admin route"
  },
  {
    northsouth : true,
    name : "Check get balance for customer route"
  }
]
```

For channels which does not support both, no need specify `eastwest` or `northsouth`

Unit tests are run using below `npm` command.

    npm test
