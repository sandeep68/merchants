const managerdb = require('./../persistence/manager')
const log = require('../sys/log')

const validateManager = (req, res, next) => {

    let incoming = req.body

    if (incoming.managerid == "" || incoming.managerid == undefined || isNaN(incoming.managerid)) {
        res.json({ status: "failed", message: "Invalid manager id to process the request." })
    }
    else if (incoming.apikey == "" || incoming.apikey == undefined) {
        res.json({ status: "failed", message: "Invalid manager apikey to process the request." })
    }
    else {
        //validate merchant API KEY value
        managerdb.getManagerById(incoming.managerid)
            .then(response => {
                if (response != undefined) {
                    if (response.apikey != incoming.apikey) {
                        throw new Error("Invalid manager api key to process the request")
                    }
                    else {
                        res.locals.role = response.role;
                        next()
                    }
                }
                else {
                    //throw new Error("Invalid manager details to process the request")
                    next()
                }


            })
            .catch(errresponse => {
                log("Middleware error : " + errresponse, true);
                res.json({ status: "failed", message: errresponse.message })
            })
    }

}

module.exports = validateManager;

