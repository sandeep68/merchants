const log = require('../sys/log');
const menukeys = require('../constant/variables').MENUKEYS
const comms = require('../sys/comms')
const grpc = require("../sys/grpc")
const Messages = require("../constant/variables")

class UserAccess {

    validate(req, res, next) {

        let baseurl = req.baseUrl.replace(/^\/+/, '')
        let url = req.url.replace(/^\/+/, '')
        let service = baseurl.split("/").pop()

        log({ baseUrl: baseurl, service: service, url: url })

        try {

            let statusCode = 400;
            let message = Messages.AUTHENTICATION_ERROR.INVALID_HEADERS.message;

            let moduleaccess = menukeys[service][url]

            if (moduleaccess == "")
                throw new Error("Blank service key")

            if (typeof req["headers"] == "undefined") {
                res.statusCode = statusCode;
                return res.json({ status: statusCode, message: message })
            }

            let { authorization } = req.headers;

            log(authorization)
            log(moduleaccess)
            if (typeof authorization == "undefined" && typeof moduleaccess == "undefined") {
                res.statusCode = statusCode;
                return res.json({ status: statusCode, message: message })
            }

            let payload = { token: authorization, moduleaccess: moduleaccess }
            log("::Merchant Authorize Request Logs::" + req)

            comms.request('useraccess/authorize', payload, grpc.getMetaData(), (err, apires) => {
                log("::Merchant Authorize Err Logs::" + err)
                log("::Merchant Authorize Reponse Logs::" + apires)
                if (err) {
                    log(err)
                    statusCode = 500;
                    message = err.details;
                    if (typeof Messages.AUTHENTICATION_ERROR[err.details] != "undefined") {
                        statusCode = Messages.AUTHENTICATION_ERROR[err.details].statuscode;
                        message = Messages.AUTHENTICATION_ERROR[err.details].message;
                    }
                    res.statusCode = statusCode;
                    res.json({ status: "expired", message: message });
                    //reject(err.details)
                } else {
                    let { id, role, merchantid, retailerid, outletid, region } = apires;
                    req["body"]["user_details"] = { id, role, merchantid, retailerid, outletid, region };

                    res.locals.role = role;
                    req.body.managerid = id;
                    req.body.aggregatorid = 1; // For now hard core lets seee in future 

                    next()
                }
            })

        } catch (error) {
            log(error.message)
            res.json({ status: "failed", message: "Invalid user access key" })
        }


    }
}
module.exports = new UserAccess()