const comms = require('../sys/comms')
// Example synchronous channel
/***
 * Make sure you pass the context object to further requests or publishes, if any
 */
module.exports = (request, context, callback) => {
  let response = { fullmessage: '' }
  if (!request.firstname) {
    callback(new Error('First name required'))
    return
  } else {
    response.fullmessage += request.firstname
  }
  if (request.lastname) {
    response.fullmessage += ` ${request.lastname}`
  }
  if (request.message) {
    response.fullmessage += ` ${request.message}`
  }
  // Example of making a request and handling updated context
  comms.publish('customer.otp.registered', { mobile: '8888888888' })
  callback(null, response)
}