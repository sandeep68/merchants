/*eslint-disable block-scoped-var, no-redeclare, no-control-regex, no-prototype-builtins*/
"use strict";

var $protobuf = require("protobufjs/minimal");

// Common aliases
var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.metaservice = (function() {

    /**
     * Namespace metaservice.
     * @exports metaservice
     * @namespace
     */
    var metaservice = {};

    metaservice.commsexample = (function() {

        /**
         * Namespace commsexample.
         * @memberof metaservice
         * @namespace
         */
        var commsexample = {};

        commsexample.request = (function() {

            /**
             * Properties of a request.
             * @memberof metaservice.commsexample
             * @interface Irequest
             * @property {string|null} [firstname] request firstname
             * @property {string|null} [lastname] request lastname
             * @property {string|null} [message] request message
             * @property {number|null} [customerid] request customerid
             * @property {string|null} [token] request token
             */

            /**
             * Constructs a new request.
             * @memberof metaservice.commsexample
             * @classdesc Represents a request.
             * @implements Irequest
             * @constructor
             * @param {metaservice.commsexample.Irequest=} [properties] Properties to set
             */
            function request(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * request firstname.
             * @member {string} firstname
             * @memberof metaservice.commsexample.request
             * @instance
             */
            request.prototype.firstname = "";

            /**
             * request lastname.
             * @member {string} lastname
             * @memberof metaservice.commsexample.request
             * @instance
             */
            request.prototype.lastname = "";

            /**
             * request message.
             * @member {string} message
             * @memberof metaservice.commsexample.request
             * @instance
             */
            request.prototype.message = "";

            /**
             * request customerid.
             * @member {number} customerid
             * @memberof metaservice.commsexample.request
             * @instance
             */
            request.prototype.customerid = 0;

            /**
             * request token.
             * @member {string} token
             * @memberof metaservice.commsexample.request
             * @instance
             */
            request.prototype.token = "";

            /**
             * Creates a new request instance using the specified properties.
             * @function create
             * @memberof metaservice.commsexample.request
             * @static
             * @param {metaservice.commsexample.Irequest=} [properties] Properties to set
             * @returns {metaservice.commsexample.request} request instance
             */
            request.create = function create(properties) {
                return new request(properties);
            };

            /**
             * Encodes the specified request message. Does not implicitly {@link metaservice.commsexample.request.verify|verify} messages.
             * @function encode
             * @memberof metaservice.commsexample.request
             * @static
             * @param {metaservice.commsexample.Irequest} message request message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            request.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.firstname != null && message.hasOwnProperty("firstname"))
                    writer.uint32(/* id 1, wireType 2 =*/10).string(message.firstname);
                if (message.lastname != null && message.hasOwnProperty("lastname"))
                    writer.uint32(/* id 2, wireType 2 =*/18).string(message.lastname);
                if (message.message != null && message.hasOwnProperty("message"))
                    writer.uint32(/* id 3, wireType 2 =*/26).string(message.message);
                if (message.customerid != null && message.hasOwnProperty("customerid"))
                    writer.uint32(/* id 4, wireType 0 =*/32).int32(message.customerid);
                if (message.token != null && message.hasOwnProperty("token"))
                    writer.uint32(/* id 5, wireType 2 =*/42).string(message.token);
                return writer;
            };

            /**
             * Encodes the specified request message, length delimited. Does not implicitly {@link metaservice.commsexample.request.verify|verify} messages.
             * @function encodeDelimited
             * @memberof metaservice.commsexample.request
             * @static
             * @param {metaservice.commsexample.Irequest} message request message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            request.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a request message from the specified reader or buffer.
             * @function decode
             * @memberof metaservice.commsexample.request
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {metaservice.commsexample.request} request
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            request.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.metaservice.commsexample.request();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.firstname = reader.string();
                        break;
                    case 2:
                        message.lastname = reader.string();
                        break;
                    case 3:
                        message.message = reader.string();
                        break;
                    case 4:
                        message.customerid = reader.int32();
                        break;
                    case 5:
                        message.token = reader.string();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a request message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof metaservice.commsexample.request
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {metaservice.commsexample.request} request
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            request.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a request message.
             * @function verify
             * @memberof metaservice.commsexample.request
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            request.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.firstname != null && message.hasOwnProperty("firstname"))
                    if (!$util.isString(message.firstname))
                        return "firstname: string expected";
                if (message.lastname != null && message.hasOwnProperty("lastname"))
                    if (!$util.isString(message.lastname))
                        return "lastname: string expected";
                if (message.message != null && message.hasOwnProperty("message"))
                    if (!$util.isString(message.message))
                        return "message: string expected";
                if (message.customerid != null && message.hasOwnProperty("customerid"))
                    if (!$util.isInteger(message.customerid))
                        return "customerid: integer expected";
                if (message.token != null && message.hasOwnProperty("token"))
                    if (!$util.isString(message.token))
                        return "token: string expected";
                return null;
            };

            /**
             * Creates a request message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof metaservice.commsexample.request
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {metaservice.commsexample.request} request
             */
            request.fromObject = function fromObject(object) {
                if (object instanceof $root.metaservice.commsexample.request)
                    return object;
                var message = new $root.metaservice.commsexample.request();
                if (object.firstname != null)
                    message.firstname = String(object.firstname);
                if (object.lastname != null)
                    message.lastname = String(object.lastname);
                if (object.message != null)
                    message.message = String(object.message);
                if (object.customerid != null)
                    message.customerid = object.customerid | 0;
                if (object.token != null)
                    message.token = String(object.token);
                return message;
            };

            /**
             * Creates a plain object from a request message. Also converts values to other types if specified.
             * @function toObject
             * @memberof metaservice.commsexample.request
             * @static
             * @param {metaservice.commsexample.request} message request
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            request.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults) {
                    object.firstname = "";
                    object.lastname = "";
                    object.message = "";
                    object.customerid = 0;
                    object.token = "";
                }
                if (message.firstname != null && message.hasOwnProperty("firstname"))
                    object.firstname = message.firstname;
                if (message.lastname != null && message.hasOwnProperty("lastname"))
                    object.lastname = message.lastname;
                if (message.message != null && message.hasOwnProperty("message"))
                    object.message = message.message;
                if (message.customerid != null && message.hasOwnProperty("customerid"))
                    object.customerid = message.customerid;
                if (message.token != null && message.hasOwnProperty("token"))
                    object.token = message.token;
                return object;
            };

            /**
             * Converts this request to JSON.
             * @function toJSON
             * @memberof metaservice.commsexample.request
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            request.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return request;
        })();

        commsexample.response = (function() {

            /**
             * Properties of a response.
             * @memberof metaservice.commsexample
             * @interface Iresponse
             * @property {string|null} [fullmessage] response fullmessage
             */

            /**
             * Constructs a new response.
             * @memberof metaservice.commsexample
             * @classdesc Represents a response.
             * @implements Iresponse
             * @constructor
             * @param {metaservice.commsexample.Iresponse=} [properties] Properties to set
             */
            function response(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * response fullmessage.
             * @member {string} fullmessage
             * @memberof metaservice.commsexample.response
             * @instance
             */
            response.prototype.fullmessage = "";

            /**
             * Creates a new response instance using the specified properties.
             * @function create
             * @memberof metaservice.commsexample.response
             * @static
             * @param {metaservice.commsexample.Iresponse=} [properties] Properties to set
             * @returns {metaservice.commsexample.response} response instance
             */
            response.create = function create(properties) {
                return new response(properties);
            };

            /**
             * Encodes the specified response message. Does not implicitly {@link metaservice.commsexample.response.verify|verify} messages.
             * @function encode
             * @memberof metaservice.commsexample.response
             * @static
             * @param {metaservice.commsexample.Iresponse} message response message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            response.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.fullmessage != null && message.hasOwnProperty("fullmessage"))
                    writer.uint32(/* id 1, wireType 2 =*/10).string(message.fullmessage);
                return writer;
            };

            /**
             * Encodes the specified response message, length delimited. Does not implicitly {@link metaservice.commsexample.response.verify|verify} messages.
             * @function encodeDelimited
             * @memberof metaservice.commsexample.response
             * @static
             * @param {metaservice.commsexample.Iresponse} message response message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            response.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a response message from the specified reader or buffer.
             * @function decode
             * @memberof metaservice.commsexample.response
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {metaservice.commsexample.response} response
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            response.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.metaservice.commsexample.response();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.fullmessage = reader.string();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a response message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof metaservice.commsexample.response
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {metaservice.commsexample.response} response
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            response.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a response message.
             * @function verify
             * @memberof metaservice.commsexample.response
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            response.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.fullmessage != null && message.hasOwnProperty("fullmessage"))
                    if (!$util.isString(message.fullmessage))
                        return "fullmessage: string expected";
                return null;
            };

            /**
             * Creates a response message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof metaservice.commsexample.response
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {metaservice.commsexample.response} response
             */
            response.fromObject = function fromObject(object) {
                if (object instanceof $root.metaservice.commsexample.response)
                    return object;
                var message = new $root.metaservice.commsexample.response();
                if (object.fullmessage != null)
                    message.fullmessage = String(object.fullmessage);
                return message;
            };

            /**
             * Creates a plain object from a response message. Also converts values to other types if specified.
             * @function toObject
             * @memberof metaservice.commsexample.response
             * @static
             * @param {metaservice.commsexample.response} message response
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            response.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults)
                    object.fullmessage = "";
                if (message.fullmessage != null && message.hasOwnProperty("fullmessage"))
                    object.fullmessage = message.fullmessage;
                return object;
            };

            /**
             * Converts this response to JSON.
             * @function toJSON
             * @memberof metaservice.commsexample.response
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            response.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return response;
        })();

        return commsexample;
    })();

    metaservice.commsresponseexample = (function() {

        /**
         * Namespace commsresponseexample.
         * @memberof metaservice
         * @namespace
         */
        var commsresponseexample = {};

        commsresponseexample.request = (function() {

            /**
             * Properties of a request.
             * @memberof metaservice.commsresponseexample
             * @interface Irequest
             * @property {string|null} [firstname] request firstname
             * @property {string|null} [lastname] request lastname
             * @property {string|null} [message] request message
             * @property {number|null} [customerid] request customerid
             * @property {string|null} [token] request token
             */

            /**
             * Constructs a new request.
             * @memberof metaservice.commsresponseexample
             * @classdesc Represents a request.
             * @implements Irequest
             * @constructor
             * @param {metaservice.commsresponseexample.Irequest=} [properties] Properties to set
             */
            function request(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * request firstname.
             * @member {string} firstname
             * @memberof metaservice.commsresponseexample.request
             * @instance
             */
            request.prototype.firstname = "";

            /**
             * request lastname.
             * @member {string} lastname
             * @memberof metaservice.commsresponseexample.request
             * @instance
             */
            request.prototype.lastname = "";

            /**
             * request message.
             * @member {string} message
             * @memberof metaservice.commsresponseexample.request
             * @instance
             */
            request.prototype.message = "";

            /**
             * request customerid.
             * @member {number} customerid
             * @memberof metaservice.commsresponseexample.request
             * @instance
             */
            request.prototype.customerid = 0;

            /**
             * request token.
             * @member {string} token
             * @memberof metaservice.commsresponseexample.request
             * @instance
             */
            request.prototype.token = "";

            /**
             * Creates a new request instance using the specified properties.
             * @function create
             * @memberof metaservice.commsresponseexample.request
             * @static
             * @param {metaservice.commsresponseexample.Irequest=} [properties] Properties to set
             * @returns {metaservice.commsresponseexample.request} request instance
             */
            request.create = function create(properties) {
                return new request(properties);
            };

            /**
             * Encodes the specified request message. Does not implicitly {@link metaservice.commsresponseexample.request.verify|verify} messages.
             * @function encode
             * @memberof metaservice.commsresponseexample.request
             * @static
             * @param {metaservice.commsresponseexample.Irequest} message request message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            request.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.firstname != null && message.hasOwnProperty("firstname"))
                    writer.uint32(/* id 1, wireType 2 =*/10).string(message.firstname);
                if (message.lastname != null && message.hasOwnProperty("lastname"))
                    writer.uint32(/* id 2, wireType 2 =*/18).string(message.lastname);
                if (message.message != null && message.hasOwnProperty("message"))
                    writer.uint32(/* id 3, wireType 2 =*/26).string(message.message);
                if (message.customerid != null && message.hasOwnProperty("customerid"))
                    writer.uint32(/* id 4, wireType 0 =*/32).int32(message.customerid);
                if (message.token != null && message.hasOwnProperty("token"))
                    writer.uint32(/* id 5, wireType 2 =*/42).string(message.token);
                return writer;
            };

            /**
             * Encodes the specified request message, length delimited. Does not implicitly {@link metaservice.commsresponseexample.request.verify|verify} messages.
             * @function encodeDelimited
             * @memberof metaservice.commsresponseexample.request
             * @static
             * @param {metaservice.commsresponseexample.Irequest} message request message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            request.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a request message from the specified reader or buffer.
             * @function decode
             * @memberof metaservice.commsresponseexample.request
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {metaservice.commsresponseexample.request} request
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            request.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.metaservice.commsresponseexample.request();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.firstname = reader.string();
                        break;
                    case 2:
                        message.lastname = reader.string();
                        break;
                    case 3:
                        message.message = reader.string();
                        break;
                    case 4:
                        message.customerid = reader.int32();
                        break;
                    case 5:
                        message.token = reader.string();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a request message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof metaservice.commsresponseexample.request
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {metaservice.commsresponseexample.request} request
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            request.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a request message.
             * @function verify
             * @memberof metaservice.commsresponseexample.request
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            request.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.firstname != null && message.hasOwnProperty("firstname"))
                    if (!$util.isString(message.firstname))
                        return "firstname: string expected";
                if (message.lastname != null && message.hasOwnProperty("lastname"))
                    if (!$util.isString(message.lastname))
                        return "lastname: string expected";
                if (message.message != null && message.hasOwnProperty("message"))
                    if (!$util.isString(message.message))
                        return "message: string expected";
                if (message.customerid != null && message.hasOwnProperty("customerid"))
                    if (!$util.isInteger(message.customerid))
                        return "customerid: integer expected";
                if (message.token != null && message.hasOwnProperty("token"))
                    if (!$util.isString(message.token))
                        return "token: string expected";
                return null;
            };

            /**
             * Creates a request message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof metaservice.commsresponseexample.request
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {metaservice.commsresponseexample.request} request
             */
            request.fromObject = function fromObject(object) {
                if (object instanceof $root.metaservice.commsresponseexample.request)
                    return object;
                var message = new $root.metaservice.commsresponseexample.request();
                if (object.firstname != null)
                    message.firstname = String(object.firstname);
                if (object.lastname != null)
                    message.lastname = String(object.lastname);
                if (object.message != null)
                    message.message = String(object.message);
                if (object.customerid != null)
                    message.customerid = object.customerid | 0;
                if (object.token != null)
                    message.token = String(object.token);
                return message;
            };

            /**
             * Creates a plain object from a request message. Also converts values to other types if specified.
             * @function toObject
             * @memberof metaservice.commsresponseexample.request
             * @static
             * @param {metaservice.commsresponseexample.request} message request
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            request.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults) {
                    object.firstname = "";
                    object.lastname = "";
                    object.message = "";
                    object.customerid = 0;
                    object.token = "";
                }
                if (message.firstname != null && message.hasOwnProperty("firstname"))
                    object.firstname = message.firstname;
                if (message.lastname != null && message.hasOwnProperty("lastname"))
                    object.lastname = message.lastname;
                if (message.message != null && message.hasOwnProperty("message"))
                    object.message = message.message;
                if (message.customerid != null && message.hasOwnProperty("customerid"))
                    object.customerid = message.customerid;
                if (message.token != null && message.hasOwnProperty("token"))
                    object.token = message.token;
                return object;
            };

            /**
             * Converts this request to JSON.
             * @function toJSON
             * @memberof metaservice.commsresponseexample.request
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            request.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return request;
        })();

        commsresponseexample.response = (function() {

            /**
             * Properties of a response.
             * @memberof metaservice.commsresponseexample
             * @interface Iresponse
             * @property {string|null} [fullmessage] response fullmessage
             */

            /**
             * Constructs a new response.
             * @memberof metaservice.commsresponseexample
             * @classdesc Represents a response.
             * @implements Iresponse
             * @constructor
             * @param {metaservice.commsresponseexample.Iresponse=} [properties] Properties to set
             */
            function response(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * response fullmessage.
             * @member {string} fullmessage
             * @memberof metaservice.commsresponseexample.response
             * @instance
             */
            response.prototype.fullmessage = "";

            /**
             * Creates a new response instance using the specified properties.
             * @function create
             * @memberof metaservice.commsresponseexample.response
             * @static
             * @param {metaservice.commsresponseexample.Iresponse=} [properties] Properties to set
             * @returns {metaservice.commsresponseexample.response} response instance
             */
            response.create = function create(properties) {
                return new response(properties);
            };

            /**
             * Encodes the specified response message. Does not implicitly {@link metaservice.commsresponseexample.response.verify|verify} messages.
             * @function encode
             * @memberof metaservice.commsresponseexample.response
             * @static
             * @param {metaservice.commsresponseexample.Iresponse} message response message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            response.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.fullmessage != null && message.hasOwnProperty("fullmessage"))
                    writer.uint32(/* id 1, wireType 2 =*/10).string(message.fullmessage);
                return writer;
            };

            /**
             * Encodes the specified response message, length delimited. Does not implicitly {@link metaservice.commsresponseexample.response.verify|verify} messages.
             * @function encodeDelimited
             * @memberof metaservice.commsresponseexample.response
             * @static
             * @param {metaservice.commsresponseexample.Iresponse} message response message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            response.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a response message from the specified reader or buffer.
             * @function decode
             * @memberof metaservice.commsresponseexample.response
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {metaservice.commsresponseexample.response} response
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            response.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.metaservice.commsresponseexample.response();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.fullmessage = reader.string();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a response message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof metaservice.commsresponseexample.response
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {metaservice.commsresponseexample.response} response
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            response.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a response message.
             * @function verify
             * @memberof metaservice.commsresponseexample.response
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            response.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.fullmessage != null && message.hasOwnProperty("fullmessage"))
                    if (!$util.isString(message.fullmessage))
                        return "fullmessage: string expected";
                return null;
            };

            /**
             * Creates a response message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof metaservice.commsresponseexample.response
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {metaservice.commsresponseexample.response} response
             */
            response.fromObject = function fromObject(object) {
                if (object instanceof $root.metaservice.commsresponseexample.response)
                    return object;
                var message = new $root.metaservice.commsresponseexample.response();
                if (object.fullmessage != null)
                    message.fullmessage = String(object.fullmessage);
                return message;
            };

            /**
             * Creates a plain object from a response message. Also converts values to other types if specified.
             * @function toObject
             * @memberof metaservice.commsresponseexample.response
             * @static
             * @param {metaservice.commsresponseexample.response} message response
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            response.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults)
                    object.fullmessage = "";
                if (message.fullmessage != null && message.hasOwnProperty("fullmessage"))
                    object.fullmessage = message.fullmessage;
                return object;
            };

            /**
             * Converts this response to JSON.
             * @function toJSON
             * @memberof metaservice.commsresponseexample.response
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            response.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return response;
        })();

        return commsresponseexample;
    })();

    return metaservice;
})();

$root.giftcards = (function() {

    /**
     * Namespace giftcards.
     * @exports giftcards
     * @namespace
     */
    var giftcards = {};

    giftcards.thirdparty = (function() {

        /**
         * Namespace thirdparty.
         * @memberof giftcards
         * @namespace
         */
        var thirdparty = {};

        thirdparty.request = (function() {

            /**
             * Properties of a request.
             * @memberof giftcards.thirdparty
             * @interface Irequest
             * @property {number|null} [retailerid] request retailerid
             * @property {number|null} [cpgid] request cpgid
             * @property {number|null} [amount] request amount
             */

            /**
             * Constructs a new request.
             * @memberof giftcards.thirdparty
             * @classdesc Represents a request.
             * @implements Irequest
             * @constructor
             * @param {giftcards.thirdparty.Irequest=} [properties] Properties to set
             */
            function request(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * request retailerid.
             * @member {number} retailerid
             * @memberof giftcards.thirdparty.request
             * @instance
             */
            request.prototype.retailerid = 0;

            /**
             * request cpgid.
             * @member {number} cpgid
             * @memberof giftcards.thirdparty.request
             * @instance
             */
            request.prototype.cpgid = 0;

            /**
             * request amount.
             * @member {number} amount
             * @memberof giftcards.thirdparty.request
             * @instance
             */
            request.prototype.amount = 0;

            /**
             * Creates a new request instance using the specified properties.
             * @function create
             * @memberof giftcards.thirdparty.request
             * @static
             * @param {giftcards.thirdparty.Irequest=} [properties] Properties to set
             * @returns {giftcards.thirdparty.request} request instance
             */
            request.create = function create(properties) {
                return new request(properties);
            };

            /**
             * Encodes the specified request message. Does not implicitly {@link giftcards.thirdparty.request.verify|verify} messages.
             * @function encode
             * @memberof giftcards.thirdparty.request
             * @static
             * @param {giftcards.thirdparty.Irequest} message request message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            request.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.retailerid != null && message.hasOwnProperty("retailerid"))
                    writer.uint32(/* id 1, wireType 0 =*/8).int32(message.retailerid);
                if (message.cpgid != null && message.hasOwnProperty("cpgid"))
                    writer.uint32(/* id 2, wireType 0 =*/16).int32(message.cpgid);
                if (message.amount != null && message.hasOwnProperty("amount"))
                    writer.uint32(/* id 3, wireType 0 =*/24).int32(message.amount);
                return writer;
            };

            /**
             * Encodes the specified request message, length delimited. Does not implicitly {@link giftcards.thirdparty.request.verify|verify} messages.
             * @function encodeDelimited
             * @memberof giftcards.thirdparty.request
             * @static
             * @param {giftcards.thirdparty.Irequest} message request message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            request.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a request message from the specified reader or buffer.
             * @function decode
             * @memberof giftcards.thirdparty.request
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {giftcards.thirdparty.request} request
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            request.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.giftcards.thirdparty.request();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.retailerid = reader.int32();
                        break;
                    case 2:
                        message.cpgid = reader.int32();
                        break;
                    case 3:
                        message.amount = reader.int32();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a request message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof giftcards.thirdparty.request
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {giftcards.thirdparty.request} request
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            request.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a request message.
             * @function verify
             * @memberof giftcards.thirdparty.request
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            request.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.retailerid != null && message.hasOwnProperty("retailerid"))
                    if (!$util.isInteger(message.retailerid))
                        return "retailerid: integer expected";
                if (message.cpgid != null && message.hasOwnProperty("cpgid"))
                    if (!$util.isInteger(message.cpgid))
                        return "cpgid: integer expected";
                if (message.amount != null && message.hasOwnProperty("amount"))
                    if (!$util.isInteger(message.amount))
                        return "amount: integer expected";
                return null;
            };

            /**
             * Creates a request message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof giftcards.thirdparty.request
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {giftcards.thirdparty.request} request
             */
            request.fromObject = function fromObject(object) {
                if (object instanceof $root.giftcards.thirdparty.request)
                    return object;
                var message = new $root.giftcards.thirdparty.request();
                if (object.retailerid != null)
                    message.retailerid = object.retailerid | 0;
                if (object.cpgid != null)
                    message.cpgid = object.cpgid | 0;
                if (object.amount != null)
                    message.amount = object.amount | 0;
                return message;
            };

            /**
             * Creates a plain object from a request message. Also converts values to other types if specified.
             * @function toObject
             * @memberof giftcards.thirdparty.request
             * @static
             * @param {giftcards.thirdparty.request} message request
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            request.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults) {
                    object.retailerid = 0;
                    object.cpgid = 0;
                    object.amount = 0;
                }
                if (message.retailerid != null && message.hasOwnProperty("retailerid"))
                    object.retailerid = message.retailerid;
                if (message.cpgid != null && message.hasOwnProperty("cpgid"))
                    object.cpgid = message.cpgid;
                if (message.amount != null && message.hasOwnProperty("amount"))
                    object.amount = message.amount;
                return object;
            };

            /**
             * Converts this request to JSON.
             * @function toJSON
             * @memberof giftcards.thirdparty.request
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            request.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return request;
        })();

        thirdparty.response = (function() {

            /**
             * Properties of a response.
             * @memberof giftcards.thirdparty
             * @interface Iresponse
             * @property {number|null} [orderid] response orderid
             * @property {string|null} [cardnumber] response cardnumber
             * @property {number|Long|null} [pin] response pin
             */

            /**
             * Constructs a new response.
             * @memberof giftcards.thirdparty
             * @classdesc Represents a response.
             * @implements Iresponse
             * @constructor
             * @param {giftcards.thirdparty.Iresponse=} [properties] Properties to set
             */
            function response(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * response orderid.
             * @member {number} orderid
             * @memberof giftcards.thirdparty.response
             * @instance
             */
            response.prototype.orderid = 0;

            /**
             * response cardnumber.
             * @member {string} cardnumber
             * @memberof giftcards.thirdparty.response
             * @instance
             */
            response.prototype.cardnumber = "";

            /**
             * response pin.
             * @member {number|Long} pin
             * @memberof giftcards.thirdparty.response
             * @instance
             */
            response.prototype.pin = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

            /**
             * Creates a new response instance using the specified properties.
             * @function create
             * @memberof giftcards.thirdparty.response
             * @static
             * @param {giftcards.thirdparty.Iresponse=} [properties] Properties to set
             * @returns {giftcards.thirdparty.response} response instance
             */
            response.create = function create(properties) {
                return new response(properties);
            };

            /**
             * Encodes the specified response message. Does not implicitly {@link giftcards.thirdparty.response.verify|verify} messages.
             * @function encode
             * @memberof giftcards.thirdparty.response
             * @static
             * @param {giftcards.thirdparty.Iresponse} message response message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            response.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.orderid != null && message.hasOwnProperty("orderid"))
                    writer.uint32(/* id 1, wireType 0 =*/8).int32(message.orderid);
                if (message.cardnumber != null && message.hasOwnProperty("cardnumber"))
                    writer.uint32(/* id 2, wireType 2 =*/18).string(message.cardnumber);
                if (message.pin != null && message.hasOwnProperty("pin"))
                    writer.uint32(/* id 3, wireType 0 =*/24).int64(message.pin);
                return writer;
            };

            /**
             * Encodes the specified response message, length delimited. Does not implicitly {@link giftcards.thirdparty.response.verify|verify} messages.
             * @function encodeDelimited
             * @memberof giftcards.thirdparty.response
             * @static
             * @param {giftcards.thirdparty.Iresponse} message response message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            response.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a response message from the specified reader or buffer.
             * @function decode
             * @memberof giftcards.thirdparty.response
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {giftcards.thirdparty.response} response
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            response.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.giftcards.thirdparty.response();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.orderid = reader.int32();
                        break;
                    case 2:
                        message.cardnumber = reader.string();
                        break;
                    case 3:
                        message.pin = reader.int64();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a response message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof giftcards.thirdparty.response
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {giftcards.thirdparty.response} response
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            response.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a response message.
             * @function verify
             * @memberof giftcards.thirdparty.response
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            response.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.orderid != null && message.hasOwnProperty("orderid"))
                    if (!$util.isInteger(message.orderid))
                        return "orderid: integer expected";
                if (message.cardnumber != null && message.hasOwnProperty("cardnumber"))
                    if (!$util.isString(message.cardnumber))
                        return "cardnumber: string expected";
                if (message.pin != null && message.hasOwnProperty("pin"))
                    if (!$util.isInteger(message.pin) && !(message.pin && $util.isInteger(message.pin.low) && $util.isInteger(message.pin.high)))
                        return "pin: integer|Long expected";
                return null;
            };

            /**
             * Creates a response message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof giftcards.thirdparty.response
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {giftcards.thirdparty.response} response
             */
            response.fromObject = function fromObject(object) {
                if (object instanceof $root.giftcards.thirdparty.response)
                    return object;
                var message = new $root.giftcards.thirdparty.response();
                if (object.orderid != null)
                    message.orderid = object.orderid | 0;
                if (object.cardnumber != null)
                    message.cardnumber = String(object.cardnumber);
                if (object.pin != null)
                    if ($util.Long)
                        (message.pin = $util.Long.fromValue(object.pin)).unsigned = false;
                    else if (typeof object.pin === "string")
                        message.pin = parseInt(object.pin, 10);
                    else if (typeof object.pin === "number")
                        message.pin = object.pin;
                    else if (typeof object.pin === "object")
                        message.pin = new $util.LongBits(object.pin.low >>> 0, object.pin.high >>> 0).toNumber();
                return message;
            };

            /**
             * Creates a plain object from a response message. Also converts values to other types if specified.
             * @function toObject
             * @memberof giftcards.thirdparty.response
             * @static
             * @param {giftcards.thirdparty.response} message response
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            response.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults) {
                    object.orderid = 0;
                    object.cardnumber = "";
                    if ($util.Long) {
                        var long = new $util.Long(0, 0, false);
                        object.pin = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                    } else
                        object.pin = options.longs === String ? "0" : 0;
                }
                if (message.orderid != null && message.hasOwnProperty("orderid"))
                    object.orderid = message.orderid;
                if (message.cardnumber != null && message.hasOwnProperty("cardnumber"))
                    object.cardnumber = message.cardnumber;
                if (message.pin != null && message.hasOwnProperty("pin"))
                    if (typeof message.pin === "number")
                        object.pin = options.longs === String ? String(message.pin) : message.pin;
                    else
                        object.pin = options.longs === String ? $util.Long.prototype.toString.call(message.pin) : options.longs === Number ? new $util.LongBits(message.pin.low >>> 0, message.pin.high >>> 0).toNumber() : message.pin;
                return object;
            };

            /**
             * Converts this response to JSON.
             * @function toJSON
             * @memberof giftcards.thirdparty.response
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            response.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return response;
        })();

        return thirdparty;
    })();

    return giftcards;
})();

module.exports = $root;
