/**
 * Service entry point
 */
'use strict'
if (process.env.ENABLE_TRACE) {
  require('@google-cloud/trace-agent').start()
}
require('colors')
const express = require('express')
const http = require('http')
const bodyParser = require('body-parser')
const _ = require('lodash')
const morgan = require('morgan')
const cors = require('cors')
const pjson = require('./package.json')
const gracefulexit = require('./sys/gracefulexit')
const log = require('./sys/log')
const app = express()
app.use(cors())
// Load env from file for dev. Set NODE_ENV in your bashrc or zshrc.
if (process.env.NODE_ENV === 'development') {
  require('env2')('./devenv.json')
}

//Initalize and resume SQS & RabbitMQ queue
require('./sys/taskinit')

let {
  sysnats,
  rxnats,
  grpcnorthsouthserver,
  grpceastwestserver,
  grpcclient,
  getrabbitmqconfig
} = require('./sys/servicewrapper')()

const diagnosticsrouter = require('./routers/servicediagnostics')
const merchantsrouter = require('./routers/merchants')
const retailersrouter = require('./routers/retailers')
const outletsrouter = require('./routers/outlets')
const terminalrouter = require('./routers/terminal')

app.use(bodyParser.json())
app.use(bodyParser.raw())
app.use(cors())
app.use(
  morgan(':method :url :status :res[content-length] - :response-time ms', {
    skip: function (req, res) {
      return req.url == '/diagnostics/alive'
    }
  })
)

/**
 * Methods bind on /dignostics
 * Service dignostics
 */
app.use('/diagnostics/', diagnosticsrouter)

// Dummy router for example
if (pjson.name == 'metaservice') {
  const examplerouter = require(process.env.MOCKAPI ? './routers/example' : './routers/example_mock')
  app.use('/example/', examplerouter)
  const cachedexamplerouter = require(process.env.MOCKAPI ? './routers/cachedexample' : './routers/cachedexample_mock')
  app.use('/cachedexample/', cachedexamplerouter)
}

// Merchants API
app.use('/merchants/', merchantsrouter)
// Retailers API
app.use('/merchants/retailer/', retailersrouter)
// Outlets API
app.use('/merchants/outlet/', outletsrouter)
// Terminal API
app.use('/merchants/terminal/', terminalrouter)

// Routes Tracking
app.use((req, res, next) => {
  const { status, data } = res.locals
  const { headers: metadata, body: request, url: channel, method } = req
  if (process.env.NODE_ENV === 'staging' && _.has(metadata, 'x-breeze-id')) {
    if (process.env.DISABLE_CACHE != 'true') {
      const Recorder = require('./sys/middleware/recorder')
      const recorder = new Recorder()
      recorder.save(recorder.capture({ channel, method }, { request, metadata }, { status, data }))
    }
  }
  res.status(status || 200).json(data)
  next()
})

const port = process.env.SERVICE_PORT || 3000
const server = http.createServer(app)
server.listen(port, () => {
  log(`REST serving on port ${port}`.green.bold)
})

/**
 * Manual Graceful service exit on SIGTERM, SIGINT events
 * Finish RXNATS, gRPC, REST processes before termination.
 * Stop Cache & MySQl connection
 * Forward RabbitMQ publisher's & subscriber's channels & connections
 */
gracefulexit(sysnats, rxnats, server, grpcnorthsouthserver, grpceastwestserver, getrabbitmqconfig)
