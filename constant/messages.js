const validate = Object.freeze({
    REQUIRED_FIELD: 'Field is Required',
    NOT_NUMBER: 'Not a number',
    INVALID_REQUEST: 'Invalid Request',
    NOT_EMPTY: 'Field Should not be empty',
    INVALID_PAN: 'Invalid PAN Number',
    INVALID_MOBILE: 'Invalid Mobile Number',
    INVALID_EMAIL: 'Invalid Email ID',
    INVALID_PINCODE: 'Invalid Pincode',
    INVALID_BOOLEAN_NUM: 'Value must be 0 or 1',
    UNIQUE_VALUE: "Value Should Be Unique",
    DUPLICATE_ENTRY: "Field is Already Exist",
    DATA_NOT_FOUND: "DATA NOT FOUND"
});

const RESPONSE_STATUS = Object.freeze({
    success: "success",
    failed: "failed",
    completed: "completed",
    errors: "errors"
});


module.exports = {
    validate,
    RESPONSE_STATUS
}