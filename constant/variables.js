const REGEX = Object.freeze({
    EMAIL: /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
    MOBILE: /^(\+?\d{1,4}[\s-])?(?!0+\s+,?$)\d{10}\s*,?$/,
    PAN: /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/,
    LONGITUDE: /^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,6})?))$/,
    LATITUDE: /^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/,
    PINCODE: /^[1-9][0-9]{5}$/,
    BOOLEAN_NUM: /^(0|1)$/,
    BOOLEAN_CHAR: /^(Y|N)$/,
    NUMBER: /^\d+$/,
    GSTCERTIFICATE: /^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/
})

const BOOLEAN_CHAR = ["Y", "N"];
const APPROVALS_ACTION = Object.freeze({
    ADD: { value: "add" },
    EDIT: { value: "edit" },
    DELETE: { value: "delete" }
});

const APPROVALS_ACTION_STATUS = Object.freeze({
    REQUESTED: {
        value: "requested",
        applicable: ["approved", "rejected", "failed"],
        previous: []
    },
    APPROVED: {
        value: "approved",
        applicable: [],
        previous: ["requested"]
    },
    REJECTED: {
        value: "rejected",
        applicable: [],
        previous: ["requested"]
    },
    FAILED: {
        value: "failed",
        applicable: [],
        previous: ["requested"]
    }
});

const ACTORS = [
    {
        "type": "merchant",
        "approvalId": "merchantid"
    },
    {
        "type": "retailer",
        "approvalId": "retailerid"
    },
    {
        "type": "outlet",
        "approvalId": "outletid"
    },
    {
        "type": "terminal",
        "approvalId": "terminalid"
    }
];

const ROLES = [
    {
        "type": "superadmin"
    },
    {
        "type": "ops"
    }
]

const CPG_FROM = ["egv", "physical"];
const DENOMINATIONS = [
    {
        "value": "0",
        "id": "0"
    },
    {
        "value": "50",
        "id": "1"
    },
    {
        "value": "100",
        "id": "2"
    },
    {
        "value": "250",
        "id": "3"
    },
    {
        "value": "500",
        "id": "4"
    },
    {
        "value": "750",
        "id": "5"
    },
    {
        "value": "1000",
        "id": "6"
    },
    {
        "value": "5000",
        "id": "7"
    }
];

const MENUKEYS = {
    'merchants': {
        "list": "generic"
    }
}

const AUTHENTICATION_ERROR = Object.freeze({
    INVALID_HEADERS: {
        key: "INVALID_HEADERS",
        message: "Invalid Headers",
        statuscode: 400
    },
    TOKEN_NOT_PROVIDED: {
        key: "TOKEN_NOT_PROVIDED",
        message: "Authentication Token is not given",
        statuscode: 400
    },
    INVALID_TOKEN: {
        key: "INVALID_TOKEN",
        message: "Invalid Token",
        statuscode: 400
    },
    USER_BLOCKED: {
        key: "USER_BLOCKED",
        message: "User is Blocked",
        statuscode: 401
    },
    UNAUTHORIZED_ACCESS: {
        key: "UNAUTHORIZED_ACCESS",
        message: "Unauthorized access",
        statuscode: 401
    },
    MODULE_ACCESS: {
        key: "MODULE_ACCESS",
        message: "Module Access is Missing",
        statuscode: 401
    }
});
module.exports = {
    REGEX,
    APPROVALS_ACTION,
    APPROVALS_ACTION_STATUS,
    ACTORS,
    BOOLEAN_CHAR,
    ROLES,
    CPG_FROM,
    DENOMINATIONS,
    MENUKEYS,
    AUTHENTICATION_ERROR
}