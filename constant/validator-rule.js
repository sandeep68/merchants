const ERROR_MESSAGE = require('./messages');
const MISC = require('./variables')
const REGEX = require('../constant/variables').REGEX
let VALIDATERULE = {
    requirestring: {
        required: true,
        rules: {
            notEmpty: true
        },
        errors: {
            required: ERROR_MESSAGE.validate.REQUIRED_FIELD,
            notEmpty: ERROR_MESSAGE.validate.INVALID_REQUEST
        }
    },
    requirenumber: {
        required: true,
        rules: {
            notEmpty: true
        },
        rules: {
            match: REGEX.NUMBER
        },
        errors: {
            required: ERROR_MESSAGE.validate.REQUIRED_FIELD,
            notEmpty: ERROR_MESSAGE.validate.INVALID_REQUEST,
            match: ERROR_MESSAGE.validate.INVALID_REQUEST
        }
    },
    notrequire: {
        required: false
    },
    notrequirenumber: {
        required: false,
        rules: {
            checkKeyExist: (value) => {
                if (typeof value == "undefined") {
                    return true
                }
                return !isNaN(value.trim()) && typeof value == "string" ? true : false
            }
        },
        errors: {
            checkKeyExist: ERROR_MESSAGE.validate.INVALID_REQUEST
        }
    },
    requirestringandallownull: {
        required: true,
        rules: {
            checkNull: (value) => {
                return isNaN(value.trim()) && value == "null" ? true : false
            }
        },
        errors: {
            required: ERROR_MESSAGE.validate.REQUIRED_FIELD,
            checkNull: ERROR_MESSAGE.validate.INVALID_REQUEST
        }
    },
    requirestringandallowempty: {
        required: true,
        isEmpty: true,
        errors: {
            required: ERROR_MESSAGE.validate.REQUIRED_FIELD,
        }
    },
    mobile: {
        required: true,
        rules: {
            minLength: 10,
            maxLength: 10,
            match: /^[0-9]+$/
        },
        errors: {
            required: ERROR_MESSAGE.validate.REQUIRED_FIELD,
            minLength: ERROR_MESSAGE.validate.INVALID_MOBILE,
            maxLength: ERROR_MESSAGE.validate.INVALID_MOBILE,
            match: ERROR_MESSAGE.validate.INVALID_MOBILE
        }
    },
    email: {
        required: true,
        rules: {
            match: MISC.REGEX.EMAIL
        },
        errors: {
            required: ERROR_MESSAGE.validate.REQUIRED_FIELD,
            match: ERROR_MESSAGE.validate.INVALID_EMAIL
        }
    },
    pan: {
        rules: {
            match: /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/
        },
        errors: {
            match: ERROR_MESSAGE.validate.INVALID_PAN
        }
    }
}

module.exports = VALIDATERULE;